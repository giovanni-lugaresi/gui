﻿using System;
using System.Windows;
using ArdManager.ViewModel;
using GalaSoft.MvvmLight.Messaging;
using Xceed.Wpf.Toolkit.PropertyGrid;

namespace ArdManager
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();

            Messenger.Default.Register<MainWindowMessage>(this, MainWindowMessageReceive);
            Messenger.Default.Register<ParamWindowMessage>(this, ParamWindowMessageReceived);
        }

        private void ParamWindowMessageReceived(ParamWindowMessage msg)
        {
            var win = new ParamWindow();
            win.Closed += Win_Closed;
            var ctx = win.DataContext as ParamViewModel;
            if (ctx != null)
            {
                ctx.Capability = msg.Capability;
            }
            win.ShowDialog();
        }

        private void Win_Closed(object sender, EventArgs e)
        {
            var win = sender as ParamWindow;
            var model = win?.DataContext as ParamViewModel;
            if (model?.Capability != null)
            {
                Messenger.Default.Send(new ParamWindowCloseMessage {Capability = model.Capability});
            }
        }

        private void MainWindowMessageReceive(MainWindowMessage mainWindowMessage)
        {
            try
            {
                if (mainWindowMessage.MessageIcon > 0)
                {
                    MessageBox.Show(mainWindowMessage.Message,
                        mainWindowMessage.Caption,
                        MessageBoxButton.OK,
                        (MessageBoxImage) mainWindowMessage.MessageIcon);
                }
                else
                {
                    MessageBox.Show(mainWindowMessage.Message, mainWindowMessage.Caption, MessageBoxButton.OK);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void PropertyGrid_OnPropertyValueChanged(object sender, PropertyValueChangedEventArgs e)
        {
            var ctx = DataContext as MainViewModel;
            if (ctx != null)
                ctx.IsDirty = true;
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void ComboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {

        }
    }
}
