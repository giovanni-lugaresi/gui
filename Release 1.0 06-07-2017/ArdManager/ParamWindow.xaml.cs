﻿using GalaSoft.MvvmLight.Messaging;

namespace ArdManager
{
    /// <summary>
    /// Logica di interazione per ParamWindow.xaml
    /// </summary>
    public partial class ParamWindow
    {
        public ParamWindow()
        {
            InitializeComponent();

            Messenger.Default.Register<NotificationMessage>(this, NotificationMessageReceived);

        }

        private void NotificationMessageReceived(NotificationMessage msg)
        {
            switch (msg.Notification)
            {
                case "Close":
                    Close();
                    break;
            }
        }
    }
}
