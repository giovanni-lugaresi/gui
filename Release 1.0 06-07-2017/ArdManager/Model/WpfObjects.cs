﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using GalaSoft.MvvmLight;

namespace ArdManager.Model
{
    public class WpfCapability : ObservableObject
    {
        public WpfCapability()
        {
            _keysArd = new ObservableCollection<WpfSearchKeyArd>();
            _keysRd = new ObservableCollection<WpfSearchKeyRd>();
            _parameters = new ObservableCollection<WpfSearchParameter>();
        }

        public override string ToString()
        {
            return Key;
        }

        private string _key;

        public string Key
        {
            get { return _key; }
            set
            {
                if (value != _key)
                {
                    _key = value;
                    RaisePropertyChanged();
                }
            }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set
            {
                if (value != _description)
                {
                    _description = value;
                    RaisePropertyChanged();
                }
            }
        }

        private bool _isChecked;

        public bool IsChecked
        {
            get { return _isChecked; }
            set
            {
                if (value != _isChecked)
                {
                    _isChecked = value;
                    RaisePropertyChanged();
                }
            }
        }

        private ObservableCollection<WpfSearchKeyArd> _keysArd;

        public ObservableCollection<WpfSearchKeyArd> KeysArd
        {
            get { return _keysArd; }
            set
            {
                if (value != _keysArd)
                {
                    _keysArd = value;
                    RaisePropertyChanged();
                    ShowResults();
                }
            }
        }

        private ObservableCollection<WpfSearchKeyRd> _keysRd;

        public ObservableCollection<WpfSearchKeyRd> KeysRd
        {
            get { return _keysRd; }
            set
            {
                if (value != _keysRd)
                {
                    _keysRd = value;
                    RaisePropertyChanged();
                    ShowResults();
                }
            }
        }

        private ObservableCollection<WpfSearchParameter> _parameters;

        public ObservableCollection<WpfSearchParameter> Parameters
        {
            get { return _parameters; }
            set
            {
                if (value != _parameters)
                {
                    _parameters = value;
                    RaisePropertyChanged();
                }
            }
        }

        public int ValuedParameter
        {
            get { return Parameters.Count(x => x.HasValue); }
        }

        public void ClearParameter()
        {
            if (Parameters != null)
            {
                foreach (var obj in Parameters)
                {
                    obj.Value = null;
                    obj.Refresh();
                }
                RaisePropertyChanged("ValuedParameter");
            }
        }

        public java.util.List JavaParameters
        {
            get
            {
                var result = new java.util.ArrayList();
                foreach (var obj in Parameters)
                {
                    if (obj.HasValue)
                        result.add(obj.Key);
                }
                return result;
            }
        }

        public java.util.List JavaParameterValues
        {
            get
            {
                var result = new java.util.ArrayList();
                var pars = JavaParameters?.toArray();
                if (pars != null)
                    foreach (string obj in pars)
                    {
                        var value = Parameters.FirstOrDefault(x => x.Key == obj);
                        if (value != null)
                        {
                            result.add(value.Value);
                        }
                    }
                return result;
            }
        }

        #region Search

        private void ShowResults()
        {
            RaisePropertyChanged("SearchKeysRd");
            RaisePropertyChanged("SearchKeysArd");
        }

        private ObservableCollection<string> _searchResultArd;

        public ObservableCollection<string> SearchResultArd
        {
            get { return _searchResultArd; }
            set
            {
                if (value != _searchResultArd)
                {
                    _searchResultArd = value;
                    RaisePropertyChanged();
                    ShowResults();
                }
            }
        }

        private ObservableCollection<string> _searchResultRd;

        public ObservableCollection<string> SearchResultRd
        {
            get { return _searchResultRd; }
            set
            {
                if (value != _searchResultRd)
                {
                    _searchResultRd = value;
                    RaisePropertyChanged();
                    ShowResults();
                }
            }
        }

        public List<string> SearchKeysArdFromAndSearch { get; set; }

        public ObservableCollection<WpfSearchKeyArd> SearchKeysArd
        {
            get
            {
                var result = KeysArd;

                if (SearchKeysArdFromAndSearch == null && SearchResultArd == null)
                    return result;

                var finalFilter = new List<string>();

                if (SearchKeysArdFromAndSearch != null) finalFilter = SearchKeysArdFromAndSearch;
                if (SearchResultArd != null) finalFilter = SearchResultArd.ToList();

                if (SearchKeysArdFromAndSearch != null)
                    finalFilter = finalFilter.Intersect(SearchKeysArdFromAndSearch).ToList();
                if (SearchResultArd != null) finalFilter = finalFilter.Intersect(SearchResultArd.ToList()).ToList();

                if (finalFilter.Any())
                {
                    result = new ObservableCollection<WpfSearchKeyArd>();
                    foreach (var obj in KeysArd)
                    {
                        if (finalFilter.Contains(obj.ArdId))
                            result.Add(obj);
                    }
                }
                else
                {
                    result = new ObservableCollection<WpfSearchKeyArd>();
                }

                return result;
            }
        }

        public List<string> SearchKeysRdFromAndSearch { get; set; }

        public ObservableCollection<WpfSearchKeyRd> SearchKeysRd
        {
            get
            {
                var result = KeysRd;

                if (SearchKeysRdFromAndSearch == null && SearchResultRd == null)
                    return result;

                var finalFilter = new List<string>();

                if (SearchKeysRdFromAndSearch != null) finalFilter = SearchKeysRdFromAndSearch;
                if (SearchResultRd != null) finalFilter = SearchResultRd.ToList();

                if (SearchKeysRdFromAndSearch != null)
                    finalFilter = finalFilter.Intersect(SearchKeysRdFromAndSearch).ToList();
                if (SearchResultRd != null) finalFilter = finalFilter.Intersect(SearchResultRd.ToList()).ToList();

                if (finalFilter.Any())
                {
                    result = new ObservableCollection<WpfSearchKeyRd>();
                    foreach (var obj in KeysRd)
                    {
                        if (finalFilter.Contains(obj.RdId))
                            result.Add(obj);
                    }
                }
                else
                {
                    result = new ObservableCollection<WpfSearchKeyRd>();
                }
                return result;
            }
        }

        #endregion
    }

    public class WpfSearchKeyArd : ObservableObject
    {
        public WpfSearchKeyArd()
        {
            Profiles = new List<WpfProfile>();
        }

        public override string ToString()
        {
            return $"{ArdId}";
        }

        private string _ardId;

        public string ArdId
        {
            get { return _ardId; }
            set
            {
                if (value != _ardId)
                {
                    _ardId = value;
                    RaisePropertyChanged();
                }
            }
        }

        private List<WpfProfile> _profiles;

        public List<WpfProfile> Profiles
        {
            get { return _profiles; }
            set
            {
                if (value != _profiles)
                {
                    _profiles = value;
                    RaisePropertyChanged();
                }
            }
        }
    }

    public class WpfProfile : ObservableObject
    {
        public WpfProfile(string profileId, string ardId, string capability)
        {
            ProfileId = profileId;
            ArdId = ardId;
            Capability = capability;
        }

        private string _profileId;

        public string ProfileId
        {
            get { return _profileId; }
            set
            {
                if (value != _profileId)
                {
                    _profileId = value;
                    RaisePropertyChanged();
                }
            }
        }

        private string _ardId;

        public string ArdId
        {
            get { return _ardId; }
            set
            {
                if (value != _ardId)
                {
                    _ardId = value;
                    RaisePropertyChanged();
                }
            }
        }

        private string _capability;

        public string Capability
        {
            get { return _capability; }
            set
            {
                if (value != _capability)
                {
                    _capability = value;
                    RaisePropertyChanged();
                }
            }
        }

        public override string ToString()
        {
            return $"{Capability} - [{ArdId}] - [{ProfileId}]";
        }
    }

    public class WpfSearchKeyRd : ObservableObject
    {
        public override string ToString()
        {
            return $"{RdId}";
        }

        private string _rdId;

        public string RdId
        {
            get { return _rdId; }
            set
            {
                if (value != _rdId)
                {
                    _rdId = value;
                    RaisePropertyChanged();
                }
            }
        }
    }

    public class WpfSearchParameter : ObservableObject
    {
        public WpfSearchParameter(string key)
        {
            Key = key;
        }

        public override string ToString()
        {
            return $"{Key}";
        }

        private string _key;

        public string Key
        {
            get { return _key; }
            set
            {
                if (value != _key)
                {
                    _key = value;
                    RaisePropertyChanged();
                }
            }
        }

        private string _value;

        public string Value
        {
            get { return _value; }
            set
            {
                if (value != _value)
                {
                    _value = value;
                    RaisePropertyChanged();
                    RaisePropertyChanged("HasValue");
                    OnPropertyChanged("Value");
                }
            }
        }

        public bool HasValue => !string.IsNullOrEmpty(Value);
        public string Description => Key?.Replace("/", " ");

        public new event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Refresh()
        {
            RaisePropertyChanged("Description");
            RaisePropertyChanged("Value");
            RaisePropertyChanged("HasValue");
        }
    }
}