﻿using fi.tut.mei.resdescapi.rescatapi;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using ArdManager.ModelXsd;
using fi.tut.mei.resdescapi.util;

namespace ArdManager.Model
{
    /// <summary>
    /// Wrapper
    /// </summary>
    public class ArdWrapper
    {
        /// <summary>
        /// Init ResCatAPI_ByName
        /// SearchType array
        /// </summary>
        public ArdWrapper()
        {
            SearchType = Enum.GetValues(typeof (EnumWpfSearchType));

        }

        #region Event

        public class MessageEventArgs : EventArgs
        {
            public string Source { get; private set; }
            public string Message { get; private set; }
            public Exception Ex { get; private set; }

            public MessageEventArgs(string message, string source)
            {
                Message = message;
                Source = source;
            }

            public MessageEventArgs(Exception ex, string source)
            {
                Ex = ex;
                Source = source;
            }
        }

        /// <summary>
        /// Message Generated (Message or Exception)
        /// </summary>
        public event EventHandler<MessageEventArgs> MessageGenerated;

        private void OnMessageGenerated(MessageEventArgs e)
        {
            MessageGenerated?.Invoke(this, e);
        }

        #endregion

        #region Properties

        /// <summary>
        /// fi.tut.mei.resdescapi.rescatapi.ResCatAPI_ByName Instance
        /// </summary>
        public ResCatAPI_ByName ResCatApiByName { get; private set; }

        public ResCatAPI_ById ResCatApiById { get; private set; }

        /// <summary>
        /// Array From EnumSearchType
        /// </summary>
        public Array SearchType { get; private set; }

        public ResourceDescription GetEmpty
            => new ResourceDescription(ResCatApiByName.getRDStructure_EmptyTemplate(), EmptyProfileId);

        //public WpfResourceDescription GetEmptyOld => new WpfResourceDescription(ResCatApiByName.getRDStructure_EmptyTemplate(), EmptyCapability);

        //private WpfCapability EmptyWpfCapability
        //{
        //    get
        //    {
        //        var emptyWpfProfile = new WpfProfile(EmptyProfileId, EmptyArdId, EmptyCapability);
        //        var emptyWpfSearchKey = new WpfSearchKeyArd { ArdId = EmptyArdId };
        //        emptyWpfSearchKey.Profiles.Add(emptyWpfProfile);
        //        var emptyWpfCapability = new WpfCapability
        //        {
        //            IsChecked = false,
        //            Key = EmptyCapability,
        //            Description = EmptyDescription
        //        };
        //        emptyWpfCapability.SearchKeysArd.Add(emptyWpfSearchKey);
        //        return emptyWpfCapability;
        //    }
        //}

        #endregion

        #region Const

        private const string EmptyCapability = "Empty";
        private const string EmptyDescription = "Empty";
        private const string EmptyArdId = "Empty ArdId";
        private const string EmptyProfileId = "Empty Template";

        #endregion

        #region Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configFileName"></param>
        /// <param name="localFileSystem"></param>
        public void Init(string configFileName, string localFileSystem)
        {
            var st = new StackTrace();
            var sf = st.GetFrame(0);
            var currentMethodName = sf.GetMethod();
            OnMessageGenerated(new MessageEventArgs(string.Empty, currentMethodName.Name));
            var watch = new Stopwatch();
            watch.Start();

            ResCatApiByName = new ResCatAPI_ByName();
            ResCatApiById = new ResCatAPI_ById();

            OnMessageGenerated(new MessageEventArgs("step: Configuration.getInstance", currentMethodName.Name));

            var configurationInstance = Configuration.getInstance();

            OnMessageGenerated(new MessageEventArgs("step: setConfigurationFileName", currentMethodName.Name));

            configurationInstance.setConfigurationFileName(configFileName);

            OnMessageGenerated(new MessageEventArgs("step: ResCatStoPool.getInstance", currentMethodName.Name));

            var resCatStoPool = ResCatStoPool.getInstance();

            //OnMessageGenerated(new MessageEventArgs("step: ResCatStoPool.getStorageKeys", currentMethodName.Name));

            //var storageKeys = resCatStoPool.getStorageKeys();

            OnMessageGenerated(new MessageEventArgs("step: ResCatStoPool.getStorage_byName", currentMethodName.Name));

            resCatStoPool.getStorage_byName(localFileSystem);

            OnMessageGenerated(new MessageEventArgs("step: ResCatAPI_ByName.setResCatStoHandle", currentMethodName.Name));

            ResCatApiByName.setResCatStoHandler(resCatStoPool.getStorage_byName(localFileSystem));

            OnMessageGenerated(new MessageEventArgs("step: ResCatAPIInterface.EnumSearchType.CONTAINS",
                currentMethodName.Name));

            ResCatApiByName.setSearchType(Util.EnumSearchType.CONTAINS);

            OnMessageGenerated(new MessageEventArgs("step: ResCatApiById.setResCatStoHandle", currentMethodName.Name));

            ResCatApiById.setResCatStoHandler(resCatStoPool.getStorage_byName(localFileSystem));

            OnMessageGenerated(new MessageEventArgs("step: ResCatApiById.EnumSearchType.CONTAINS",
                currentMethodName.Name));

            ResCatApiById.setSearchType(Util.EnumSearchType.CONTAINS);

            watch.Stop();
            OnMessageGenerated(new MessageEventArgs($"Complete ({watch.Elapsed})", currentMethodName.Name));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="selectedCapability"></param>
        /// <param name="selectedArdId"></param>
        /// <returns></returns>
        public List<string> GetProfiles(WpfCapability selectedCapability, string selectedArdId)
        {
            var st = new StackTrace();
            var sf = st.GetFrame(0);
            var currentMethodName = sf.GetMethod();
            OnMessageGenerated(new MessageEventArgs(string.Empty, currentMethodName.Name));
            var watch = new Stopwatch();
            watch.Start();

            var coll = new List<string>();
            try
            {
                if (selectedCapability == null)
                {
                    OnMessageGenerated(new MessageEventArgs("No Capability", currentMethodName.Name));
                    return coll;
                }

                var lstByName = ResCatApiByName.viewProfile_ARDandCapability(selectedCapability.Key, selectedArdId);
                var lstById = ResCatApiById.viewProfile_ARDandCapability(selectedCapability.Key, selectedArdId);

                if (lstByName?.keySet() == null && lstById?.keySet() == null)
                {
                    OnMessageGenerated(new MessageEventArgs("No Result", currentMethodName.Name));
                    return coll;
                }

                if (lstByName != null) coll.AddRange(lstByName.keySet().toArray().Select(p => p.ToString()));
                if (lstById != null) coll.AddRange(lstById.keySet().toArray().Select(p => p.ToString()));

                watch.Stop();
                OnMessageGenerated(new MessageEventArgs($"Complete ({watch.Elapsed})", currentMethodName.Name));
            }
            catch (Exception ex)
            {
                OnMessageGenerated(new MessageEventArgs(ex, currentMethodName.Name));
            }
            return coll.Distinct().OrderBy(o => o).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="selectedCapability"></param>
        /// <returns></returns>
        public List<string> GetArdIds(WpfCapability selectedCapability)
        {
            var st = new StackTrace();
            var sf = st.GetFrame(0);
            var currentMethodName = sf.GetMethod();
            OnMessageGenerated(new MessageEventArgs(string.Empty, currentMethodName.Name));
            var watch = new Stopwatch();
            watch.Start();

            var coll = new List<string>();
            try
            {
                if (selectedCapability == null)
                {
                    OnMessageGenerated(new MessageEventArgs("No Capability", currentMethodName.Name));
                    return coll;
                }

                var lstByName = ResCatApiByName.viewARD_Capability(selectedCapability.Key);
                var lstById = ResCatApiById.viewARD_Capability(selectedCapability.Key);

                if (lstByName?.keySet() == null && lstById?.keySet() == null)
                {
                    OnMessageGenerated(new MessageEventArgs("No Result", currentMethodName.Name));
                    return coll;
                }

                if (lstByName != null) coll.AddRange(lstByName.keySet().toArray().Select(p => p.ToString()));
                if (lstById != null) coll.AddRange(lstById.keySet().toArray().Select(p => p.ToString()));

                watch.Stop();
                OnMessageGenerated(new MessageEventArgs($"Complete ({watch.Elapsed})", currentMethodName.Name));
            }
            catch (Exception ex)
            {
                OnMessageGenerated(new MessageEventArgs(ex, currentMethodName.Name));
            }
            return coll.Distinct().OrderBy(o => o).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="selectedCapability"></param>
        /// <returns></returns>
        public void GetSearchKeysArd(WpfCapability selectedCapability)
        {
            var st = new StackTrace();
            var sf = st.GetFrame(0);
            var currentMethodName = sf.GetMethod();
            OnMessageGenerated(new MessageEventArgs(string.Empty, currentMethodName.Name));
            var watch = new Stopwatch();
            watch.Start();

            try
            {
                if (selectedCapability == null)
                {
                    OnMessageGenerated(new MessageEventArgs("No Capability", currentMethodName.Name));
                    return;
                }

                if (selectedCapability.Key == EmptyCapability)
                {
                    OnMessageGenerated(new MessageEventArgs("Empty Capability", currentMethodName.Name));
                    return;
                }

                var lstByName = ResCatApiByName.viewARD_Capability(selectedCapability.Key);
                var lstById = ResCatApiById.viewARD_Capability(selectedCapability.Key);
                var temp = new ObservableCollection<WpfSearchKeyArd>();

                if (lstByName?.keySet() != null)
                {
                    foreach (string obj in lstByName.keySet().toArray())
                    {
                        if (!temp.ToList().Exists(x => x.ArdId == obj))
                        {
                            var newObj = new WpfSearchKeyArd
                            {
                                ArdId = obj
                            };
                            temp.Add(newObj);
                        }
                    }
                }
                if (lstById?.keySet() != null)
                {
                    foreach (string obj in lstById.keySet().toArray())
                    {
                        if (!temp.ToList().Exists(x => x.ArdId == obj))
                        {
                            var newObj = new WpfSearchKeyArd
                            {
                                ArdId = obj
                            };
                            temp.Add(newObj);
                        }
                    }
                }

                foreach (var searchKey in temp)
                {
                    var lstProByName = ResCatApiByName.viewProfile_ARDandCapability(selectedCapability.Key,
                        searchKey.ArdId);

                    if (lstProByName?.keySet() != null)
                    {
                        foreach (string obj in lstProByName.keySet().toArray())
                        {
                            if (searchKey.Profiles.Count(x => x.ProfileId == obj) == 0)
                                searchKey.Profiles.Add(new WpfProfile(obj, searchKey.ArdId, selectedCapability.Key));
                        }
                    }

                    var lstProById = ResCatApiById.viewProfile_ARDandCapability(selectedCapability.Key,
                        searchKey.ArdId);

                    if (lstProById?.keySet() != null)
                    {
                        foreach (string obj in lstProById.keySet().toArray())
                        {
                            if (searchKey.Profiles.Count(x => x.ProfileId == obj) == 0)
                                searchKey.Profiles.Add(new WpfProfile(obj, searchKey.ArdId, selectedCapability.Key));
                        }
                    }
                }

                selectedCapability.KeysArd = temp;
                watch.Stop();
                OnMessageGenerated(new MessageEventArgs($"Complete ({watch.Elapsed})", currentMethodName.Name));
            }
            catch (Exception ex)
            {
                OnMessageGenerated(new MessageEventArgs(ex, currentMethodName.Name));
            }
        }

        public void GetSearchKeysRd(WpfCapability selectedCapability)
        {
            var st = new StackTrace();
            var sf = st.GetFrame(0);
            var currentMethodName = sf.GetMethod();
            OnMessageGenerated(new MessageEventArgs(string.Empty, currentMethodName.Name));
            var watch = new Stopwatch();
            watch.Start();

            try
            {
                if (selectedCapability == null)
                {
                    OnMessageGenerated(new MessageEventArgs("No Capability", currentMethodName.Name));
                    return;
                }

                if (selectedCapability.Key == EmptyCapability)
                {
                    OnMessageGenerated(new MessageEventArgs("Empty Capability", currentMethodName.Name));
                    return;
                }

                var lstByName = ResCatApiByName.viewRD_Capability(selectedCapability.Key);
                var lstById = ResCatApiById.viewRD_Capability(selectedCapability.Key);
                var temp = new ObservableCollection<WpfSearchKeyRd>();


                var coll = new List<string>();
                if (lstByName?.keySet() != null)
                {
                    coll.AddRange(lstByName.keySet().toArray().Select(p => p.ToString()));
                }

                if (lstById?.keySet() != null)
                {
                    coll.AddRange(lstById.keySet().toArray().Select(p => p.ToString()));
                }
                foreach (string obj in coll.Distinct())
                {
                    var newObj = new WpfSearchKeyRd();
                    newObj.RdId = obj;
                    temp.Add(newObj);
                }
                selectedCapability.KeysRd = temp;
                watch.Stop();
                OnMessageGenerated(new MessageEventArgs($"Complete ({watch.Elapsed})", currentMethodName.Name));
            }
            catch (Exception ex)
            {
                OnMessageGenerated(new MessageEventArgs(ex, currentMethodName.Name));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="selectedProfile"></param>
        /// <returns></returns>
        public ResourceDescription GetSkeleton(WpfProfile selectedProfile)
        {
            var st = new StackTrace();
            var sf = st.GetFrame(0);
            var currentMethodName = sf.GetMethod();
            OnMessageGenerated(new MessageEventArgs(string.Empty, currentMethodName.Name));
            var watch = new Stopwatch();
            watch.Start();

            ResourceDescription result = null;
            try
            {
                if (selectedProfile == null)
                {
                    OnMessageGenerated(new MessageEventArgs("No Profile", currentMethodName.Name));
                    return null;
                }
                if (selectedProfile.ProfileId == EmptyProfileId &&
                    selectedProfile.ArdId == EmptyArdId &&
                    selectedProfile.Capability == EmptyCapability)
                {
                    OnMessageGenerated(new MessageEventArgs("Empty Profile", currentMethodName.Name));
                    result = GetEmpty;
                }
                else
                {
                    var temp = ResCatApiByName.getRDSkeleton_ARDandProfile(selectedProfile.ArdId,
                        selectedProfile.ProfileId);
                    if (temp == null)
                    {
                        temp = ResCatApiById.getRDSkeleton_ARDandProfile(selectedProfile.ArdId,
                            selectedProfile.ProfileId);
                    }

                    result =
                        new ResourceDescription(temp, selectedProfile.ToString());


                }


                watch.Stop();
                OnMessageGenerated(new MessageEventArgs($"Complete ({watch.Elapsed})", currentMethodName.Name));
            }
            catch (Exception ex)
            {
                OnMessageGenerated(new MessageEventArgs(ex, currentMethodName.Name));
            }
            return result;
        }

        //public WpfResourceDescription GetSkeletonOld(WpfProfile selectedProfile)
        //{
        //    var st = new StackTrace();
        //    var sf = st.GetFrame(0);
        //    var currentMethodName = sf.GetMethod();
        //    OnMessageGenerated(new MessageEventArgs(string.Empty, currentMethodName.Name));
        //    var watch = new Stopwatch();
        //    watch.Start();

        //    WpfResourceDescription result = null;
        //    try
        //    {
        //        if (selectedProfile == null)
        //        {
        //            OnMessageGenerated(new MessageEventArgs("No Profile", currentMethodName.Name));
        //            return null;
        //        }
        //        if (selectedProfile.ProfileId == EmptyProfileId &&
        //            selectedProfile.ArdId == EmptyArdId &&
        //            selectedProfile.Capability == EmptyCapability)
        //        {
        //            OnMessageGenerated(new MessageEventArgs("Empty Profile", currentMethodName.Name));
        //            result = GetEmptyOld;
        //        }
        //        else
        //        {
        //            result =
        //              new WpfResourceDescription(ResCatApiByName.getRDSkeleton_ARDandProfile(selectedProfile.ArdId,
        //                  selectedProfile.ProfileId), selectedProfile);
        //        }


        //        watch.Stop();
        //        OnMessageGenerated(new MessageEventArgs($"Complete ({watch.Elapsed})", currentMethodName.Name));
        //    }
        //    catch (Exception ex)
        //    {
        //        OnMessageGenerated(new MessageEventArgs(ex, currentMethodName.Name));
        //    }
        //    return result;
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="rdId"></param>
        ///// <returns></returns>
        //public WpfResourceDescription GetRdStructureOld(string rdId)
        //{
        //    var st = new StackTrace();
        //    var sf = st.GetFrame(0);
        //    var currentMethodName = sf.GetMethod();
        //    OnMessageGenerated(new MessageEventArgs(string.Empty, currentMethodName.Name));
        //    var watch = new Stopwatch();
        //    watch.Start();

        //    WpfResourceDescription result = null;
        //    try
        //    {
        //        result =
        //            new WpfResourceDescription(ResCatApiByName.getRDStructure_RDId(rdId), rdId);

        //        watch.Stop();
        //        OnMessageGenerated(new MessageEventArgs($"Complete ({watch.Elapsed})", currentMethodName.Name));
        //    }
        //    catch (Exception ex)
        //    {
        //        OnMessageGenerated(new MessageEventArgs(ex, currentMethodName.Name));
        //    }
        //    return result;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rdId"></param>
        /// <returns></returns>
        public ResourceDescription GetRdStructure(string rdId)
        {
            var st = new StackTrace();
            var sf = st.GetFrame(0);
            var currentMethodName = sf.GetMethod();
            OnMessageGenerated(new MessageEventArgs(string.Empty, currentMethodName.Name));
            var watch = new Stopwatch();
            watch.Start();

            ResourceDescription result = null;
            if (string.IsNullOrEmpty(rdId))
                return null;
            try
            {
                var temp = ResCatApiByName.getRDStructure_RDId(rdId);
                if (temp == null)
                {
                    temp = ResCatApiById.getRDStructure_RDId(rdId);
                }

                result =
                    new ResourceDescription(temp, rdId);

                watch.Stop();
                OnMessageGenerated(new MessageEventArgs($"Complete ({watch.Elapsed})", currentMethodName.Name));
            }
            catch (Exception ex)
            {
                OnMessageGenerated(new MessageEventArgs(ex, currentMethodName.Name));
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rdId"></param>
        /// <returns></returns>
        public ResourceDescription TestFinlands()
        {
            var st = new StackTrace();
            var sf = st.GetFrame(0);
            var currentMethodName = sf.GetMethod();
            OnMessageGenerated(new MessageEventArgs(string.Empty, currentMethodName.Name));
            var watch = new Stopwatch();
            watch.Start();

            ResourceDescription result = null;
            try
            {

                ResCatApiByName.setSearchType(Util.EnumSearchType.EXACT);
                //“EXACT”
                var test1 = ResCatApiByName.viewRD_Description("rd.Bosch - ReCaM - Gripper - Disc1");

                var test2 =
                    ResCatApiByName.viewRD_Category(
                        "http://resourcedescription.tut.fi/ontology/resourceModel#GraspingDevice");
                var test3 = ResCatApiByName.viewRD_Category("27290904");
                var test4 =
                    ResCatApiByName.viewARD_Category(
                        "http://resourcedescription.tut.fi/ontology/resourceModel#GraspingDevice");
                var test5 = ResCatApiByName.viewARD_Interface("std.ISO_29262");

                ResCatApiByName.setSearchType(Util.EnumSearchType.CONTAINS);
                //“CONTAINS”
                var test6 = ResCatApiByName.viewRD_Category("#GraspingDevice");
                var test7 = ResCatApiByName.viewRD_Category("resourcedescription.tut.fi/ontology");
                var test8 = ResCatApiByName.viewRD_Interface("gripper");

                //ResCatApiById.setSearchType(Util.EnumSearchType.EXACT);
                ////“EXACT”
                //var test1 = ResCatApiById.viewRD_Description("rd.Bosch - ReCaM - Gripper - Disc1");

                //var test2 =
                //    ResCatApiById.viewRD_Category(
                //        "http://resourcedescription.tut.fi/ontology/resourceModel#GraspingDevice");
                //var test3 = ResCatApiById.viewRD_Category("27290904");
                //var test4 =
                //    ResCatApiById.viewARD_Category(
                //        "http://resourcedescription.tut.fi/ontology/resourceModel#GraspingDevice");
                //var test5 = ResCatApiById.viewARD_Interface("std.ISO_29262");

                //ResCatApiById.setSearchType(Util.EnumSearchType.CONTAINS);
                ////“CONTAINS”
                //var test6 = ResCatApiById.viewRD_Category("#GraspingDevice");
                //var test7 = ResCatApiById.viewRD_Category("resourcedescription.tut.fi/ontology");
                //var test8 = ResCatApiById.viewRD_Interface("gripper");


                watch.Stop();
                OnMessageGenerated(new MessageEventArgs($"Complete ({watch.Elapsed})", currentMethodName.Name));
            }
            catch (Exception ex)
            {
                OnMessageGenerated(new MessageEventArgs(ex, currentMethodName.Name));
            }
            return result;
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="wpfSaveMode"></param>
        ///// <param name="obj"></param>
        //public void SaveOld(EnumWpfSaveMode wpfSaveMode, WpfResourceDescription obj)
        //{
        //    var st = new StackTrace();
        //    var sf = st.GetFrame(0);
        //    var currentMethodName = sf.GetMethod();
        //    OnMessageGenerated(new MessageEventArgs(string.Empty, currentMethodName.Name));
        //    var watch = new Stopwatch();
        //    watch.Start();

        //    try
        //    {
        //        if (obj == null)
        //        {
        //            throw new Exception("Resource Description is Empty!!!");
        //        }
        //        //switch (saveMode)
        //        //{
        //        //    case EnumSaveMode.ArdContent:
        //        //        ResCatApiByName.getResCatStoHandler().pushARDContentToStorage("", "", true);
        //        //        break;
        //        //    case EnumSaveMode.ArdStructure:
        //        //        //ResCatAPI_ByName.getResCatStoHandler().pushARDStructureToStorage(PropertyGridObject.InternalObj, $"{PropertyGridObject.Name}.xml", true);
        //        //        break;
        //        //    case EnumSaveMode.RdContent:
        //        //        ResCatApiByName.getResCatStoHandler().pushRDContentToStorage("", "", true);
        //        //        break;
        //        //    case EnumSaveMode.RdStructure:
        //        ResCatApiByName.getResCatStoHandler()
        //            .pushRDStructureToStorage(obj.InternalObj, $"{obj.WpfResurcesDescriptionAttributes?.Name}.xml", true);
        //        var resCatStoPool = ResCatStoPool.getInstance();
        //        resCatStoPool.reinitialiseStorages();
        //        //        break;
        //        //}
        //        watch.Stop();
        //        OnMessageGenerated(new MessageEventArgs($"Complete ({watch.Elapsed})", currentMethodName.Name));
        //    }
        //    catch (Exception ex)
        //    {
        //        OnMessageGenerated(new MessageEventArgs(ex, currentMethodName.Name));
        //    }
        //}

        public void Save(EnumWpfSaveMode wpfSaveMode, ResourceDescription obj)
        {
            var st = new StackTrace();
            var sf = st.GetFrame(0);
            var currentMethodName = sf.GetMethod();
            OnMessageGenerated(new MessageEventArgs(string.Empty, currentMethodName.Name));
            var watch = new Stopwatch();
            watch.Start();

            try
            {
                if (obj == null)
                {
                    throw new Exception("Resource Description is Empty!!!");
                }
                //switch (saveMode)
                //{
                //    case EnumSaveMode.ArdContent:
                //        ResCatApiByName.getResCatStoHandler().pushARDContentToStorage("", "", true);
                //        break;
                //    case EnumSaveMode.ArdStructure:
                //        //ResCatAPI_ByName.getResCatStoHandler().pushARDStructureToStorage(PropertyGridObject.InternalObj, $"{PropertyGridObject.Name}.xml", true);
                //        break;
                //    case EnumSaveMode.RdContent:
                //        ResCatApiByName.getResCatStoHandler().pushRDContentToStorage("", "", true);
                //        break;
                //    case EnumSaveMode.RdStructure:
                ResCatApiByName.getResCatStoHandler()
                    .pushRDStructureToStorage(obj.InternalObj, $"{obj.GeneralInformation?.Name}.xml", true);
                var resCatStoPool = ResCatStoPool.getInstance();
                resCatStoPool.reinitialiseStorages();
                //        break;
                //}
                watch.Stop();
                OnMessageGenerated(new MessageEventArgs($"Complete ({watch.Elapsed})", currentMethodName.Name));
            }
            catch (Exception ex)
            {
                OnMessageGenerated(new MessageEventArgs(ex, currentMethodName.Name));
            }
        }

        /// <summary>
        /// 
        /// </summary> 
        public List<WpfCapability> GetCapabilities()
        {
            var st = new StackTrace();
            var sf = st.GetFrame(0);
            var currentMethodName = sf.GetMethod();
            OnMessageGenerated(new MessageEventArgs(string.Empty, currentMethodName.Name));
            var watch = new Stopwatch();
            watch.Start();

            var coll = new List<WpfCapability>();

            try
            {
                var client = new fi.tut.mei.capabilityQueryLib.Client();
                var caps = client.getCapabilities().toArray();

                coll.AddRange(from fi.tut.mei.capabilityQueryLib.model.Capability capab in caps
                    where capab != null
                    select new WpfCapability {Key = capab.getName(), Description = capab.getDescription()});
                //coll = GetFakeCapabilities;

                foreach (var wpfCapability in coll)
                {
                    var par = client.viewParameters(wpfCapability.Key);
                    var collPar = new List<WpfSearchParameter>();
                    if (par?.toArray() != null)
                    {
                        foreach (string sPar in par.toArray())
                        {
                            if (collPar.FirstOrDefault(x => x.Key == sPar) == null)
                            {
                                var item = new WpfSearchParameter(sPar);
                                item.PropertyChanged += (o, e) => { item.Refresh(); };
                                collPar.Add(item);
                            }
                        }
                        wpfCapability.Parameters =
                            new ObservableCollection<WpfSearchParameter>(collPar.OrderBy(x => x.Key));
                    }
                }

                //coll.Add(EmptyWpfCapability);

                watch.Stop();
                OnMessageGenerated(new MessageEventArgs($"Complete ({watch.Elapsed})", currentMethodName.Name));
            }
            catch (Exception ex)
            {
                OnMessageGenerated(new MessageEventArgs(ex, currentMethodName.Name));
            }
            return coll.OrderBy(o => o.Key).ToList();
        }

        private List<WpfCapability> GetFakeCapabilities
        {
            get
            {
                var coll = new List<WpfCapability>();
                coll.Add(new WpfCapability {Key = "Riveting", Description = "Fake Generated Riveting"});
                coll.Add(new WpfCapability {Key = "Picking", Description = "Fake Generated Picking"});
                coll.Add(new WpfCapability {Key = "PartStoringInTray", Description = "Fake Generated PartStoringInTray"});
                coll.Add(new WpfCapability
                {
                    Key = "MillingCutterFunction",
                    Description = "Fake Generated MillingCutterFunction"
                });
                coll.Add(new WpfCapability {Key = "BulkBoxFeeding", Description = "Fake Generated BulkBoxFeeding"});
                coll.Add(new WpfCapability
                {
                    Key = "MillingToolFunction",
                    Description = "Fake Generated MillingToolFunction"
                });
                coll.Add(new WpfCapability
                {
                    Key = "DrillingToolFunction",
                    Description = "Fake Generated DrillingToolFunction"
                });
                coll.Add(new WpfCapability {Key = "HoldingByGravity", Description = "Fake Generated HoldingByGravity"});
                coll.Add(new WpfCapability {Key = "FingerGrasping", Description = "Fake Generated FingerGrasping"});
                coll.Add(new WpfCapability {Key = "TransportFeeding", Description = "Fake Generated TransportFeeding"});
                coll.Add(new WpfCapability {Key = "Moving", Description = "Fake Generated Moving"});
                coll.Add(new WpfCapability {Key = "Storing", Description = "Fake Generated Storing"});
                coll.Add(new WpfCapability {Key = "Pressing", Description = "Fake Generated Pressing"});
                coll.Add(new WpfCapability {Key = "PartStoringInTube", Description = "Fake Generated PartStoringInTube"});
                coll.Add(new WpfCapability {Key = "ForceApplying", Description = "Fake Generated ForceApplying"});
                coll.Add(new WpfCapability {Key = "VacuumGrasping", Description = "Fake Generated VacuumGrasping"});
                coll.Add(new WpfCapability {Key = "PartStoringInBulk", Description = "Fake Generated PartStoringInBulk"});
                coll.Add(new WpfCapability {Key = "FingerFunction", Description = "Fake Generated FingerFunction"});
                coll.Add(new WpfCapability {Key = "PositioningVisual", Description = "Fake Generated PositioningVisual"});
                coll.Add(new WpfCapability {Key = "Transporting", Description = "Fake Generated Transporting"});
                coll.Add(new WpfCapability
                {
                    Key = "PressPlateFunction",
                    Description = "Fake Generated PressPlateFunction"
                });
                coll.Add(new WpfCapability
                {
                    Key = "PressPlateFunction",
                    Description = "Fake Generated PressPlateFunction"
                });
                coll.Add(new WpfCapability {Key = "Drilling", Description = "Fake Generated Drilling"});
                coll.Add(new WpfCapability {Key = "MovingTool", Description = "Fake Generated MovingTool"});
                coll.Add(new WpfCapability {Key = "Placing", Description = "Fake Generated Placing"});
                coll.Add(new WpfCapability {Key = "TrayFeeding", Description = "Fake Generated TrayFeeding"});

                return coll;
            }
        }

        public List<string> GetLocalFileSystems(string filename)
        {
            var result = new List<string>();

            if (!File.Exists(filename))
            {
                return result;
            }

            try
            {
                result.AddRange(from line in
                    File.ReadAllLines(filename)
                    select line.Trim().Split('"')
                    into s
                    where
                        s.Length > 1 &&
                        s[0] == ("<ResourceCatalogue name=")
                    select s[1]);
            }
            catch (Exception)
            {

            }
            return result;

        }

        //public void SearchArdByInterface(string search)
        //{
        //    var st = new StackTrace();
        //    var sf = st.GetFrame(0);
        //    var currentMethodName = sf.GetMethod();
        //    OnMessageGenerated(new MessageEventArgs(string.Empty, currentMethodName.Name));
        //    var watch = new Stopwatch();
        //    watch.Start();

        //    //WpfResourceDescription result = null;
        //    try
        //    {
        //        //var test = ResCatApiByName.viewARD_Interface(search);

        //        //var resCatStoHandler = ResCatApiByName.getResCatStoHandler();

        //        //var test = resCatStoHandler.viewARDAndProfiles_Interface(search, ResCatStorageInterface.EnumSearchField.NAME,
        //        //    ResCatAPIInterface.EnumSearchType.CONTAINS, ResCatStorageInterface.EnumSearchDomainForARD.ARD);

        //        //resCatStoHandler.viewARDAndProfiles_Interface(interfaceStd, this.searchField, this.searchType, ResCatStorageInterface.EnumSearchDomainForARD.__ <> ARD);

        //        watch.Stop();
        //        OnMessageGenerated(new MessageEventArgs($"Complete ({watch.Elapsed})", currentMethodName.Name));
        //    }
        //    catch (Exception ex)
        //    {
        //        OnMessageGenerated(new MessageEventArgs(ex, currentMethodName.Name));
        //    }
        //}
        //public void SearchArdByCategory(string search)
        //{
        //    var st = new StackTrace();
        //    var sf = st.GetFrame(0);
        //    var currentMethodName = sf.GetMethod();
        //    OnMessageGenerated(new MessageEventArgs(string.Empty, currentMethodName.Name));
        //    var watch = new Stopwatch();
        //    watch.Start();

        //    //WpfResourceDescription result = null;
        //    try
        //    {
        //        var test = ResCatApiByName.viewARD_Category(search);

        //        watch.Stop();
        //        OnMessageGenerated(new MessageEventArgs($"Complete ({watch.Elapsed})", currentMethodName.Name));
        //    }
        //    catch (Exception ex)
        //    {
        //        OnMessageGenerated(new MessageEventArgs(ex, currentMethodName.Name));
        //    }
        //}
        //public void SearchArdByDescription(string search)
        //{
        //    var st = new StackTrace();
        //    var sf = st.GetFrame(0);
        //    var currentMethodName = sf.GetMethod();
        //    OnMessageGenerated(new MessageEventArgs(string.Empty, currentMethodName.Name));
        //    var watch = new Stopwatch();
        //    watch.Start();

        //    //WpfResourceDescription result = null;
        //    try
        //    {
        //        var test = ResCatApiByName.viewARD_Description(search);

        //        watch.Stop();
        //        OnMessageGenerated(new MessageEventArgs($"Complete ({watch.Elapsed})", currentMethodName.Name));
        //    }
        //    catch (Exception ex)
        //    {
        //        OnMessageGenerated(new MessageEventArgs(ex, currentMethodName.Name));
        //    }
        //}

        public enum EnumSearchType
        {
            Or,
            And
        }

        public List<string> SearchArdByAdvanced(string searchDescription, string searchCategory, string searchInterface,
            EnumSearchType searchType)
        {
            var st = new StackTrace();
            var sf = st.GetFrame(0);
            var currentMethodName = sf.GetMethod();
            OnMessageGenerated(new MessageEventArgs(string.Empty, currentMethodName.Name));
            var watch = new Stopwatch();
            watch.Start();

            var result = new List<string>();

            if (string.IsNullOrEmpty(searchDescription) &&
                string.IsNullOrEmpty(searchCategory) &&
                string.IsNullOrEmpty(searchInterface))
            {
                result = null;
            }

            var resultDescription = new List<string>();
            var resultCategory = new List<string>();
            var resultInterface = new List<string>();

            try
            {
                if (!string.IsNullOrEmpty(searchDescription))
                {
                    try
                    {
                        var obj = ResCatApiByName.viewARD_Description(searchDescription);
                        var founds = obj?.entrySet()?.toArray();
                        if (founds != null)
                            foreach (var set in founds)
                            {
                                var res = set.ToString();
                                resultDescription.AddRange(res.Split('='));
                            }
                    }
                    catch (Exception ex)
                    {
                        var exMess = ex.Message;
                    }
                    try
                    {
                        var obj = ResCatApiById.viewARD_Description(searchDescription);
                        var founds = obj?.entrySet()?.toArray();
                        if (founds != null)
                            foreach (var set in founds)
                            {
                                var res = set.ToString();
                                resultDescription.AddRange(res.Split('='));
                            }
                    }
                    catch (Exception ex)
                    {
                        var exMess = ex.Message;
                    }
                }

                if (!string.IsNullOrEmpty(searchCategory))
                {
                    try
                    {
                        var obj = ResCatApiByName.viewARD_Category(searchCategory);
                        var founds = obj?.entrySet()?.toArray();
                        if (founds != null)
                            foreach (var set in founds)
                            {
                                var res = set.ToString();
                                resultCategory.AddRange(res.Split('='));
                            }
                    }
                    catch (Exception ex)
                    {
                        var exMess = ex.Message;
                    }
                    try
                    {
                        var obj = ResCatApiById.viewARD_Category(searchCategory);
                        var founds = obj?.entrySet()?.toArray();
                        if (founds != null)
                            foreach (var set in founds)
                            {
                                var res = set.ToString();
                                resultCategory.AddRange(res.Split('='));
                            }
                    }
                    catch (Exception ex)
                    {
                        var exMess = ex.Message;
                    }
                }

                if (!string.IsNullOrEmpty(searchInterface))
                {
                    try
                    {
                        var obj = ResCatApiByName.viewARD_Interface(searchInterface);
                        var founds = obj?.entrySet()?.toArray();
                        if (founds != null)
                            foreach (var set in founds)
                            {
                                var res = set.ToString();
                                resultInterface.AddRange(res.Split('='));
                            }
                    }
                    catch (Exception ex)
                    {
                        var exMess = ex.Message;
                    }
                    try
                    {
                        var obj = ResCatApiById.viewARD_Interface(searchInterface);
                        var founds = obj?.entrySet()?.toArray();
                        if (founds != null)
                            foreach (var set in founds)
                            {
                                var res = set.ToString();
                                resultInterface.AddRange(res.Split('='));
                            }
                    }
                    catch (Exception ex)
                    {
                        var exMess = ex.Message;
                    }
                }

                if (searchType == EnumSearchType.Or)
                {
                    if (resultInterface.Any()) result.AddRange(resultInterface);
                    if (resultCategory.Any()) result.AddRange(resultCategory);
                    if (resultDescription.Any()) result.AddRange(resultDescription);
                }
                if (searchType == EnumSearchType.And)
                {
                    if (resultDescription.Any()) result = resultDescription;
                    if (resultCategory.Any()) result = resultCategory;
                    if (resultInterface.Any()) result = resultInterface;

                    if (!string.IsNullOrEmpty(searchDescription)) result = result.Intersect(resultDescription).ToList();
                    if (!string.IsNullOrEmpty(searchCategory)) result = result.Intersect(resultCategory).ToList();
                    if (!string.IsNullOrEmpty(searchInterface)) result = result.Intersect(resultInterface).ToList();
                }

                watch.Stop();
                OnMessageGenerated(new MessageEventArgs($"Complete ({watch.Elapsed})", currentMethodName.Name));
            }
            catch (Exception ex)
            {
                OnMessageGenerated(new MessageEventArgs(ex, currentMethodName.Name));
            }

            return result?.Distinct().ToList();
        }

        public List<string> SearchRdByAdvanced(string searchDescription, string searchCategory, string searchInterface,
            EnumSearchType searchType)
        {
            var st = new StackTrace();
            var sf = st.GetFrame(0);
            var currentMethodName = sf.GetMethod();
            OnMessageGenerated(new MessageEventArgs(string.Empty, currentMethodName.Name));
            var watch = new Stopwatch();
            watch.Start();

            var result = new List<string>();

            if (string.IsNullOrEmpty(searchDescription) &&
                string.IsNullOrEmpty(searchCategory) &&
                string.IsNullOrEmpty(searchInterface))
            {
                result = null;
            }

            var resultDescription = new List<string>();
            var resultCategory = new List<string>();
            var resultInterface = new List<string>();

            try
            {
                if (!string.IsNullOrEmpty(searchDescription))
                {
                    try
                    {
                        var obj = ResCatApiByName.viewRD_Description(searchDescription);
                        var founds = obj?.entrySet()?.toArray();
                        if (founds != null)
                            foreach (var set in founds)
                            {
                                var res = set.ToString();
                                resultDescription.AddRange(res.Split('='));
                            }
                    }
                    catch (Exception ex)
                    {
                        var exMess = ex.Message;
                    }
                    try
                    {
                        var obj = ResCatApiById.viewRD_Description(searchDescription);
                        var founds = obj?.entrySet()?.toArray();
                        if (founds != null)
                            foreach (var set in founds)
                            {
                                var res = set.ToString();
                                resultDescription.AddRange(res.Split('='));
                            }
                    }
                    catch (Exception ex)
                    {
                        var exMess = ex.Message;
                    }
                }

                if (!string.IsNullOrEmpty(searchCategory))
                {
                    try
                    {
                        var obj = ResCatApiByName.viewRD_Category(searchCategory);
                        var founds = obj?.entrySet()?.toArray();
                        if (founds != null)
                            foreach (var set in founds)
                            {
                                var res = set.ToString();
                                resultCategory.AddRange(res.Split('='));
                            }
                    }
                    catch (Exception ex)
                    {
                        var exMess = ex.Message;
                    }
                    try
                    {
                        var obj = ResCatApiById.viewRD_Category(searchCategory);
                        var founds = obj?.entrySet()?.toArray();
                        if (founds != null)
                            foreach (var set in founds)
                            {
                                var res = set.ToString();
                                resultCategory.AddRange(res.Split('='));
                            }
                    }
                    catch (Exception ex)
                    {
                        var exMess = ex.Message;
                    }
                }

                if (!string.IsNullOrEmpty(searchInterface))
                {
                    try
                    {
                        var obj = ResCatApiByName.viewRD_Interface(searchInterface);
                        var founds = obj?.entrySet()?.toArray();
                        if (founds != null)
                            foreach (var set in founds)
                            {
                                var res = set.ToString();
                                resultInterface.AddRange(res.Split('='));
                            }
                    }
                    catch (Exception ex)
                    {
                        var exMess = ex.Message;
                    }
                    try
                    {
                        var obj = ResCatApiById.viewRD_Interface(searchInterface);
                        var founds = obj?.entrySet()?.toArray();
                        if (founds != null)
                            foreach (var set in founds)
                            {
                                var res = set.ToString();
                                resultInterface.AddRange(res.Split('='));
                            }
                    }
                    catch (Exception ex)
                    {
                        var exMess = ex.Message;
                    }
                }

                if (searchType == EnumSearchType.Or)
                {
                    if (resultInterface.Any()) result.AddRange(resultInterface);
                    if (resultCategory.Any()) result.AddRange(resultCategory);
                    if (resultDescription.Any()) result.AddRange(resultDescription);
                }

                if (searchType == EnumSearchType.And)
                {
                    if (resultDescription.Any()) result = resultDescription;
                    if (resultCategory.Any()) result = resultCategory;
                    if (resultInterface.Any()) result = resultInterface;

                    if (!string.IsNullOrEmpty(searchDescription)) result = result.Intersect(resultDescription).ToList();
                    if (!string.IsNullOrEmpty(searchCategory)) result = result.Intersect(resultCategory).ToList();
                    if (!string.IsNullOrEmpty(searchInterface)) result = result.Intersect(resultInterface).ToList();
                }

                watch.Stop();
                OnMessageGenerated(new MessageEventArgs($"Complete ({watch.Elapsed})", currentMethodName.Name));
            }
            catch (Exception ex)
            {
                OnMessageGenerated(new MessageEventArgs(ex, currentMethodName.Name));
            }
            return result?.Distinct().ToList();
        }

        public List<object> SearchArdByParameters(string capName, java.util.List pars, java.util.List values)
        {
            var result = new List<object>();
            var st = new StackTrace();
            var sf = st.GetFrame(0);
            var currentMethodName = sf.GetMethod();
            OnMessageGenerated(new MessageEventArgs(string.Empty, currentMethodName.Name));
            var watch = new Stopwatch();
            watch.Start();

            try
            {
                var temp = ResCatApiByName.viewARD_CapabilityParameter(capName, pars, values);
                var temp1 = ResCatApiById.viewARD_CapabilityParameter(capName, pars, values);

                watch.Stop();
                OnMessageGenerated(new MessageEventArgs($"Complete ({watch.Elapsed})", currentMethodName.Name));
            }
            catch (Exception ex)
            {
                OnMessageGenerated(new MessageEventArgs(ex, currentMethodName.Name));
            }

            return result;
        }

        public List<object> SearchRdByParameters(string capName, java.util.List pars, java.util.List values)
        {
            var result = new List<object>();
            var st = new StackTrace();
            var sf = st.GetFrame(0);
            var currentMethodName = sf.GetMethod();
            OnMessageGenerated(new MessageEventArgs(string.Empty, currentMethodName.Name));
            var watch = new Stopwatch();
            watch.Start();

            try
            {
                var temp = ResCatApiByName.viewRD_CapabilityParameter(capName, pars, values);
                var temp1 = ResCatApiById.viewRD_CapabilityParameter(capName, pars, values);

                watch.Stop();
                OnMessageGenerated(new MessageEventArgs($"Complete ({watch.Elapsed})", currentMethodName.Name));
            }
            catch (Exception ex)
            {
                OnMessageGenerated(new MessageEventArgs(ex, currentMethodName.Name));
            }

            return result;
        }

        #endregion

        #region Enum

        public enum EnumWpfSaveMode
        {
            ArdContent = 0,
            ArdStructure = 1,
            RdContent = 2,
            RdStructure = 3
        }

        public enum EnumWpfSearchType
        {
            Contains = 0,
            EndsWith = 1,
            Exact = 2,
            StartsWith = 3
        }

        #endregion
    }
}