using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    public class VariableBoolean_CT : Variable_CT_wComplex
    {
        public VariableBoolean_CT()
        {
            
        }
        public VariableBoolean_CT(fi.tut.mei.resdescapi.rdapi.model.rd.VariableBooleanCT obj) : base(obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.VariableBooleanCT InternalObj { get; }

        [DisplayName(@"IsValue")]
        public bool? IsValue => InternalObj?.isValue()?.booleanValue();

        [DisplayName(@"IsDefaultValue")]
        public bool? IsDefaultValue => InternalObj?.isDefaultValue()?.booleanValue();
 
    }
}