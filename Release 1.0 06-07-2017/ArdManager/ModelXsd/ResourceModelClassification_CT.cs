using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ResourceModelClassification_CT")]

    public class ResourceModelClassification_CT
    {
        public ResourceModelClassification_CT()
        {
            
        }
        public override string ToString()
        {
            return Name;
        }

        public ResourceModelClassification_CT(
            fi.tut.mei.resdescapi.rdapi.model.rd.ResourceModelClassificationCT resourceDescription)
        {
            InternalObj = resourceDescription;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ResourceModelClassificationCT InternalObj { get; }

        [DisplayName(@"Gid")]
        public string Gid
        {
            get { return InternalObj?.getGid(); }
            set { InternalObj?.setGid(value); }
        }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }

        public static List<ResourceModelClassification_CT> Get(object[] array)
        {
            var result = new List<ResourceModelClassification_CT>();
            if (array == null)
                return result;
            try
            {
                result = array.OfType<fi.tut.mei.resdescapi.rdapi.model.rd.ResourceModelClassificationCT>()
                    .Select(objType => new ResourceModelClassification_CT(objType))
                    .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}