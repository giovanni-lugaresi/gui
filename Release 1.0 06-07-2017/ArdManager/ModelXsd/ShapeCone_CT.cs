using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ShapeCone_CT")]
    public class ShapeCone_CT
    {
        public ShapeCone_CT()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public ShapeCone_CT(
            fi.tut.mei.resdescapi.rdapi.model.rd.ShapeConeCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ShapeConeCT InternalObj { get; }

        [DisplayName(@"diameter1")]
        public string Diameter1
        {
            get { return InternalObj?.getDiameter1(); }
            set { InternalObj?.setDiameter1(value); }
        }
        [DisplayName(@"diameter2")]
        public string Diameter2
        {
            get { return InternalObj?.getDiameter2(); }
            set { InternalObj?.setDiameter2(value); }
        }
        [DisplayName(@"dimension_z")]
        public string DimensionZ
        {
            get { return InternalObj?.getDimensionZ(); }
            set { InternalObj?.setDimensionZ(value); }
        }

        public static List<ShapeCone_CT> Get(object[] array)
        {
            var result = new List<ShapeCone_CT>();
            if (array == null)
                return result;
            try
            {
                result = array.OfType<fi.tut.mei.resdescapi.rdapi.model.rd.ShapeConeCT>()
                    .Select(objType => new ShapeCone_CT(objType))
                    .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}