﻿using System.Collections.Generic;
using System.ComponentModel;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"EnumType_CTFields")]

    public class EnumType_CTFields 
    {
        public EnumType_CTFields()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public EnumType_CTFields(fi.tut.mei.resdescapi.rdapi.model.rd.EnumTypeCT.Fields obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.EnumTypeCT.Fields InternalObj { get; }


        [DisplayName(@"Field")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<EnumType_CTFieldsField> Field
            => EnumType_CTFieldsField.Get(InternalObj?.getField()?.toArray());
    }
}