﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ExternalResourceLink_CT")]
    public class ExternalResourceLink_CT
    {
        public ExternalResourceLink_CT()
        {
            
        }
        public override string ToString()
        {
            return Name;
        }

        public ExternalResourceLink_CT(fi.tut.mei.resdescapi.rdapi.model.rd.ExternalResourceLinkCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ExternalResourceLinkCT InternalObj { get; }

        [DisplayName(@"URL")]
        public string URL
        {
            get { return InternalObj?.getURL(); }
            set { InternalObj?.setURL(value); }
        }

        [DisplayName(@"Id")]
        public string Id
        {
            get { return InternalObj?.getId(); }
            set { InternalObj?.setId(value); }
        }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }

        [DisplayName(@"Description")]
        public string Description
        {
            get { return InternalObj?.getDescription(); }
            set { InternalObj?.setDescription(value); }
        }

        [DisplayName(@"Gid")]
        public string Gid
        {
            get { return InternalObj?.getGid(); }
            set { InternalObj?.setGid(value); }
        }



        public static List<ExternalResourceLink_CT> Get(object[] array)
        {
            var result = new List<ExternalResourceLink_CT>();
            if (array == null)
                return result;
            try
            {
                result = array.OfType<fi.tut.mei.resdescapi.rdapi.model.rd.ExternalResourceLinkCT>()
                    .Select(objType => new ExternalResourceLink_CT(objType))
                    .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}