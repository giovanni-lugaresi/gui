﻿using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ValueString_CT")]
    public class ValueString_CT
    {
        public ValueString_CT()
        {
            
        }
        public ValueString_CT(fi.tut.mei.resdescapi.rdapi.model.rd.ValueStringCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ValueStringCT InternalObj { get; }

        [DisplayName(@"Id")]
        public string Id
        {
            get { return InternalObj?.getId(); }
            set { InternalObj?.setId(value); }
        }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }

        [DisplayName(@"DataType")]
        public string DataType => InternalObj?.getDataType().ToString();

        [DisplayName(@"Value")]
        public string Value
        {
            get { return InternalObj?.getValue(); }
            set { InternalObj?.setValue(value); }
        }

        [DisplayName(@"Description")]
        public string Description
        {
            get { return InternalObj?.getDescription(); }
            set { InternalObj?.setDescription(value); }
        }

        [DisplayName(@"ReqOpt")]
        public string ReqOpt => InternalObj?.getReqOpt()?.value();
    }
}