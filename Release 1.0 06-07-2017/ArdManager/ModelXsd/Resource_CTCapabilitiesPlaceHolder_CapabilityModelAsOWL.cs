using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"Resource_CTCapabilitiesPlaceHolder_CapabilityModelAsOWL")]

    public class Resource_CTCapabilitiesPlaceHolder_CapabilityModelAsOWL
    {
        public Resource_CTCapabilitiesPlaceHolder_CapabilityModelAsOWL()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public Resource_CTCapabilitiesPlaceHolder_CapabilityModelAsOWL(
            fi.tut.mei.resdescapi.rdapi.model.rd.ResourceCT.Capabilities.PlaceHolderCapabilityModelAsOWL obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ResourceCT.Capabilities.PlaceHolderCapabilityModelAsOWL InternalObj
        { get; }


        [DisplayName(@"Any")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<string> Any
        {
            get
            {
                List<string> result = null;
                if (InternalObj?.getAny()?.toArray() != null)
                {
                    result = InternalObj.getAny().toArray().Cast<string>().ToList();
                }
                return result;
            }
        }

        [DisplayName(@"URL_importedOntology")]
        public string UrlImportedOntology
        {
            get { return InternalObj?.getURLImportedOntology(); }
            set { InternalObj?.setURLImportedOntology(value); }
        }
    }
}