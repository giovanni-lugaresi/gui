using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ShapePyramid_CT")]
    public class ShapePyramid_CT
    {
        public ShapePyramid_CT()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public ShapePyramid_CT(
            fi.tut.mei.resdescapi.rdapi.model.rd.ShapePyramidCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ShapePyramidCT InternalObj { get; }

        [DisplayName(@"dimension_x1")]
        public string DimensionX1
        {
            get { return InternalObj?.getDimensionX1(); }
            set { InternalObj?.setDimensionX1(value); }
        }

        [DisplayName(@"dimension_y1")]
        public string DimensionY1
        {
            get { return InternalObj?.getDimensionY1(); }
            set { InternalObj?.setDimensionY1(value); }
        }
        [DisplayName(@"dimension_x2")]
        public string DimensionX2
        {
            get { return InternalObj?.getDimensionX2(); }
            set { InternalObj?.setDimensionX2(value); }
        }

        [DisplayName(@"dimension_y2")]
        public string DimensionY2
        {
            get { return InternalObj?.getDimensionY2(); }
            set { InternalObj?.setDimensionY2(value); }
        }

        [DisplayName(@"dimension_z")]
        public string DimensionZ
        {
            get { return InternalObj?.getDimensionZ(); }
            set { InternalObj?.setDimensionZ(value); }
        }

        public static List<ShapePyramid_CT> Get(object[] array)
        {
            var result = new List<ShapePyramid_CT>();
            if (array == null)
                return result;
            try
            {
                result = array.OfType<fi.tut.mei.resdescapi.rdapi.model.rd.ShapePyramidCT>()
                    .Select(objType => new ShapePyramid_CT(objType))
                    .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}