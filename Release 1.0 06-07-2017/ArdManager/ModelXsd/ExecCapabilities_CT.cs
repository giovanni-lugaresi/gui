﻿using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"Resource ExecCapabilities")]
    public class ExecCapabilities_CT
    {
        public ExecCapabilities_CT()
        {
            
        }
        public ExecCapabilities_CT(fi.tut.mei.resdescapi.rdapi.model.rd.ExecCapabilitiesCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ExecCapabilitiesCT InternalObj { get; }

        ////todo: non c'è la definizione nella classe creata
        //public Symbol_CT SharedPortSymbols
        //  =>
        //        InternalObj?.getSharedPortSymbols() != null
        //            ? new Symbol_CT(
        //                (fi.tut.mei.resdescapi.rdapi.model.rd.SymbolCT)InternalObj.getSharedPortSymbols())
        //            : null;

        [DisplayName(@"ExecCapas")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public ExecCapabilities_CTExecCapas ExecCapas
            =>
                InternalObj?.getExecCapas() != null
                    ? new ExecCapabilities_CTExecCapas(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.ExecCapabilitiesCT.ExecCapas) InternalObj?.getExecCapas())
                    : null;

        //Todo: non c'è la classe giusta mappata
        //[DisplayName(@"ConnectionMethodsForCtrlAndComm")]
        //public ExecCapabilitiesCTConnectionMethodsForCtrlAndComm ConnectionMethodsForCtrlAndComm
        //    =>
        //        InternalObj?.getConnectionMethodsForCtrlAndComm() != null
        //            ? new ConnectionMethodCtrlAndComm_CT(
        //                (fi.tut.mei.resdescapi.rdapi.model.rd.ExecCapabilitiesCT.ConnectionMethodsForCtrlAndComm)
        //                    InternalObj.getConnectionMethodsForCtrlAndComm())
        //            : null;

        [DisplayName(@"Implementation")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public Implementation_CT Implementation
            =>
                InternalObj?.getImplementation() != null
                    ? new Implementation_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.ImplementationCT) InternalObj.getImplementation())
                    : null;
    }
}