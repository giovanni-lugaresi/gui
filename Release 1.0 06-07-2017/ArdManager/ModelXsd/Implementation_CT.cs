using System.Collections.Generic;
using System.ComponentModel;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"Extension")]

    public class Implementation_CT
    {
        public Implementation_CT()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public Implementation_CT(fi.tut.mei.resdescapi.rdapi.model.rd.ImplementationCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ImplementationCT InternalObj { get; }

        [DisplayName(@"PLC_Related")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public Implementation_CTPLC_Related PLC_Related =>
            InternalObj?.getPLCRelated() != null
                ? new Implementation_CTPLC_Related(
                    (fi.tut.mei.resdescapi.rdapi.model.rd.ImplementationCT.PLCRelated)InternalObj.getPLCRelated())
                : null;

      
        [DisplayName(@"Extension")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<ExtensionDataCt> Extension
        {
            get
            {
                List<ExtensionDataCt> result = null;
                if (InternalObj?.getExtension() != null)
                {
                    result = new List<ExtensionDataCt>();

                    foreach (var obj in InternalObj.getExtension().toArray())
                    {
                        var objType = obj;
                        if (objType is fi.tut.mei.resdescapi.rdapi.model.rd.ExtensionDataCT)
                        {
                            result.Add(
                                new ExtensionDataCt(
                                    (fi.tut.mei.resdescapi.rdapi.model.rd.ExtensionDataCT)objType));
                        }
                    }
                }
                return result;
            }
        }
    }
}