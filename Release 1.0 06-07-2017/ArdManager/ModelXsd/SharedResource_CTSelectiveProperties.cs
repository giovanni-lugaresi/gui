﻿using System.Collections.Generic;
using System.ComponentModel;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"SharedResource_CTSelectiveProperties")] 
    public class SharedResource_CTSelectiveProperties
    {
        public SharedResource_CTSelectiveProperties()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public SharedResource_CTSelectiveProperties(fi.tut.mei.resdescapi.rdapi.model.rd.SharedResourceCT.SelectiveProperties obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.SharedResourceCT.SelectiveProperties InternalObj { get; }
 
        [DisplayName(@"Variable")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<Variable_CT_wComplex> Variable => Variable_CT_wComplex.Get(InternalObj?.getVariable()?.toArray());

        [DisplayName(@"VariableRef")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<VariableRef_CT> VariableRef => VariableRef_CT.Get(InternalObj?.getVariableRef()?.toArray());
    }
}