using System.Collections.Generic;
using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ValueComplex_CT")]
    public class ValueArray_CT
    {
        public ValueArray_CT()
        {
            
        }
        public ValueArray_CT(fi.tut.mei.resdescapi.rdapi.model.rd.ValueArrayCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ValueArrayCT InternalObj { get; }

        [DisplayName(@"Item")]
        public List<ValueArray_CTItem> Item
        {
            get
            {
                List<ValueArray_CTItem> result = null;
                if (InternalObj?.getItem()?.toArray() != null)
                {
                    result = new List<ValueArray_CTItem>();

                    foreach (var obj in InternalObj.getItem().toArray())
                    {
                        var objType = obj;
                        if (objType is fi.tut.mei.resdescapi.rdapi.model.rd.ValueArrayCT.Item)
                        {
                            result.Add(
                                new ValueArray_CTItem((fi.tut.mei.resdescapi.rdapi.model.rd.ValueArrayCT.Item) objType));
                        }
                    }
                }
                return result;
            }
        }

        [DisplayName(@"Datatype")]
        public string Datatype => InternalObj?.getDatatype();

        [DisplayName(@"Size")]
        public string Size => InternalObj?.getSize().toString();

        [DisplayName(@"LBound")]
        public string LBound => InternalObj?.getLBound().toString();
    }
}