using System.Collections.Generic;
using System.ComponentModel;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"PropertiesForRD_CTEnvironmentalProperties")]

    public   class PropertiesForRD_CTEnvironmentalProperties
    {
        public PropertiesForRD_CTEnvironmentalProperties()
        {
            
        }
        public PropertiesForRD_CTEnvironmentalProperties(fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.EnvironmentalProperties obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.EnvironmentalProperties InternalObj { get; }

        //todo: va in eccezione
        //[DisplayName(@"EnvironmentalProperty")]
        //[TypeConverter(typeof(ExpandableObjectConverter))]
        //[Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        //public List<EnvironmentalProperty_CT> EnvironmentalProperty => EnvironmentalProperty_CT.Get(InternalObj?.getEnvironmentalProperty()?.toArray());

        [DisplayName(@"VariableRef")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<VariableRef_CT> VariableRef => VariableRef_CT.Get(InternalObj?.getVariableRef()?.toArray());
    }
}