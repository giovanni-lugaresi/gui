﻿using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ModuleCategory_ST")]
    public class ModuleCategory_ST
    {
        public ModuleCategory_ST()
        {
            
        }
        public ModuleCategory_ST(fi.tut.mei.resdescapi.rdapi.model.rd.ModuleCategoryST resourceDescription)
        {
            InternalObj = resourceDescription; 
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ModuleCategoryST InternalObj { get; }

        [DisplayName(@"OTHER")]
        public string Other => fi.tut.mei.resdescapi.rdapi.model.rd.ModuleCategoryST.OTHER.value();

        [DisplayName(@"PROCESS_MODULE")]
        public string ProcessModule => fi.tut.mei.resdescapi.rdapi.model.rd.ModuleCategoryST.PROCESS_MODULE.value();

        [DisplayName(@"STATIONHEAD")]
        public string Stationhead => fi.tut.mei.resdescapi.rdapi.model.rd.ModuleCategoryST.STATIONHEAD.value();

        [DisplayName(@"SUB_MODULE")]
        public string SubModule => fi.tut.mei.resdescapi.rdapi.model.rd.ModuleCategoryST.SUB_MODULE.value();

        [DisplayName(@"SUPERVISOR")]
        public string Supervisor => fi.tut.mei.resdescapi.rdapi.model.rd.ModuleCategoryST.SUPERVISOR.value();

        [DisplayName(@"CONVEYOR")]
        public string Conveyor => fi.tut.mei.resdescapi.rdapi.model.rd.ModuleCategoryST.CONVEYOR.value();

    }
}