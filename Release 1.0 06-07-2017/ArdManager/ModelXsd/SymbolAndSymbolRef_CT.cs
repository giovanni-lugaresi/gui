using System.Collections.Generic;
using System.ComponentModel;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"SymbolAndSymbolRef_CT")]

    public class SymbolAndSymbolRef_CT
    {
        public SymbolAndSymbolRef_CT()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public SymbolAndSymbolRef_CT(fi.tut.mei.resdescapi.rdapi.model.rd.SymbolAndSymbolRefCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.SymbolAndSymbolRefCT InternalObj { get; }


        [DisplayName(@"SymbolRefs")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<SymbolRefTypePort_CT> SymbolRefs
        {
            get
            {
                List<SymbolRefTypePort_CT> result = null;
                if (InternalObj?.getSymbol() != null)
                {
                    result = new List<SymbolRefTypePort_CT>();

                    foreach (var obj in InternalObj.getSymbol().toArray())
                    {
                        var objType = obj;
                        if (objType is fi.tut.mei.resdescapi.rdapi.model.rd.SymbolRefTypePortCT)
                        {
                            result.Add(
                                new SymbolRefTypePort_CT(
                                    (fi.tut.mei.resdescapi.rdapi.model.rd.SymbolRefTypePortCT) objType));
                        }
                    }
                }
                return result;
            }
        }

        [DisplayName(@"Symbols")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<Symbol_CT> Symbols
        {
            get
            {
                List<Symbol_CT> result = null;
                if (InternalObj?.getSymbol() != null)
                {
                    result = new List<Symbol_CT>();

                    foreach (var obj in InternalObj.getSymbol().toArray())
                    {
                        var objType = obj;
                        if (objType is fi.tut.mei.resdescapi.rdapi.model.rd.SymbolCT)
                        {
                            result.Add(new Symbol_CT((fi.tut.mei.resdescapi.rdapi.model.rd.SymbolCT) objType));
                        }
                    }
                }
                return result;
            }
        }
    }
}