﻿using System.Collections.Generic;
using System.ComponentModel;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"CapabilityMapping_CT")]
    public class CapabilityMapping_CT : CapabilityRef_CT
    {
        public CapabilityMapping_CT()
        {
            
        }
        public override string ToString()
        {
            return string.IsNullOrEmpty(Name) ? IdRef : Name;
        }

        public CapabilityMapping_CT(fi.tut.mei.resdescapi.rdapi.model.rd.CapabilityMappingCT obj) : base(obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.CapabilityMappingCT InternalObj { get; }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }

        [DisplayName(@"MappingType")]
        public string MappingType => InternalObj?.getMappingType();

        [DisplayName(@"ParameterValueRestriction")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<CapabilityMapping_CTParameterValueRestriction> ParameterValueRestriction
        {
            get
            {
                List<CapabilityMapping_CTParameterValueRestriction> result = null;
                if (InternalObj?.getParameterValueRestriction()?.toArray() != null)
                {
                    result = new List<CapabilityMapping_CTParameterValueRestriction>();

                    foreach (var obj in InternalObj.getParameterValueRestriction().toArray())
                    {
                        var objType = obj;
                        if (objType is fi.tut.mei.resdescapi.rdapi.model.rd.CapabilityMappingCT.ParameterValueRestriction)
                        {
                            result.Add(
                                new CapabilityMapping_CTParameterValueRestriction(
                                    (fi.tut.mei.resdescapi.rdapi.model.rd.CapabilityMappingCT.ParameterValueRestriction)
                                        objType));
                        }
                    }
                }
                return result;
            }
        }
    }
}