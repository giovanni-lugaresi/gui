using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ConnectionMethodCtrlAndComm_CT")]
    public class ConnectionMethodCtrlAndComm_CT
    {
        public ConnectionMethodCtrlAndComm_CT()
        {
            
        }
        public ConnectionMethodCtrlAndComm_CT(fi.tut.mei.resdescapi.rdapi.model.rd.ConnectionMethodCtrlAndCommCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ConnectionMethodCtrlAndCommCT InternalObj { get; }


        [DisplayName(@"Labels")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<LocalizedString_CT> Labels => LocalizedString_CT.Get(InternalObj?.getLabel()?.toArray());

        [DisplayName(@"Comments")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<LocalizedString_CT> Comments => LocalizedString_CT.Get(InternalObj?.getComment()?.toArray());

        [DisplayName(@"Documentations")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<Documentation_CT> Documentations => Documentation_CT.Get(InternalObj?.getDocumentation()?.toArray());

        [DisplayName(@"LinkToDescriptionFile")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public List<ExternalFileResourceLink_CT> LinkToDescriptionFile
        {
            get
            {
                List<ExternalFileResourceLink_CT> result = null;
                if (InternalObj?.getLinkToDescriptionFile()?.toArray() != null)
                {
                    result = new List<ExternalFileResourceLink_CT>();

                    foreach (var obj in InternalObj.getLinkToDescriptionFile().toArray())
                    {
                        var objType = obj;
                        if (objType is fi.tut.mei.resdescapi.rdapi.model.rd.ExternalFileResourceLinkCT)
                        {
                            result.Add(
                                new ExternalFileResourceLink_CT(
                                    (fi.tut.mei.resdescapi.rdapi.model.rd.ExternalFileResourceLinkCT) objType));
                        }
                    }
                }
                return result;
            }
        }

        [DisplayName(@"Extensions")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public ExtensionDataCt Extensions
            =>
                InternalObj?.getExtensions() != null
                    ? new ExtensionDataCt(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.ExtensionDataCT) InternalObj.getExtensions())
                    : null;

        [DisplayName(@"Id")]
        public string Id
        {
            get { return InternalObj?.getId(); }
            set { InternalObj?.setId(value); }
        }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }

        [DisplayName(@"Description")]
        public string Description
        {
            get { return InternalObj?.getDescription(); }
            set { InternalObj?.setDescription(value); }
        }
    }
}