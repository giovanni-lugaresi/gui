using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ShapeCylinder_CT")]

    public class ShapeCylinder_CT
    {
        public ShapeCylinder_CT()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public ShapeCylinder_CT(
            fi.tut.mei.resdescapi.rdapi.model.rd.ShapeCylinderCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ShapeCylinderCT InternalObj { get; }

        [DisplayName(@"diameter")]
        public string Diameter
        {
            get { return InternalObj?.getDiameter(); }
            set { InternalObj?.setDiameter(value); }
        }

        [DisplayName(@"dimension_z")]
        public string DimensionZ
        {
            get { return InternalObj?.getDimensionZ(); }
            set { InternalObj?.setDimensionZ(value); }
        }

        public static List<ShapeCylinder_CT> Get(object[] array)
        {
            var result = new List<ShapeCylinder_CT>();
            if (array == null)
                return result;
            try
            {
                result = array.OfType<fi.tut.mei.resdescapi.rdapi.model.rd.ShapeCylinderCT>()
                    .Select(objType => new ShapeCylinder_CT(objType))
                    .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}