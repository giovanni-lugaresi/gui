using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ValueSubRange_CT")]
    public class ValueSubRange_CT
    {
        public ValueSubRange_CT()
        {
            
        }
        public ValueSubRange_CT(fi.tut.mei.resdescapi.rdapi.model.rd.ValueSubRangeCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ValueSubRangeCT InternalObj { get; }

        [DisplayName(@"SimpleValue")]
        public ValueSimple_CT SimpleValue =>
            InternalObj?.getValue() != null
                ? new ValueSimple_CT(InternalObj?.getValue())
                : null;
    }
}