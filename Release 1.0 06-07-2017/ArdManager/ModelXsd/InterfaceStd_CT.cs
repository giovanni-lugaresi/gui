using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"InterfaceStd_CT")]

    public   class InterfaceStd_CT
    {
        public InterfaceStd_CT()
        {
            
        }
        public InterfaceStd_CT(fi.tut.mei.resdescapi.rdapi.model.rd.InterfaceStdCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.InterfaceStdCT InternalObj { get; }
 
        [DisplayName(@"XHTML")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public XHTML_Doc_CT Xhtml =>
            InternalObj?.getXHTML() != null
                ? new XHTML_Doc_CT(InternalObj?.getXHTML())
                : null;


        [DisplayName(@"Class")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<IFClassOption_CT> Class
            =>
                IFClassOption_CT.Get(
                    InternalObj?.getClazz()?.toArray());



        [DisplayName(@"Option")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<IFClassOption_CT> Option
            =>
                IFClassOption_CT.Get(
                    InternalObj?.getOption()?.toArray());


        [DisplayName(@"IFProperty")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<IFProperty_CT> IFProperty
            =>
                IFProperty_CT.Get(
                    InternalObj?.getIFProperty()?.toArray());

        [DisplayName(@"IFPropertyRef")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<IFPropertyRef_CT> IFPropertyRef
            =>
                IFPropertyRef_CT.Get(
                    InternalObj?.getIFPropertyRef()?.toArray());


        [DisplayName(@"Id")]
        public string Id
        {
            get { return InternalObj?.getId(); }
            set { InternalObj?.setId(value); }
        }

        [DisplayName(@"Gid")]
        public string Gid
        {
            get { return InternalObj?.getGid(); }
            set { InternalObj?.setGid(value); }
        }

        [DisplayName(@"Url")]
        public string Url
        {
            get { return InternalObj?.getURL(); }
            set { InternalObj?.setURL(value); }
        }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }

        [DisplayName(@"description")]
        public string Description
        {
            get { return InternalObj?.getDescription(); }
            set { InternalObj?.setDescription(value); }
        }

        [DisplayName(@"idRefStdBody")]
        public string IdRefStdBod => InternalObj?.getIdRefStdBody().ToString();

        [DisplayName(@"code")]
        public string Code
        {
            get { return InternalObj?.getCode(); }
            set { InternalObj?.setCode(value); }
        }

        public static List<InterfaceStd_CT> Get(object[] array)
        {
            var result = new List<InterfaceStd_CT>();
            if (array == null)
                return result;
            try
            {
                result = array.OfType<fi.tut.mei.resdescapi.rdapi.model.rd.InterfaceStdCT>()
                    .Select(objType => new InterfaceStd_CT(objType))
                    .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}