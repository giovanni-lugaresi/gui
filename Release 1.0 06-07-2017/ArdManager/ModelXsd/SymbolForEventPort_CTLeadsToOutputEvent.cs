using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"SymbolForEventPort_CTLeadsToOutputEvent")]
    public class SymbolForEventPort_CTLeadsToOutputEvent
    {
        public SymbolForEventPort_CTLeadsToOutputEvent()
        {
            
        }
        public SymbolForEventPort_CTLeadsToOutputEvent(
            fi.tut.mei.resdescapi.rdapi.model.rd.SymbolForEventPortCT.LeadsToOutputEvent obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.SymbolForEventPortCT.LeadsToOutputEvent InternalObj { get; }

        [DisplayName(@"IdRef")]
        public string IdRef
        {
            get { return InternalObj?.getIdRef(); }
            set { InternalObj?.setIdRef(value); }
        }
    }
}