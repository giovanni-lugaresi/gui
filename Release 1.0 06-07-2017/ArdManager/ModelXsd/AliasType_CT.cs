﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"AliasType_CT")]
    public class AliasType_CT
    {
        public AliasType_CT()
        {
            
        }
        public override string ToString()
        {
            return Name;
        }

        public AliasType_CT(fi.tut.mei.resdescapi.rdapi.model.rd.AliasTypeCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.AliasTypeCT InternalObj { get; }

        [DisplayName(@"Id")]
        public string Id
        {
            get { return InternalObj?.getId(); }
            set { InternalObj?.setId(value); }
        }

        [DisplayName(@"BaseType")]
        public string BaseType
        {
            get { return InternalObj?.getBaseType(); }
            set { InternalObj?.setBaseType(value); }
        }

        [DisplayName(@"NameSpace")]
        public string NameSpace
        {
            get { return InternalObj?.getNameSpace(); }
            set { InternalObj?.setNameSpace(value); }
        }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }


        public static List<AliasType_CT> Get(object[] array)
        {
            var result = new List<AliasType_CT>();
            if (array == null)
                return result;
            try
            {
                result =
                    array
                        .OfType
                        <
                            fi.tut.mei.resdescapi.rdapi.model.rd.AliasTypeCT>()
                        .Select(objType => new AliasType_CT(objType))
                        .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}