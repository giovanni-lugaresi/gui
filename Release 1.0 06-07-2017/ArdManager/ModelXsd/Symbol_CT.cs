using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"Symbol_CT")]
    public class Symbol_CT : ValueSimple_CT
    {
        public Symbol_CT()
        {
            
        }
        public Symbol_CT(fi.tut.mei.resdescapi.rdapi.model.rd.SymbolCT obj) : base(obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.SymbolCT InternalObj { get; }

        [DisplayName(@"NameLoc")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<LocalizedString_CT> NameLoc => LocalizedString_CT.Get(InternalObj?.getNameLoc()?.toArray());

        [DisplayName(@"Comment")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<LocalizedString_CT> Comment => LocalizedString_CT.Get(InternalObj?.getComment()?.toArray());

        [DisplayName(@"ConnectionMethod")]
        public string ConnectionMethod => InternalObj?.getConnectionMethod()?.value();

        [DisplayName(@"InterfaceType")]
        public string InterfaceType => InternalObj?.getInterfaceType()?.value();


        public static List<Symbol_CT> Get(object[] array)
        {
            var result = new List<Symbol_CT>();
            if (array == null)
                return result;
            try
            {
                result =
                    array
                        .OfType
                        <
                            fi.tut.mei.resdescapi.rdapi.model.rd.SymbolCT>()
                        .Select(objType => new Symbol_CT(objType))
                        .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}