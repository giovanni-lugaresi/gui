using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"SymbolRefTypeForEventPort_CT")]

    public class SymbolRefTypeForEventPort_CTLeadsToOutputEvent
    {
        public SymbolRefTypeForEventPort_CTLeadsToOutputEvent()
        {
            
        }
        public SymbolRefTypeForEventPort_CTLeadsToOutputEvent(
            fi.tut.mei.resdescapi.rdapi.model.rd.SymbolRefTypeForEventPortCT.LeadsToOutputEvent obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.SymbolRefTypeForEventPortCT.LeadsToOutputEvent InternalObj { get; }

        [DisplayName(@"IdRef")]
        public string IdRef
        {
            get { return InternalObj?.getIdRef(); }
            set { InternalObj?.setIdRef(value); }
        }
    }
}