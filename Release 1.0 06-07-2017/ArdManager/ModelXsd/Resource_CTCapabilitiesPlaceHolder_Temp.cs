using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"Resource_CTCapabilitiesPlaceHolder_Temp")]

    public class Resource_CTCapabilitiesPlaceHolder_Temp
    {
        public Resource_CTCapabilitiesPlaceHolder_Temp()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public Resource_CTCapabilitiesPlaceHolder_Temp(
            fi.tut.mei.resdescapi.rdapi.model.rd.ResourceCT.Capabilities.PlaceHolderTemp obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ResourceCT.Capabilities.PlaceHolderTemp InternalObj { get; }


        [DisplayName(@"IndividualDevice")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Resource_CTCapabilitiesPlaceHolder_TempIndividualDevice IndividualDevice
         =>
                InternalObj?.getIndividualDevice() != null
                    ? new Resource_CTCapabilitiesPlaceHolder_TempIndividualDevice(InternalObj?.getIndividualDevice())
                    : null;

        [DisplayName(@"Any")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<string> Any
        {
            get
            {
                List<string> result = null;
                if (InternalObj?.getAny()?.toArray() != null)
                {
                    result = InternalObj.getAny().toArray().Cast<string>().ToList();
                }
                return result;
            }
        }

        [DisplayName(@"URL_importedOntology")]
        public string UrlImportedOntology
        {
            get { return InternalObj?.getURLImportedOntology(); }
            set { InternalObj?.setURLImportedOntology(value); }
        }
    }
}