﻿using System.ComponentModel;
using ikvm.extensions;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"DOF3_dataLimits_CT")]

    public   class DOF3_dataLimits_CT
    {
        public DOF3_dataLimits_CT()
        {
            
        }
        public DOF3_dataLimits_CT(
            fi.tut.mei.resdescapi.rdapi.model.rd.DOF3DataLimitsCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.DOF3DataLimitsCT InternalObj { get; }

        [DisplayName(@"Min")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public DOF3_data_Basic_CT Min
            =>
                InternalObj?.getMin() != null
                    ? new DOF3_data_Basic_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.DOF3DataBasicCT)
                            InternalObj.getMin())
                    : null;

        [DisplayName(@"Max")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public DOF3_data_Basic_CT Max
            =>
                InternalObj?.getMax() != null
                    ? new DOF3_data_Basic_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.DOF3DataBasicCT)
                            InternalObj.getMax())
                    : null;

        [DisplayName(@"Unit")]
        public string Unit
        {
            get { return InternalObj?.getUnit(); }
            set { InternalObj?.setUnit(value); }
        }

        [DisplayName(@"Decade")]
        public string Decade => InternalObj?.getDecade().toString();

        [DisplayName(@"Increment")]
        public string Increment => InternalObj?.getIncrement().toString();
    }
}