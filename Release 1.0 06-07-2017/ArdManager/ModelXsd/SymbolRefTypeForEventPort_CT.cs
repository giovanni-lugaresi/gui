using System.Collections.Generic;
using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"SymbolRefTypeForEventPort_CT")]
    public class SymbolRefTypeForEventPort_CT : SymbolRefTypePort_CT
    {
        public SymbolRefTypeForEventPort_CT()
        {
            
        }
        public SymbolRefTypeForEventPort_CT(fi.tut.mei.resdescapi.rdapi.model.rd.SymbolRefTypeForEventPortCT obj):base(obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.SymbolRefTypeForEventPortCT InternalObj { get; }

        [DisplayName(@"LinkedParamPorts")]
        public LinkedParamPorts_CT LinkedParamPorts
            =>
                InternalObj?.getLinkedParamPorts() != null
                    ? new LinkedParamPorts_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.LinkedParamPortsCT)
                            InternalObj.getLinkedParamPorts())
                    : null;

        [DisplayName(@"LeadsToOutputEvent")]
        public List<SymbolRefTypeForEventPort_CTLeadsToOutputEvent> LeadsToOutputEvent
        {
            get
            {
                List<SymbolRefTypeForEventPort_CTLeadsToOutputEvent> result = null;
                if (InternalObj?.getLeadsToOutputEvent()?.toArray() != null)
                {
                    result = new List<SymbolRefTypeForEventPort_CTLeadsToOutputEvent>();

                    foreach (var obj in InternalObj.getLeadsToOutputEvent().toArray())
                    {
                        var objType = obj;
                        if (
                            objType is
                                fi.tut.mei.resdescapi.rdapi.model.rd.SymbolRefTypeForEventPortCT.LeadsToOutputEvent)
                        {
                            result.Add(
                                new SymbolRefTypeForEventPort_CTLeadsToOutputEvent(
                                    (fi.tut.mei.resdescapi.rdapi.model.rd.SymbolRefTypeForEventPortCT.LeadsToOutputEvent
                                        )
                                        objType));
                        }
                    }
                }
                return result;
            }
        }
    }
}