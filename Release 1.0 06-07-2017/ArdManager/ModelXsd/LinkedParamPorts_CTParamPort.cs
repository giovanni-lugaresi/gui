using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"LinkedParamPorts_CTParamPort")]
    public class LinkedParamPorts_CTParamPort
    {
        public LinkedParamPorts_CTParamPort()
        {
            
        }
        public LinkedParamPorts_CTParamPort(fi.tut.mei.resdescapi.rdapi.model.rd.LinkedParamPortsCT.ParamPort obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.LinkedParamPortsCT.ParamPort InternalObj { get; }

        [DisplayName(@"ReqOpt")]
        public string ReqOpt => InternalObj?.getReqOpt()?.value();
    }
}