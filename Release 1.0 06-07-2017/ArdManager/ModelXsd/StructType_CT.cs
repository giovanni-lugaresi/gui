using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"StructType_CT")]
    public  class StructType_CT
    {
        public StructType_CT()
        {
            
        }
        public override string ToString()
        {
            return Name;
        }

        public StructType_CT(fi.tut.mei.resdescapi.rdapi.model.rd.StructTypeCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.StructTypeCT InternalObj { get; }

        [DisplayName(@"Id")]
        public string Id
        {
            get { return InternalObj?.getId(); }
            set { InternalObj?.setId(value); }
        }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }

        [DisplayName(@"NameSpace")]
        public string NameSpace
        {
            get { return InternalObj?.getNameSpace(); }
            set { InternalObj?.setNameSpace(value); }
        }
 
        [DisplayName(@"Members")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<MemberType_CT> Members
            => MemberType_CT.Get(InternalObj?.getMembers()?.getMemberOrStatic()?.toArray());
 
        [DisplayName(@"Statics")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<Symbol_CT> Statics
            => Symbol_CT.Get(InternalObj?.getMembers()?.getMemberOrStatic()?.toArray());


        public static List<StructType_CT> Get(object[] array)
        {
            var result = new List<StructType_CT>();
            if (array == null)
                return result;
            try
            {
                result =
                    array
                        .OfType
                        <
                            fi.tut.mei.resdescapi.rdapi.model.rd.StructTypeCT>()
                        .Select(objType => new StructType_CT(objType))
                        .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}