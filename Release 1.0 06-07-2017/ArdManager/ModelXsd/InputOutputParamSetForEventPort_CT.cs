using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"InputOutputParamSetForEventPort_CT")]
    public class InputOutputParamSetForEventPort_CT
    {
        public InputOutputParamSetForEventPort_CT()
        {
            
        }
        public InputOutputParamSetForEventPort_CT(
            fi.tut.mei.resdescapi.rdapi.model.rd.InputOutputParamSetForEventPortCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.InputOutputParamSetForEventPortCT InternalObj { get; }

        /// <remarks/>
        public EventAndEventRef_RD_CT Inputs =>
            InternalObj?.getInputs() != null
                ? new EventAndEventRef_RD_CT(
                    (fi.tut.mei.resdescapi.rdapi.model.rd.EventAndEventRefRDCT) InternalObj.getInputs())
                : null;

        /// <remarks/>
        public EventAndEventRef_RD_CT Outputs =>
            InternalObj?.getOutputs() != null
                ? new EventAndEventRef_RD_CT(
                    (fi.tut.mei.resdescapi.rdapi.model.rd.EventAndEventRefRDCT) InternalObj.getOutputs())
                : null;
    }
}