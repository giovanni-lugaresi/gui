using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"SubRangeType_CT")]

    public class SubRangeType_CT
    {
        public SubRangeType_CT()
        {
            
        }
        public override string ToString()
        {
            return Name;
        }

        public SubRangeType_CT(fi.tut.mei.resdescapi.rdapi.model.rd.SubRangeTypeCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.SubRangeTypeCT InternalObj { get; }

        [DisplayName(@"UpperBound")]
        public string UpperBound => InternalObj?.getUpperBound().toString();

        [DisplayName(@"LowerBound")]
        public string LowerBound => InternalObj?.getLowerBound().toString();

        [DisplayName(@"BaseType")]
        public string BaseType
        {
            get { return InternalObj?.getBaseType(); }
            set { InternalObj?.setBaseType(value); }
        }

        [DisplayName(@"Id")]
        public string Id
        {
            get { return InternalObj?.getId(); }
            set { InternalObj?.setId(value); }
        }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }

        [DisplayName(@"NameSpace")]
        public string NameSpace
        {
            get { return InternalObj?.getNameSpace(); }
            set { InternalObj?.setNameSpace(value); }
        }



        public static List<SubRangeType_CT> Get(object[] array)
        {
            var result = new List<SubRangeType_CT>();
            if (array == null)
                return result;
            try
            {
                result =
                    array
                        .OfType
                        <
                            fi.tut.mei.resdescapi.rdapi.model.rd.SubRangeTypeCT>()
                        .Select(objType => new SubRangeType_CT(objType))
                        .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}