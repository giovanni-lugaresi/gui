using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"Implementation_CTPLC_Related")]

    public class Implementation_CTPLC_Related
    {
        public Implementation_CTPLC_Related()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public Implementation_CTPLC_Related(fi.tut.mei.resdescapi.rdapi.model.rd.ImplementationCT.PLCRelated obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ImplementationCT.PLCRelated InternalObj { get; }
 
        /// <remarks/>
        [DisplayName(@"Symbols")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public Implementation_CTPLC_RelatedSymbols Symbols =>
            InternalObj?.getSymbols() != null
                ? new Implementation_CTPLC_RelatedSymbols(
                    (fi.tut.mei.resdescapi.rdapi.model.rd.ImplementationCT.PLCRelated.Symbols)InternalObj.getSymbols())
                : null;
    }
}