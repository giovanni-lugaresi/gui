﻿using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ValueStruct_CTMember")]

    public class ValueStruct_CTMember
    {
        public ValueStruct_CTMember()
        {
            
        }
        public ValueStruct_CTMember(fi.tut.mei.resdescapi.rdapi.model.rd.ValueStructCT.Member obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ValueStructCT.Member InternalObj { get; }

        [DisplayName(@"SimpleValue")]
        public ValueSimple_CT SimpleValue =>
            InternalObj?.getSimpleValue() != null
                ? new ValueSimple_CT(InternalObj?.getSimpleValue())
                : null;

        [DisplayName(@"StringValue")]
        public ValueString_CT StringValue =>
            InternalObj?.getStringValue() != null
                ? new ValueString_CT(InternalObj?.getStringValue())
                : null;

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }

        [DisplayName(@"Datatype")]
        public string Datatype => InternalObj?.getDatatype();
    }
}