using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ElectricalResourceDesc_CT")]

    public  class ElectricalResourceDesc_CT : ResourceDescHWInterface_CT
    {
        public ElectricalResourceDesc_CT()
        {
            
        }
        public ElectricalResourceDesc_CT(fi.tut.mei.resdescapi.rdapi.model.rd.ElectricalResourceDescCT obj) : base(obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ElectricalResourceDescCT InternalObj { get; }
    }
}