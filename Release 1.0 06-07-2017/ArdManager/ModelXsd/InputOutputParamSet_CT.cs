using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"InputOutputParamSet_CT")]
    public class InputOutputParamSet_CT
    {
        public InputOutputParamSet_CT()
        {
            
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public InputOutputParamSet_CT(fi.tut.mei.resdescapi.rdapi.model.rd.InputOutputParamSetCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.InputOutputParamSetCT InternalObj { get; }


        [DisplayName(@"Inputs")]
        public SymbolAndSymbolRef_CT Inputs =>
            InternalObj?.getInputs() != null
                ? new SymbolAndSymbolRef_CT(
                    (fi.tut.mei.resdescapi.rdapi.model.rd.SymbolAndSymbolRefCT) InternalObj.getInputs())
                : null;

        [DisplayName(@"Outputs")]
        public SymbolAndSymbolRef_CT Outputs =>
            InternalObj?.getOutputs() != null
                ? new SymbolAndSymbolRef_CT(
                    (fi.tut.mei.resdescapi.rdapi.model.rd.SymbolAndSymbolRefCT) InternalObj.getOutputs())
                : null;
    }
}