﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"InterfaceLink")]
    public class InterfaceLink : ExternalResourceLink_CT
    {
        public InterfaceLink()
        {
            
        }
        public InterfaceLink(fi.tut.mei.resdescapi.rdapi.model.rd.InterfaceLink obj) : base(obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.InterfaceLink InternalObj { get; }


        [DisplayName(@"XHTML")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public XHTML_Doc_CT Xhtml =>
            InternalObj?.getXHTML() != null
                ? new XHTML_Doc_CT(InternalObj?.getXHTML())
                : null;

        [DisplayName(@"Class")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<IFClassOption_CT> Class
        {
            get
            {
                List<IFClassOption_CT> result = null;
                if (InternalObj?.getClazz()?.toArray() != null)
                {
                    result = new List<IFClassOption_CT>();

                    foreach (var obj in InternalObj.getClazz().toArray())
                    {
                        var objType = obj;
                        if (objType is fi.tut.mei.resdescapi.rdapi.model.rd.IFClassOptionCT)
                        {
                            result.Add(
                                new IFClassOption_CT((fi.tut.mei.resdescapi.rdapi.model.rd.IFClassOptionCT)objType));
                        }
                    }
                }
                return result;
            }
        }

        [DisplayName(@"Option")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<IFClassOption_CT> Option
        {
            get
            {
                List<IFClassOption_CT> result = null;
                if (InternalObj?.getOption()?.toArray() != null)
                {
                    result = new List<IFClassOption_CT>();

                    foreach (var obj in InternalObj.getOption().toArray())
                    {
                        var objType = obj;
                        if (objType is fi.tut.mei.resdescapi.rdapi.model.rd.IFClassOptionCT)
                        {
                            result.Add(
                                new IFClassOption_CT((fi.tut.mei.resdescapi.rdapi.model.rd.IFClassOptionCT)objType));
                        }
                    }
                }
                return result;
            }
        }

        [DisplayName(@"IFProperty")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<IFProperty_CT> IfProperty
        {
            get
            {
                List<IFProperty_CT> result = null;
                if (InternalObj?.getIFProperty()?.toArray() != null)
                {
                    result = new List<IFProperty_CT>();

                    foreach (var obj in InternalObj.getIFProperty().toArray())
                    {
                        var objType = obj;
                        if (objType is fi.tut.mei.resdescapi.rdapi.model.rd.IFPropertyCT)
                        {
                            result.Add(
                                new IFProperty_CT((fi.tut.mei.resdescapi.rdapi.model.rd.IFPropertyCT)objType));
                        }
                    }
                }
                return result;
            }
        }
 

        [DisplayName(@"IdRefStdBody")]
        public string IdRefStdBody => InternalObj?.getIdRefStdBody()?.ToString();



        public static List<InterfaceLink> Get(object[] array)
        {
            var result = new List<InterfaceLink>();
            if (array == null)
                return result;
            try
            {
                result = array.OfType<fi.tut.mei.resdescapi.rdapi.model.rd.InterfaceLink>()
                    .Select(objType => new InterfaceLink(objType))
                    .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}