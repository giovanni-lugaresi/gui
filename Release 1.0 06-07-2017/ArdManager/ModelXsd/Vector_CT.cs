using System.ComponentModel;
using ikvm.extensions;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"Vector_CT")]

    public class Vector_CT
    {
        public Vector_CT()
        {
            
        }
        public Vector_CT(
            fi.tut.mei.resdescapi.rdapi.model.rd.VectorCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.VectorCT InternalObj { get; }

        [DisplayName(@"X")]
        public string X => InternalObj?.getX().toString();

        [DisplayName(@"Y")]
        public string Y => InternalObj?.getY().toString();

        [DisplayName(@"Z")]
        public string Z => InternalObj?.getZ().toString();
    }
}