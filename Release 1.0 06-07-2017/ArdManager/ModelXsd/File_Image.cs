﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"File_Image")]
    public class File_Image
    {
        public File_Image()
        {
            
        }
        public override string ToString()
        {
            return Name;
        }

        public File_Image(fi.tut.mei.resdescapi.rdapi.model.rd.FileImage resourceDescription)
        {
            InternalObj = resourceDescription;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.FileImage InternalObj { get; }

        [DisplayName(@"XHTML")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public XHTML_Doc_CT Xhtml =>
            InternalObj?.getXHTML() != null
                ? new XHTML_Doc_CT(InternalObj?.getXHTML())
                : null;


        [DisplayName(@"Url")]
        public string Url
        {
            get { return InternalObj?.getURL(); }
            set { InternalObj?.setURL(value); }
        }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }

        [DisplayName(@"Image_formats_ST")]
        public string ImageFormatsSt => InternalObj?.getFormat()?.value();

        public static List<File_Image> Get(object[] array)
        {
            var result = new List<File_Image>();
            if (array == null)
                return result;
            try
            {
                result = array.OfType<fi.tut.mei.resdescapi.rdapi.model.rd.FileImage>()
                    .Select(objType => new File_Image(objType))
                    .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}