using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"Version_CT")]
    public class Version_CT
    {
        public Version_CT()
        {
            
        }
        public Version_CT(fi.tut.mei.resdescapi.rdapi.model.rd.VersionCT resourceDescription)
        {
            InternalObj = resourceDescription;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.VersionCT InternalObj { get; }
 

        [DisplayName(@"VersionNumber")]
        public string VersionNumber
        {
            get { return InternalObj?.getVersionNumber(); }
            set { InternalObj?.setVersionNumber(value); }
        }

        [DisplayName(@"DateTime")]
        public string DateTime => InternalObj?.getDateTime().toString();
    }
}