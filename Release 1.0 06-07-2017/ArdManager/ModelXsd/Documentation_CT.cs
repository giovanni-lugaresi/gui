﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"DocumentationCT")]
    public class Documentation_CT
    {
        public Documentation_CT()
        {
            
        }
        public override string ToString()
        {
            return Name;
        }

        public Documentation_CT(fi.tut.mei.resdescapi.rdapi.model.rd.DocumentationCT internalObj)
        {
            InternalObj = internalObj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.DocumentationCT InternalObj { get; }

        [DisplayName(@"Links")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<SingleLinkType_CT> Links => SingleLinkType_CT.Get(InternalObj?.getLink()?.toArray());

        [DisplayName(@"XHTMLs")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<XHTML_Doc_CT> Xhtmls => XHTML_Doc_CT.Get(InternalObj?.getXHTML()?.toArray());

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }

        [DisplayName(@"Type")]
        public string Type
        {
            get { return InternalObj?.getType(); }
            set { InternalObj?.setType(value); }
        }

        public static List<Documentation_CT> Get(object[] array)
        {
            var result = new List<Documentation_CT>();
            if (array == null)
                return result;
            try
            {
                result = array.OfType<fi.tut.mei.resdescapi.rdapi.model.rd.DocumentationCT>()
                    .Select(objType => new Documentation_CT(objType))
                    .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}