﻿using System.Collections.Generic;
using System.ComponentModel;
using javax.print.attribute.standard;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"Resource CT")]
    public class Resource_CT
    {
        public Resource_CT()
        {
            
        }
        public Resource_CT(fi.tut.mei.resdescapi.rdapi.model.rd.ResourceCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ResourceCT InternalObj { get; }



        [DisplayName(@"GeneralInfo")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public ModuleInfo_CT GeneralInfo
            =>
                InternalObj?.getGeneralInfo() != null
                    ? new ModuleInfo_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.ModuleInfoCT)
                            InternalObj.getGeneralInfo())
                    : null;

        [DisplayName(@"InterfacesInfo")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public InterfacesForRD_CT InterfacesInfo
            =>
                InternalObj?.getInterfacesInfo() != null
                    ? new InterfacesForRD_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.InterfacesForRDCT) InternalObj.getInterfacesInfo())
                    : null;

        [DisplayName(@"Capabilities")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public Resource_CTCapabilities Capabilities
            =>
                InternalObj?.getCapabilities() != null
                    ? new Resource_CTCapabilities(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.ResourceCT.Capabilities) InternalObj.getCapabilities())
                    : null;

        [DisplayName(@"ExecCapabilities")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public ExecCapabilities_CT ExecCapabilities
            =>
                InternalObj?.getExecCapabilities() != null
                    ? new ExecCapabilities_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.ExecCapabilitiesCT) InternalObj.getExecCapabilities())
                    : null;

        [DisplayName(@"SharedResources")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public SharedResource_CT SharedResources
            =>
                InternalObj?.getSharedResources() != null
                    ? new SharedResource_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.SharedResourceCT) InternalObj.getSharedResources())
                    : null;

        [DisplayName(@"Properties")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public PropertiesForRD_CT Properties
            =>
                InternalObj?.getProperties() != null
                    ? new PropertiesForRD_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT) InternalObj.getProperties())
                    : null;

        //todo:
        //[DisplayName(@"GUIsInfo")]
        //[TypeConverter(typeof(ExpandableObjectConverter))]
        //public Resource_CTGUIsInfo GUIsInfo
        //   =>
        //       InternalObj?.getGUIsInfo() != null
        //           ? new GUIDefinition_CT(
        //               (fi.tut.mei.resdescapi.rdapi.model.rd.ResourceCT.GUIsInfo)InternalObj.getGUIsInfo())
        //           : null;


        [DisplayName(@"Calibration")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public Resource_CTCalibration Calibration
            =>
                InternalObj?.getCalibration() != null
                    ? new Resource_CTCalibration(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.ResourceCT.Calibration) InternalObj.getCalibration())
                    : null;

        [DisplayName(@"TestProcedures")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public Resource_CTTestProcedures TestProcedures
            =>
                InternalObj?.getTestProcedures() != null
                    ? new Resource_CTTestProcedures(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.ResourceCT.TestProcedures) InternalObj.getTestProcedures())
                    : null;

        [DisplayName(@"HardwareVersion")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public Version_CT HardwareVersion
            =>
                InternalObj?.getHardwareVersion() != null
                    ? new Version_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.VersionCT) InternalObj.getHardwareVersion())
                    : null;

        [DisplayName(@"Extension")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<ExtensionDataCt> Extension
            => ExtensionDataCt.Get(InternalObj?.getExtension()?.toArray());

        [DisplayName(@"Category")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public ModuleCategory_ST Category
            =>
                InternalObj?.getCategory() != null
                    ? new ModuleCategory_ST(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.ModuleCategoryST) InternalObj.getCategory())
                    : null;
           

        #region Simple Properties

        [DisplayName(@"Id")]
        public string Id
        {
            get { return InternalObj?.getId(); }
            set { InternalObj?.setId(value); }
        }

        [DisplayName(@"Gid")]
        public string Gid
        {
            get { return InternalObj?.getGid(); }
            set { InternalObj?.setGid(value); }
        }

        [DisplayName(@"Url")]
        public string Url
        {
            get { return InternalObj?.getURL(); }
            set { InternalObj?.setURL(value); }
        }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }

        [DisplayName(@"ArdIdRef")]
        public string ArdIdRef
        {
            get { return InternalObj?.getArdIDRef(); }
            set { InternalObj?.setArdProfileIDRef(value); }
        }

        [DisplayName(@"ArdGidRef")]
        public string ArdGidRef
        {
            get { return InternalObj?.getArdGIDRef(); }
            set { InternalObj?.setArdGIDRef(value); }
        }

        [DisplayName(@"ArdProfileIdRef")]
        public string ArdProfileIdRef
        {
            get { return InternalObj?.getArdProfileIDRef(); }
            set { InternalObj?.setArdProfileIDRef(value); }
        }

        [DisplayName(@"ArdUrl")]
        public string ArdUrl
        {
            get { return InternalObj?.getArdURL(); }
            set { InternalObj?.setArdURL(value); }
        }

        #endregion
    }
}