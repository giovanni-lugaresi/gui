using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ServiceResourceDesc_CT")]

    public  class ServiceResourceDesc_CT : ResourceDescHWInterface_CT
    {
        public ServiceResourceDesc_CT()
        {
            
        }
        public ServiceResourceDesc_CT(fi.tut.mei.resdescapi.rdapi.model.rd.ServiceResourceDescCT obj):base(obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ServiceResourceDescCT InternalObj { get; }
    }
}