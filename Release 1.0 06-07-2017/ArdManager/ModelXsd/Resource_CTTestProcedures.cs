﻿using System.Collections.Generic;
using System.ComponentModel;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"Resource_CTTestProcedures")]

    public class Resource_CTTestProcedures
    {
        public Resource_CTTestProcedures()
        {
            
        }
        public Resource_CTTestProcedures(fi.tut.mei.resdescapi.rdapi.model.rd.ResourceCT.TestProcedures obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ResourceCT.TestProcedures InternalObj { get; }

        [DisplayName(@"Labels")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<LocalizedString_CT> Labels => LocalizedString_CT.Get(InternalObj?.getLabel()?.toArray());

        [DisplayName(@"Comments")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<LocalizedString_CT> Comments => LocalizedString_CT.Get(InternalObj?.getComment()?.toArray());

        [DisplayName(@"Documentations")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<Documentation_CT> Documentations => Documentation_CT.Get(InternalObj?.getDocumentation()?.toArray());
    }
}