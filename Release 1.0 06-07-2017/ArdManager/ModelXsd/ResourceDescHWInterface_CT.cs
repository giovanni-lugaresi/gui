using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ResourceDescHWInterface_CT")]
    public  class ResourceDescHWInterface_CT
    {
        public ResourceDescHWInterface_CT()
        {
            
        }

        public ResourceDescHWInterface_CT(fi.tut.mei.resdescapi.rdapi.model.rd.ResourceDescHWInterfaceCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ResourceDescHWInterfaceCT InternalObj { get; }

        private InterfaceImplRD_CT[] interfacesField;

        private ExtensionDataCt extensionField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Interface", IsNullable = false)]
        public InterfaceImplRD_CT[] Interfaces
        {
            get { return this.interfacesField; }
            set { this.interfacesField = value; }
        }

        /// <remarks/>
        public ExtensionDataCt Extension
        {
            get { return this.extensionField; }
            set { this.extensionField = value; }
        }
    }
}