using System.Collections.Generic;
using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"LinkedParamPorts_CT")]
    public class LinkedParamPorts_CT
    {
        public LinkedParamPorts_CT()
        {
            
        }
        public LinkedParamPorts_CT(fi.tut.mei.resdescapi.rdapi.model.rd.LinkedParamPortsCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.LinkedParamPortsCT InternalObj { get; }

        [DisplayName(@"LeadsToOutputEvent")]
        public List<LinkedParamPorts_CTParamPort> ParamPort
        {
            get
            {
                List<LinkedParamPorts_CTParamPort> result = null;
                if (InternalObj?.getParamPort()?.toArray() != null)
                {
                    result = new List<LinkedParamPorts_CTParamPort>();

                    foreach (var obj in InternalObj.getParamPort().toArray())
                    {
                        var objType = obj;
                        if (objType is fi.tut.mei.resdescapi.rdapi.model.rd.LinkedParamPortsCT.ParamPort)
                        {
                            result.Add(
                                new LinkedParamPorts_CTParamPort(
                                    (fi.tut.mei.resdescapi.rdapi.model.rd.LinkedParamPortsCT.ParamPort)
                                        objType));
                        }
                    }
                }
                return result;
            }
        }
    }
}