﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"DataTypes_CTStringType")]
    public class DataTypes_CTStringType
    {
        public DataTypes_CTStringType()
        {
            
        }
        public override string ToString()
        {
            return Name;
        }

        public DataTypes_CTStringType(fi.tut.mei.resdescapi.rdapi.model.rd.DataTypesCT.StringType obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.DataTypesCT.StringType InternalObj { get; }

        [DisplayName(@"Id")]
        public string Id
        {
            get { return InternalObj?.getId(); }
            set { InternalObj?.setId(value); }
        }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }

        [DisplayName(@"ArrayLength")]
        public string ArrayLength => InternalObj?.getArrayLength().ToString();



        public static List<DataTypes_CTStringType> Get(object[] array)
        {
            var result = new List<DataTypes_CTStringType>();
            if (array == null)
                return result;
            try
            {
                result =
                    array
                        .OfType
                        <
                            fi.tut.mei.resdescapi.rdapi.model.rd.DataTypesCT.StringType>()
                        .Select(objType => new DataTypes_CTStringType(objType))
                        .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}