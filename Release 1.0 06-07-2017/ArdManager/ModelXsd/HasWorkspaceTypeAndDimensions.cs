using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"hasWorkspaceTypeAndDimensions")]
    public class HasWorkspaceTypeAndDimensions
    {
        public HasWorkspaceTypeAndDimensions()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public HasWorkspaceTypeAndDimensions(
            fi.tut.mei.resdescapi.rdapi.model.rd.HasWorkspaceTypeAndDimensions
                obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.HasWorkspaceTypeAndDimensions
            InternalObj { get; }

        [DisplayName(@"WorkspaceCylinder")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public WorkspaceCylinder_CT WorkspaceCylinder
            =>
                InternalObj?.getWorkspaceCylinder() != null
                    ? new WorkspaceCylinder_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.WorkspaceCylinderCT)
                            InternalObj.getWorkspaceCylinder())
                    : null;

        [DisplayName(@"WorkspacePolar")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public WorkspacePolar_CT WorkspacePolar
            =>
                InternalObj?.getWorkspacePolar() != null
                    ? new WorkspacePolar_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.WorkspacePolarCT)
                            InternalObj.getWorkspacePolar())
                    : null;



        [DisplayName(@"WorkspaceArticulated")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public WorkspaceArticulated_CT WorkspaceArticulatedCt
            =>
                InternalObj?.getWorkspaceArticulated() != null
                    ? new WorkspaceArticulated_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.WorkspaceArticulatedCT)
                            InternalObj.getWorkspaceArticulated())
                    : null;

        [DisplayName(@"WorkspaceCartesian")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public WorkspaceCartesian_CT WorkspaceCartesianCt
            =>
                InternalObj?.getWorkspaceCartesian() != null
                    ? new WorkspaceCartesian_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.WorkspaceCartesianCT)
                            InternalObj.getWorkspaceCartesian())
                    : null;
    }
}