using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"WorkspaceCartesian_CT")]

    public class WorkspaceCartesian_CT
    {
        public WorkspaceCartesian_CT()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public WorkspaceCartesian_CT(
            fi.tut.mei.resdescapi.rdapi.model.rd.WorkspaceCartesianCT
                obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.WorkspaceCartesianCT
            InternalObj
        { get; }

        [DisplayName(@"dimension_x")]
        public string DimensionX
        {
            get { return InternalObj?.getDimensionX(); }
            set { InternalObj?.setDimensionX(value); }
        }
        [DisplayName(@"dimension_y")]
        public string DimensionY
        {
            get { return InternalObj?.getDimensionY(); }
            set { InternalObj?.setDimensionY(value); }
        }
        [DisplayName(@"dimension_z")]
        public string DimensionZ
        {
            get { return InternalObj?.getDimensionZ(); }
            set { InternalObj?.setDimensionZ(value); }
        }
    }
}