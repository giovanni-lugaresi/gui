using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ShapeBox_CT")]

    public class ShapeBox_CT
    {
        public ShapeBox_CT()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public ShapeBox_CT(
            fi.tut.mei.resdescapi.rdapi.model.rd.ShapeBoxCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ShapeBoxCT InternalObj { get; }


        [DisplayName(@"dimension_x")]
        public string DimensionX
        {
            get { return InternalObj?.getDimensionX(); }
            set { InternalObj?.setDimensionX(value); }
        }

        [DisplayName(@"dimension_y")]
        public string DimensionY
        {
            get { return InternalObj?.getDimensionY(); }
            set { InternalObj?.setDimensionY(value); }
        }

        [DisplayName(@"dimension_z")]
        public string DimensionZ
        {
            get { return InternalObj?.getDimensionZ(); }
            set { InternalObj?.setDimensionZ(value); }
        }


        public static List<ShapeBox_CT> Get(object[] array)
        {
            var result = new List<ShapeBox_CT>();
            if (array == null)
                return result;
            try
            {
                result = array.OfType<fi.tut.mei.resdescapi.rdapi.model.rd.ShapeBoxCT>()
                    .Select(objType => new ShapeBox_CT(objType))
                    .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}