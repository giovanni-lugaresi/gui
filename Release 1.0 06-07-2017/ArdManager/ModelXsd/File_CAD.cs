﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"File_CAD")]
    public class File_CAD
    {
        public File_CAD()
        {
            
        }
        public override string ToString()
        {
            return Name;
        }

        public File_CAD(fi.tut.mei.resdescapi.rdapi.model.rd.FileCAD resourceDescription)
        {
            InternalObj = resourceDescription;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.FileCAD InternalObj { get; }

        [DisplayName(@"XHTML")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public XHTML_Doc_CT Xhtml =>
            InternalObj?.getXHTML() != null
                ? new XHTML_Doc_CT(InternalObj?.getXHTML())
                : null;

        [DisplayName(@"Url")]
        public string Url
        {
            get { return InternalObj?.getURL(); }
            set { InternalObj?.setURL(value); }
        }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }

        [DisplayName(@"CAD_formats_ST")]
        public string CadFormatsSt => InternalObj?.getFormat()?.value();


        public static List<File_CAD> Get(object[] array)
        {
            var result = new List<File_CAD>();
            if (array == null)
                return result;
            try
            {
                result = array.OfType<fi.tut.mei.resdescapi.rdapi.model.rd.FileCAD>()
                    .Select(objType => new File_CAD(objType))
                    .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}