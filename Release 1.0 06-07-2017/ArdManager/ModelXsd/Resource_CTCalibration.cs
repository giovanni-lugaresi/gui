﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using com.sun.corba.se.impl.protocol.giopmsgheaders;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"Resource Calibration")]
    public class Resource_CTCalibration
    { 
        public Resource_CTCalibration()
        {

        }

        public Resource_CTCalibration(fi.tut.mei.resdescapi.rdapi.model.rd.ResourceCT.Calibration obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ResourceCT.Calibration InternalObj { get; }

        private ObservableCollection<LocalizedString_CT> _labels;

        [DisplayName(@"Labels")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public ObservableCollection<LocalizedString_CT> Labels
        {
            get
            {
                if (_labels == null)
                {
                    _labels = new ObservableCollection<LocalizedString_CT>(LocalizedString_CT.Get(InternalObj?.getLabel()?.toArray()));
                    _labels.CollectionChanged += _labels_CollectionChanged;
                }
                return _labels;
            }
        }

        private void _labels_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            //var array = _labels.ToArray();
            //MessageBox.Show($"_labels.ToArray() count: {array.Count()}" );
        }

        private List<LocalizedString_CT> _comments;
        [DisplayName(@"Comments")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<LocalizedString_CT> Comments
        {
            get
            {
                if (_comments == null)
                {
                    _comments = LocalizedString_CT.Get(InternalObj?.getComment()?.toArray());
                }
                return _comments;
            }
        }

        private List<Documentation_CT> _documentations;
        [DisplayName(@"Documentations")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<Documentation_CT> Documentations
        {
            get
            {
                if (_documentations == null)
                {
                    _documentations = Documentation_CT.Get(InternalObj?.getDocumentation()?.toArray());
                }
                return _documentations;
            }
        }

    }
}
