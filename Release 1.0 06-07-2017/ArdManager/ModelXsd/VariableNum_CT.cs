using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"VariableNum_CT")]
    public class VariableNum_CT : Variable_CT_wComplex
    {
        public VariableNum_CT()
        {
            
        }
        public VariableNum_CT(fi.tut.mei.resdescapi.rdapi.model.rd.VariableNumCT obj) : base(obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.VariableNumCT InternalObj { get; }

        [DisplayName(@"Min")]
        public string Min => InternalObj?.getMin().ToString();

        [DisplayName(@"Max")]
        public string Max => InternalObj?.getMax().toString();

        [DisplayName(@"DefaultValue")]
        public string DefaultValue => InternalObj?.getDefaultValue().toString();

        [DisplayName(@"Value")]
        public string Value => InternalObj?.getValue().toString();


        public static List<VariableNum_CT> Get(object[] array)
        {
            var result = new List<VariableNum_CT>();
            if (array == null)
                return result;
            try
            {
                result =
                    array
                        .OfType
                        <
                            fi.tut.mei.resdescapi.rdapi.model.rd.VariableNumCT>()
                        .Select(objType => new VariableNum_CT(objType))
                        .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}