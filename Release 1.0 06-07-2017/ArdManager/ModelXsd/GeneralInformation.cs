﻿using System.ComponentModel;
using System;

namespace ArdManager.ModelXsd
{
    public class GeneralInformation
    {
        public GeneralInformation()
        {
            
        }
        public GeneralInformation(fi.tut.mei.resdescapi.rdapi.model.rd.ResourceDescription resourceDescription)
        {
            InternalObj = resourceDescription;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ResourceDescription InternalObj { get; }

        #region Simple Properties

        public string SerialNumber
        {
            get { return InternalObj?.getSerialNumber()?.toString(); }
            //set { this.serialNumberField = value; }
        }

        public string Phone
        {
            get { return InternalObj?.getPhone(); }
            set { InternalObj?.setPhone(value); }
        }

        public string SchemaVersion
        {
            get { return InternalObj?.getSchemaVersion(); }
            set { InternalObj?.setSchemaVersion(value); }
        }

        public string Version
        {
            get { return InternalObj?.getVersion(); }
            set { InternalObj?.setVersion(value); }
        }

        public string Id
        {
            get { return InternalObj?.getId(); }
            set { InternalObj?.setId(value); }
        }

        public string Gid
        {
            get { return InternalObj?.getGid(); }
            set { InternalObj?.setGid(value); }
        }

        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }

        public string Url
        {
            get { return InternalObj?.getURL(); }
            set { InternalObj?.setURL(value); }
        }

        public string Author
        {
            get { return InternalObj?.getAuthor(); }
            set { InternalObj?.setAuthor(value); }
        }

        public string Email
        {
            get { return InternalObj?.getEmail(); }
            set { InternalObj?.setEmail(value); }
        }

        public string Company
        {
            get { return InternalObj?.getCompany(); }
            set { InternalObj?.setCompany(value); }
        }

        public string Created
        {
            get { return InternalObj?.getCreated().toString(); }
            //set { this.createdField = value; }
        }
        //System.Console.WriteLine("Prova")
        public string Modified
        {
            get { return InternalObj?.getModified().toString(); }
            //set { this.modifiedField = value; }  
        }
        // eccezione qui (GL)
        #endregion
    }
}