using System.Collections.Generic;
using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"SymbolRefTypePort_CT")]
    public class SymbolRefTypePort_CT
    {
        public SymbolRefTypePort_CT()
        {
            
        }
        public override string ToString()
        {
            return IdRef;
        }

        public SymbolRefTypePort_CT(fi.tut.mei.resdescapi.rdapi.model.rd.SymbolRefTypePortCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.SymbolRefTypePortCT InternalObj { get; }


        [DisplayName(@"IdRef")]
        public string IdRef
        {
            get { return InternalObj?.getIdRef(); }
            set { InternalObj?.setIdRef(value); }
        }

        [DisplayName(@"ReqOpt")]
        public string ReqOpt => InternalObj?.getReqOpt()?.value();

        public string ConnectionMethod => InternalObj?.getConnectionMethod()?.value();
    }
}