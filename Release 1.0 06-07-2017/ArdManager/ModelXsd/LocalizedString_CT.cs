﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 

namespace ArdManager.ModelXsd
{
    [DisplayName(@"LocalizedStringCT")]
    public class LocalizedString_CT
    {
        public LocalizedString_CT()
        {
            
        }
        public override string ToString()
        {
            return Label;
        }

        public LocalizedString_CT(fi.tut.mei.resdescapi.rdapi.model.rd.LocalizedStringCT internalObj)
        {
            InternalObj = internalObj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.LocalizedStringCT InternalObj { get; }

        [DisplayName(@"Lang")]
        public string Lang
        {
            get { return InternalObj?.getLang(); }
            set { InternalObj?.setLang(value); }
        }

        [DisplayName(@"Label")]
        public string Label
        {
            get { return InternalObj?.getValue(); }
            set { InternalObj?.setValue(value); }
        }


        public static List<LocalizedString_CT> Get(object[] array)
        {
            var result = new List<LocalizedString_CT>();
            if (array == null)
                return result;
            try
            {
                result = array.OfType<fi.tut.mei.resdescapi.rdapi.model.rd.LocalizedStringCT>()
                    .Select(objType => new LocalizedString_CT(objType))
                    .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}
