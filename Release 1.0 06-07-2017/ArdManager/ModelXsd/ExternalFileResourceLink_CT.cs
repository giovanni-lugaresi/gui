using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ExternalFileResourceLink_CT")]
    public class ExternalFileResourceLink_CT : ExternalResourceLink_CT
    {
        public ExternalFileResourceLink_CT()
        {
            
        }
        public override string ToString()
        {
            return FormatGid;
        }

        public ExternalFileResourceLink_CT(fi.tut.mei.resdescapi.rdapi.model.rd.ExternalFileResourceLinkCT obj):base(obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ExternalFileResourceLinkCT InternalObj { get; }

        [DisplayName(@"formatGID")]
        public string FormatGid
        {
            get { return InternalObj?.getFormatGID(); }
            set { InternalObj?.setFormatGID(value); }
        }

        [DisplayName(@"format")]
        public string Format
        {
            get { return InternalObj?.getFormat(); }
            set { InternalObj?.setFormat(value); }
        }
    }
}