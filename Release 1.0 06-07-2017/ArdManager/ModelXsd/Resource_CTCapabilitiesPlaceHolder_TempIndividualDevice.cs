using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"Resource_CTCapabilitiesPlaceHolder_TempIndividualDevice")]

    public class Resource_CTCapabilitiesPlaceHolder_TempIndividualDevice
    {
        public Resource_CTCapabilitiesPlaceHolder_TempIndividualDevice()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public Resource_CTCapabilitiesPlaceHolder_TempIndividualDevice(fi.tut.mei.resdescapi.rdapi.model.rd.ResourceCT.Capabilities.PlaceHolderTemp.IndividualDevice obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ResourceCT.Capabilities.PlaceHolderTemp.IndividualDevice InternalObj { get; }

       
        [DisplayName(@"Any")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<string> Any
        {
            get
            {
                List<string> result = null;
                if (InternalObj?.getAny()?.toArray() != null)
                {
                    result = InternalObj.getAny().toArray().Cast<string>().ToList();
                }
                return result;
            }
        }
    }
}