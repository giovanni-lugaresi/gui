using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"DH_Parameter_CTParameter")]

    public class DH_Parameter_CTParameter
    {
        public DH_Parameter_CTParameter()
        {
            
        }
        public DH_Parameter_CTParameter(fi.tut.mei.resdescapi.rdapi.model.rd.DHParameterCT.Parameter obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.DHParameterCT.Parameter InternalObj { get; }
 
        [DisplayName(@"associatedWith")]
        public string AssociatedWith => InternalObj?.getAssociatedWith();

        [DisplayName(@"IdRef")]
        public string IdRef => InternalObj?.getIdRef().ToString();
 
        [DisplayName(@"parOperator")]
        public string ParOperator => InternalObj?.getParOperator();

        [DisplayName(@"parCoefficient")]
        public string ParCoefficient => InternalObj?.getParCoefficient().toString();

        public static List<DH_Parameter_CTParameter> Get(object[] array)
        {
            var result = new List<DH_Parameter_CTParameter>();
            if (array == null)
                return result;
            try
            {
                result =
                    array
                        .OfType
                        <
                            fi.tut.mei.resdescapi.rdapi.model.rd.DHParameterCT.Parameter>()
                        .Select(objType => new DH_Parameter_CTParameter(objType))
                        .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}