using System.ComponentModel;
using ikvm.extensions;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"DOF6_dataLimits_CT")]

    public class DOF6_dataLimits_CT
    {
        public DOF6_dataLimits_CT()
        {
            
        }
        public DOF6_dataLimits_CT(fi.tut.mei.resdescapi.rdapi.model.rd.DOF6DataLimitsCT resourceDescription)
        {
            InternalObj = resourceDescription;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.DOF6DataLimitsCT InternalObj { get; }

        [DisplayName(@"Min")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public DOF6_data_Basic_CT Min
            =>
                InternalObj?.getMin() != null
                    ? new DOF6_data_Basic_CT(InternalObj?.getMin())
                    : null;

        [DisplayName(@"Max")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public DOF6_data_Basic_CT Max
            =>
                InternalObj?.getMax() != null
                    ? new DOF6_data_Basic_CT(InternalObj?.getMax())
                    : null;


        [DisplayName(@"Unit")]
        public string Unit
        {
            get { return InternalObj?.getUnit(); }
            set { InternalObj?.setUnit(value); }
        }

        [DisplayName(@"Decade")]
        public string Decade => InternalObj?.getDecade().toString();

        [DisplayName(@"Increment")]
        public string Increment => InternalObj?.getIncrement().toString();


        [DisplayName(@"RotUnit")]
        public string RotUnit
        {
            get { return InternalObj?.getRotUnit(); }
            set { InternalObj?.setRotUnit(value); }
        }

        [DisplayName(@"RotDecade")]
        public string RotDecade => InternalObj?.getRotDecade().toString();

        [DisplayName(@"RotIncrement")]
        public string RotIncrement => InternalObj?.getRotIncrement().toString();

    }
}