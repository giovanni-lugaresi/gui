﻿using System.Collections.Generic;
using System.ComponentModel;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"PropertiesForRD_CTTechnicalProperties")]

    public class PropertiesForRD_CTTechnicalProperties
    {
        public PropertiesForRD_CTTechnicalProperties()
        {
            
        }
        public PropertiesForRD_CTTechnicalProperties(fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.TechnicalProperties resourceDescription)
        {
            InternalObj = resourceDescription;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.TechnicalProperties InternalObj { get; }

        [DisplayName(@"Variable")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<Variable_CT_wComplex> Variable => Variable_CT_wComplex.Get(InternalObj?.getVariable()?.toArray());

        [DisplayName(@"VariableRef")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<VariableRef_CT> VariableRef => VariableRef_CT.Get(InternalObj?.getVariableRef()?.toArray());
    }
}