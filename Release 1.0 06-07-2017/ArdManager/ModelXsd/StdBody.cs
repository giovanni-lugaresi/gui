using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"StdBody")]

    public   class StdBody
    {
        public StdBody()
        {
            
        }
        public override string ToString()
        {
            return Name;
        }
        public StdBody(fi.tut.mei.resdescapi.rdapi.model.rd.StdBody obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.StdBody InternalObj { get; }

        [DisplayName(@"XHTML")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public XHTML_Doc_CT Xhtml =>
            InternalObj?.getXHTML() != null
                ? new XHTML_Doc_CT(InternalObj?.getXHTML())
                : null;


        [DisplayName(@"Id")]
        public string Id
        {
            get { return InternalObj?.getId(); }
            set { InternalObj?.setId(value); }
        }

        [DisplayName(@"Gid")]
        public string Gid
        {
            get { return InternalObj?.getGid(); }
            set { InternalObj?.setGid(value); }
        }

        [DisplayName(@"Url")]
        public string Url
        {
            get { return InternalObj?.getURL(); }
            set { InternalObj?.setURL(value); }
        }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }



        public static List<StdBody> Get(object[] array)
        {
            var result = new List<StdBody>();
            if (array == null)
                return result;
            try
            {
                result = array.OfType<fi.tut.mei.resdescapi.rdapi.model.rd.StdBody>()
                    .Select(objType => new StdBody(objType))
                    .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}