using System.Collections.Generic;
using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"SymbolForEventPort_CT")]
    public class SymbolForEventPort_CT : Symbol_CT
    {
        public SymbolForEventPort_CT()
        {
            
        }
        public SymbolForEventPort_CT(fi.tut.mei.resdescapi.rdapi.model.rd.SymbolForEventPortCT obj):base(obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.SymbolForEventPortCT InternalObj { get; }

        [DisplayName(@"LinkedParamPorts")]

        public LinkedParamPorts_CT LinkedParamPorts
            =>
                InternalObj?.getLinkedParamPorts() != null
                    ? new LinkedParamPorts_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.LinkedParamPortsCT)
                            InternalObj.getLinkedParamPorts())
                    : null;

        [DisplayName(@"LeadsToOutputEvent")]
        public List<SymbolForEventPort_CTLeadsToOutputEvent> LeadsToOutputEvent
        {
            get
            {
                List<SymbolForEventPort_CTLeadsToOutputEvent> result = null;
                if (InternalObj?.getLeadsToOutputEvent()?.toArray() != null)
                {
                    result = new List<SymbolForEventPort_CTLeadsToOutputEvent>();

                    foreach (var obj in InternalObj.getLeadsToOutputEvent().toArray())
                    {
                        var objType = obj;
                        if (objType is fi.tut.mei.resdescapi.rdapi.model.rd.SymbolForEventPortCT.LeadsToOutputEvent)
                        {
                            result.Add(
                                new SymbolForEventPort_CTLeadsToOutputEvent(
                                    (fi.tut.mei.resdescapi.rdapi.model.rd.SymbolForEventPortCT.LeadsToOutputEvent)
                                        objType));
                        }
                    }
                }
                return result;
            }
        }
    }
}