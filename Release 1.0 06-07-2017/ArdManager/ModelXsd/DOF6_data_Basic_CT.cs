﻿using System.ComponentModel;
using ikvm.extensions;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"DOF6_data_Basic_CT")]

    public class DOF6_data_Basic_CT : DOF3_data_Basic_CT
    {
        public DOF6_data_Basic_CT()
        {
            
        }
        public DOF6_data_Basic_CT(fi.tut.mei.resdescapi.rdapi.model.rd.DOF6DataBasicCT obj) : base(obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.DOF6DataBasicCT InternalObj { get; }

        [DisplayName(@"RotX")]
        public string RotX => InternalObj?.getRotX().toString();

        [DisplayName(@"RotY")]
        public string RotY => InternalObj?.getRotY().toString();

        [DisplayName(@"Z")]
        public string RotZ => InternalObj?.getRotZ().toString();
    }
}