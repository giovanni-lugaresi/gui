using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"MechanicalResourceDesc_CT")]

    public  class MechanicalResourceDesc_CT : ResourceDescHWInterface_CT
    {
        public MechanicalResourceDesc_CT()
        {
            
        }
        public MechanicalResourceDesc_CT(fi.tut.mei.resdescapi.rdapi.model.rd.MechanicalResourceDescCT obj) : base(obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.MechanicalResourceDescCT InternalObj { get; }
    }
}