using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ItemSize_CT")]
    public class ItemSize_CT
    {
        public ItemSize_CT()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public ItemSize_CT(
            fi.tut.mei.resdescapi.rdapi.model.rd.ItemSizeCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ItemSizeCT InternalObj { get; }

        [DisplayName(@"diameter")]
        public string Diameter
        {
            get { return InternalObj?.getDiameter(); }
            set { InternalObj?.setDiameter(value); }
        }

        [DisplayName(@"width")]
        public string Width
        {
            get { return InternalObj?.getWidth(); }
            set { InternalObj?.setWidth(value); }
        }

        [DisplayName(@"length")]
        public string Length
        {
            get { return InternalObj?.getLength(); }
            set { InternalObj?.setLength(value); }
        }

        [DisplayName(@"height")]
        public string Height
        {
            get { return InternalObj?.getHeight(); }
            set { InternalObj?.setHeight(value); }
        }

        [DisplayName(@"weight")]
        public string Weight
        {
            get { return InternalObj?.getWeight(); }
            set { InternalObj?.setWeight(value); }
        }
    }
}