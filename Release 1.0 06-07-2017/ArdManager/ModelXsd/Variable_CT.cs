using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"Comment")]
    public class Variable_CT : VariableBase_CT
    {
        public Variable_CT()
        {
            
        }
        public Variable_CT(fi.tut.mei.resdescapi.rdapi.model.rd.VariableCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.VariableCT InternalObj { get; }

        [DisplayName(@"Id")]
        public string Id
        {
            get { return InternalObj?.getId(); }
            set { InternalObj?.setId(value); }
        }

        [DisplayName(@"Datatype")]
        public string Datatype => InternalObj?.getDatatype()?.value();

        [DisplayName(@"ReqOpt")]
        public string ReqOpt => InternalObj?.getReqOpt()?.value();
    }
}