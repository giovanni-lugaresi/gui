﻿using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"PropertiesForRD_CTPhysicalPropertiesInstallationPositions")]

    public class PropertiesForRD_CTPhysicalPropertiesInstallationPositions
    {
        public PropertiesForRD_CTPhysicalPropertiesInstallationPositions()
        {
            
        }
        public PropertiesForRD_CTPhysicalPropertiesInstallationPositions(
            fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.PhysicalProperties.InstallationPositions
                resourceDescription)
        {
            InternalObj = resourceDescription;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.PhysicalProperties.InstallationPositions
            InternalObj { get; }

        [DisplayName(@"AccelerationLimits")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public DOF6_dataLimits_CT AccelerationLimits
            =>
                InternalObj?.getAccelerationLimits() != null
                    ? new DOF6_dataLimits_CT(InternalObj?.getAccelerationLimits())
                    : null;


        [DisplayName(@"DescriptionLimitations")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public XHTML_Doc_CT DescriptionLimitations =>
            InternalObj?.getDescriptionLimitations() != null
                ? new XHTML_Doc_CT(InternalObj?.getDescriptionLimitations())
                : null;
    }
}