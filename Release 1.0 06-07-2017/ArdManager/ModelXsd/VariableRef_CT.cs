using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"VariableRef_CT")]

    public class VariableRef_CT
    {
        public VariableRef_CT()
        {
            
        }
        public VariableRef_CT(fi.tut.mei.resdescapi.rdapi.model.rd.VariableRefCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.VariableRefCT InternalObj { get; }

        [DisplayName(@"IdRef")]
        public string IdRef => InternalObj?.getIdRef().ToString();

        [DisplayName(@"ReqOpt")]
        public string ReqOpt => InternalObj?.getReqOpt()?.value();

        [DisplayName(@"Decade")]
        public string Decade => InternalObj?.getDecade()?.toString();

        [DisplayName(@"Increment")]
        public string Increment => InternalObj?.getIncrement()?.toString();

        [DisplayName(@"Max")]
        public string Max
        {
            get { return InternalObj?.getMax(); }
            set { InternalObj?.setMax(value); }
        }

        [DisplayName(@"Min")]
        public string Min
        {
            get { return InternalObj?.getMin(); }
            set { InternalObj?.setMin(value); }
        }

        [DisplayName(@"Value")]
        public string Value
        {
            get { return InternalObj?.getValue(); }
            set { InternalObj?.setValue(value); }
        }

        [DisplayName(@"DefaultValue")]
        public string DefaultValue
        {
            get { return InternalObj?.getDefaultValue(); }
            set { InternalObj?.setDefaultValue(value); }
        }

        public static List<VariableRef_CT> Get(object[] array)
        {
            var result = new List<VariableRef_CT>();
            if (array == null)
                return result;
            try
            {
                result =
                    array
                        .OfType
                        <
                            fi.tut.mei.resdescapi.rdapi.model.rd.VariableRefCT>()
                        .Select(objType => new VariableRef_CT(objType))
                        .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}