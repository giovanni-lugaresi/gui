﻿using System.Collections.Generic;
using System.ComponentModel;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"InterfacesForRD_CTInterfaceStds")]

    public   class InterfacesForRD_CTInterfaceStds
    {
        public InterfacesForRD_CTInterfaceStds()
        {
            
        }
        public InterfacesForRD_CTInterfaceStds(fi.tut.mei.resdescapi.rdapi.model.rd.InterfacesForRDCT.InterfaceStds obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.InterfacesForRDCT.InterfaceStds InternalObj { get; }


        [DisplayName(@"InterfaceLink")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<InterfaceLink> InterfaceLinklst
            =>
                InterfaceLink.Get(
                    InternalObj?.getInterfaceLink()?.toArray());

        [DisplayName(@"Interface")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<InterfaceStd_CT> Interface
            =>
                InterfaceStd_CT.Get(
                    InternalObj?.getInterface()?.toArray());
 
    }
}