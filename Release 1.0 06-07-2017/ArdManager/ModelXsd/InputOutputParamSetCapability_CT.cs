using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"InputOutputParamSetCapability_CT")]
    public class InputOutputParamSetCapability_CT
    {
        public InputOutputParamSetCapability_CT()
        {
            
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public InputOutputParamSetCapability_CT(fi.tut.mei.resdescapi.rdapi.model.rd.InputOutputParamSetCapabilityCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.InputOutputParamSetCapabilityCT InternalObj { get; }

        [DisplayName(@"Characteristics")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public InputOutputParamSetCapability_CTCharacteristics Characteristics
            =>
                InternalObj?.getCharacteristics() != null
                    ? new InputOutputParamSetCapability_CTCharacteristics(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.InputOutputParamSetCapabilityCT.Characteristics)
                            InternalObj.getCharacteristics())
                    : null;

        [DisplayName(@"Outputs")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public CapabilityParameterSet_CT Inputs
            =>
                InternalObj?.getInputs() != null
                    ? new CapabilityParameterSet_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.CapabilityParameterSetCT) InternalObj.getInputs())
                    : null;

        [DisplayName(@"Outputs")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public CapabilityParameterSet_CT Outputs
            =>
                InternalObj?.getOutputs() != null
                    ? new CapabilityParameterSet_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.CapabilityParameterSetCT) InternalObj.getOutputs())
                    : null;
    }
}