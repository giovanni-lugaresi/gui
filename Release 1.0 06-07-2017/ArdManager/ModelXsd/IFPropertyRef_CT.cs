﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"IFProperty_CT")]
    public class IFPropertyRef_CT : VariableRef_CT
    {
        public IFPropertyRef_CT()
        {
            
        }
        public IFPropertyRef_CT(fi.tut.mei.resdescapi.rdapi.model.rd.IFPropertyRefCT obj) : base(obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.IFPropertyRefCT InternalObj { get; }

        [DisplayName(@"IfCategory")]
        public string IfCategory => InternalObj?.getIfCategory()?.value();


        public static List<IFPropertyRef_CT> Get(object[] array)
        {
            var result = new List<IFPropertyRef_CT>();
            if (array == null)
                return result;
            try
            {
                result = array.OfType<fi.tut.mei.resdescapi.rdapi.model.rd.IFPropertyRefCT>()
                    .Select(objType => new IFPropertyRef_CT(objType))
                    .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}