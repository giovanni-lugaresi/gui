﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"IFProperty_CT")]

    public class IFProperty_CT : VariableNum_CT_noComplex
    {
        public IFProperty_CT()
        {
            
        }
        public IFProperty_CT(fi.tut.mei.resdescapi.rdapi.model.rd.IFPropertyCT obj) : base(obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.IFPropertyCT InternalObj { get; }

        [DisplayName(@"IfCategory")]
        public string IfCategory => InternalObj?.getIfCategory()?.value();



        public static List<IFProperty_CT> Get(object[] array)
        {
            var result = new List<IFProperty_CT>();
            if (array == null)
                return result;
            try
            {
                result = array.OfType<fi.tut.mei.resdescapi.rdapi.model.rd.IFPropertyCT>()
                    .Select(objType => new IFProperty_CT(objType))
                    .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}