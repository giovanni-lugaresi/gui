using System.Collections.Generic;
using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"InterfaceRef_CT")]
    public class InterfaceRef_CT
    {
        public InterfaceRef_CT()
        {
            
        }
        public InterfaceRef_CT(fi.tut.mei.resdescapi.rdapi.model.rd.InterfaceRefCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.InterfaceRefCT InternalObj { get; }

        [DisplayName(@"Class")]

        public List<IFClassOption_CT> Class
        {
            get
            {
                List<IFClassOption_CT> result = null;
                if (InternalObj?.getClazz()?.toArray() != null)
                {
                    result = new List<IFClassOption_CT>();

                    foreach (var obj in InternalObj.getClazz().toArray())
                    {
                        var objType = obj;
                        if (objType is fi.tut.mei.resdescapi.rdapi.model.rd.IFClassOptionCT)
                        {
                            result.Add(
                                new IFClassOption_CT((fi.tut.mei.resdescapi.rdapi.model.rd.IFClassOptionCT)objType));
                        }
                    }
                }
                return result;
            }
        }

        [DisplayName(@"Option")]
        public List<IFClassOption_CT> Option
        {
            get
            {
                List<IFClassOption_CT> result = null;
                if (InternalObj?.getOption()?.toArray() != null)
                {
                    result = new List<IFClassOption_CT>();

                    foreach (var obj in InternalObj.getOption().toArray())
                    {
                        var objType = obj;
                        if (objType is fi.tut.mei.resdescapi.rdapi.model.rd.IFClassOptionCT)
                        {
                            result.Add(
                                new IFClassOption_CT((fi.tut.mei.resdescapi.rdapi.model.rd.IFClassOptionCT)objType));
                        }
                    }
                }
                return result;
            }
        }

        [DisplayName(@"IFProperty")]

        public List<IFProperty_CT> IfProperty
        {
            get
            {
                List<IFProperty_CT> result = null;
                if (InternalObj?.getIFProperty()?.toArray() != null)
                {
                    result = new List<IFProperty_CT>();

                    foreach (var obj in InternalObj.getIFProperty().toArray())
                    {
                        var objType = obj;
                        if (objType is fi.tut.mei.resdescapi.rdapi.model.rd.IFPropertyCT)
                        {
                            result.Add(
                                new IFProperty_CT((fi.tut.mei.resdescapi.rdapi.model.rd.IFPropertyCT)objType));
                        }
                    }
                }
                return result;
            }
        }

        [DisplayName(@"IFPropertyRef")]

        public List<IFPropertyRef_CT> IfPropertyRef
        {
            get
            {
                List<IFPropertyRef_CT> result = null;
                if (InternalObj?.getIFPropertyRef()?.toArray() != null)
                {
                    result = new List<IFPropertyRef_CT>();

                    foreach (var obj in InternalObj.getIFPropertyRef().toArray())
                    {
                        var objType = obj;
                        if (objType is fi.tut.mei.resdescapi.rdapi.model.rd.IFPropertyRefCT)
                        {
                            result.Add(
                                new IFPropertyRef_CT((fi.tut.mei.resdescapi.rdapi.model.rd.IFPropertyRefCT)objType));
                        }
                    }
                }
                return result;
            }
        }

        [DisplayName(@"IfCategory")]
        public string IfCategory => InternalObj?.getIfCategory()?.value();

        [DisplayName(@"AbstractInterfacePortRef")]
        public string AbstractInterfacePortRef
        {
            get { return InternalObj?.getAbstractInterfacePortRef().ToString(); }
        }

        [DisplayName(@"InterfaceStdRef")]
        public string InterfaceStdRef => InternalObj?.getInterfaceStdRef().ToString();

        [DisplayName(@"Description")]
        public string Description
        {
            get { return InternalObj?.getDescription(); }
            set { InternalObj?.setDescription(value); }
        }

        [DisplayName(@"Gender")]
        public string Gender => InternalObj?.getGender()?.value();

        [DisplayName(@"ReqOpt")]
        public string ReqOpt => InternalObj?.getReqOpt()?.value();

        [DisplayName(@"MinOcc")]
        public string MinOcc
        {
            get { return InternalObj?.getMinOcc(); }
            set { InternalObj?.setMinOcc(value); }
        }

        [DisplayName(@"MaxOcc")]
        public string MaxOcc
        {
            get { return InternalObj?.getMaxOcc(); }
            set { InternalObj?.setMaxOcc(value); }
        }
    }
}