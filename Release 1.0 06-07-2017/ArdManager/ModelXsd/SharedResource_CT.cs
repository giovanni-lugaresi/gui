using System.Collections.Generic;
using System.ComponentModel;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"SharedResource_CT")]
    public class SharedResource_CT
    {
        public SharedResource_CT()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public SharedResource_CT(fi.tut.mei.resdescapi.rdapi.model.rd.SharedResourceCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.SharedResourceCT InternalObj { get; }

        [DisplayName(@"Alias")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<AliasType_CT> Alias
            => AliasType_CT.Get(InternalObj?.getDatatypes()?.getBaseTypeOrStringTypeOrEnum()?.toArray());

        [DisplayName(@"Array")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<ArrayType_CT> Array
            => ArrayType_CT.Get(InternalObj?.getDatatypes()?.getBaseTypeOrStringTypeOrEnum()?.toArray());

        [DisplayName(@"Enum")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<EnumType_CT> Enum
            => EnumType_CT.Get(InternalObj?.getDatatypes()?.getBaseTypeOrStringTypeOrEnum()?.toArray());

        [DisplayName(@"StringType")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<DataTypes_CTStringType> StringType
            => DataTypes_CTStringType.Get(InternalObj?.getDatatypes()?.getBaseTypeOrStringTypeOrEnum()?.toArray());

        [DisplayName(@"Struct")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<StructType_CT> Struct
            => StructType_CT.Get(InternalObj?.getDatatypes()?.getBaseTypeOrStringTypeOrEnum()?.toArray());

        [DisplayName(@"SubRange")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<SubRangeType_CT> SubRange
            => SubRangeType_CT.Get(InternalObj?.getDatatypes()?.getBaseTypeOrStringTypeOrEnum()?.toArray());
 
        [DisplayName(@"SelectiveProperties")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public SharedResource_CTSelectiveProperties SelectiveProperties
            =>
                InternalObj?.getSelectiveProperties() != null
                    ? new SharedResource_CTSelectiveProperties(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.SharedResourceCT.SelectiveProperties)
                            InternalObj.getSelectiveProperties())
                    : null;

        [DisplayName(@"Properties")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public SharedResource_CTProperties Properties
            =>
                InternalObj?.getProperties() != null
                    ? new SharedResource_CTProperties(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.SharedResourceCT.Properties)
                            InternalObj.getProperties())
                    : null;



        [DisplayName(@"Variables")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public SharedResource_CTVariables Variables
            =>
                InternalObj?.getVariables() != null
                    ? new SharedResource_CTVariables(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.SharedResourceCT.Variables)
                            InternalObj.getVariables())
                    : null;
    }
}