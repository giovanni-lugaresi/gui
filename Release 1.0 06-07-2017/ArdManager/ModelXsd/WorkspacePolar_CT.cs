using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"WorkspacePolar_CT")]

    public class WorkspacePolar_CT
    {
        public WorkspacePolar_CT()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public WorkspacePolar_CT(
            fi.tut.mei.resdescapi.rdapi.model.rd.WorkspacePolarCT
                obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.WorkspacePolarCT
            InternalObj
        { get; }

      
        [DisplayName(@"distance_r")]
        public string DistanceR
        {
            get { return InternalObj?.getDistanceR(); }
            set { InternalObj?.setDistanceR(value); }
        }
        [DisplayName(@"angle_a")]
        public string AngleA
        {
            get { return InternalObj?.getAngleA(); }
            set { InternalObj?.setAngleA(value); }
        }
    }
}