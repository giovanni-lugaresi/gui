﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ExecCapaRef_CT")]
    public class ExecCapaRef_CT
    {
        public ExecCapaRef_CT()
        {
            
        }
        public override string ToString()
        {
            return Name;
        }
   
        public ExecCapaRef_CT(fi.tut.mei.resdescapi.rdapi.model.rd.ExecCapaRefCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ExecCapaRefCT InternalObj { get; }

        [DisplayName(@"IdRef")]
        public string IdRef
        {
            get { return InternalObj?.getIdRef(); }
            set { InternalObj?.setIdRef(value); }
        }

        [DisplayName(@"GidRef")]
        public string GidRef
        {
            get { return InternalObj?.getGidRef(); }
            set { InternalObj?.setGidRef(value); }
        }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }

        [DisplayName(@"ReqOpt")]
        public string ReqOpt => InternalObj?.getReqOpt()?.value();

    }
}
