﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"BusinessProperty_CT")]

    public class BusinessProperty_CT : Variable_CT
    {
        public BusinessProperty_CT()
        {
            
        }
        public BusinessProperty_CT(fi.tut.mei.resdescapi.rdapi.model.rd.BusinessPropertyCT obj) : base(obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.BusinessPropertyCT InternalObj { get; }

        [DisplayName(@"Min")]
        public string Min => InternalObj?.getMin().toString();

        [DisplayName(@"Max")]
        public string Max => InternalObj?.getMax().toString();

        [DisplayName(@"DefaultValue")]
        public string DefaultValue => InternalObj?.getDefaultValue().toString();

        [DisplayName(@"Value")]
        public string Value => InternalObj?.getValue();

        [DisplayName(@"ClassOfBusinessProperty")]
        public fi.tut.mei.resdescapi.rdapi.model.rd.CostClassifierST ClassOfBusinessProperty
        {
            get { return InternalObj?.getClassOfBusinessProperty(); }
            set { InternalObj?.setClassOfBusinessProperty(value); }
        }


        public static List<BusinessProperty_CT> Get(object[] array)
        {
            var result = new List<BusinessProperty_CT>();
            if (array == null)
                return result;
            try
            {
                result =
                    array
                        .OfType
                        <
                            fi.tut.mei.resdescapi.rdapi.model.rd.BusinessPropertyCT>()
                        .Select(objType => new BusinessProperty_CT(objType))
                        .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}