﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"GUIDefinition_CT")]
    public class GUIDefinition_CT : ConnectionMethodCtrlAndComm_CT
    {
        public GUIDefinition_CT()
        {
            
        }
        public GUIDefinition_CT(fi.tut.mei.resdescapi.rdapi.model.rd.GUIDefinitionCT obj) : base(obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.GUIDefinitionCT InternalObj { get; }



        public static List<GUIDefinition_CT> Get(object[] array)
        {
            var result = new List<GUIDefinition_CT>();
            if (array == null)
                return result;
            try
            {
                result = array.OfType<fi.tut.mei.resdescapi.rdapi.model.rd.GUIDefinitionCT>()
                    .Select(objType => new GUIDefinition_CT(objType))
                    .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}