using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"Variable_CT_wComplex")]
    public class Variable_CT_wComplex : Variable_CT
    {
        public Variable_CT_wComplex()
        {
            
        }
        public Variable_CT_wComplex(fi.tut.mei.resdescapi.rdapi.model.rd.VariableCTWComplex obj) : base(obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.VariableCTWComplex InternalObj { get; }

        [DisplayName(@"ComplexValue")]
        public ValueComplex_CT ComplexValue
            =>
                InternalObj?.getComplexValue() != null
                    ? new ValueComplex_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.ValueComplexCT) InternalObj.getComplexValue())
                    : null;


        public static List<Variable_CT_wComplex> Get(object[] array)
        {
            var result = new List<Variable_CT_wComplex>();
            if (array == null)
                return result;
            try
            {
                result =
                    array
                        .OfType
                        <
                            fi.tut.mei.resdescapi.rdapi.model.rd.VariableCTWComplex>()
                        .Select(objType => new Variable_CT_wComplex(objType))
                        .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}