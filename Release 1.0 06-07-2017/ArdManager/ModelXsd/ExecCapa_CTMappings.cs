﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ExecCapa_CTMappings")]
    public class ExecCapa_CTMappings
    {
        public override string ToString()
        {
            return "ExecCapa_CTMappings";
        }

        public ExecCapa_CTMappings()
        {

        }

        public ExecCapa_CTMappings(fi.tut.mei.resdescapi.rdapi.model.rd.ExecCapaCT.Mappings obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ExecCapaCT.Mappings InternalObj { get; }

        [DisplayName(@"CapabilityMapping")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<CapabilityMapping_CT> CapabilityMapping
        {
            get
            {
                List<CapabilityMapping_CT> result = null;
                if (InternalObj?.getCapabilityMapping()?.toArray() != null)
                {
                    result = new List<CapabilityMapping_CT>();

                    foreach (var obj in InternalObj.getCapabilityMapping().toArray())
                    {
                        var objType = obj;
                        if (objType is fi.tut.mei.resdescapi.rdapi.model.rd.CapabilityMappingCT)
                        {
                            result.Add(
                                new CapabilityMapping_CT(
                                    (fi.tut.mei.resdescapi.rdapi.model.rd.CapabilityMappingCT) objType));
                        }
                    }
                }
                return result;
            }
        }

        [DisplayName(@"ExecCapaMapping")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<ExecCapaMapping_CT> ExecCapaMapping
        {
            get
            {
                List<ExecCapaMapping_CT> result = null;
                if (InternalObj?.getCapabilityMapping()?.toArray() != null)
                {
                    result = new List<ExecCapaMapping_CT>();

                    foreach (var obj in InternalObj.getCapabilityMapping().toArray())
                    {
                        var objType = obj;
                        if (objType is fi.tut.mei.resdescapi.rdapi.model.rd.ExecCapaMappingCT)
                        {
                            result.Add(
                                new ExecCapaMapping_CT((fi.tut.mei.resdescapi.rdapi.model.rd.ExecCapaMappingCT) objType));
                        }
                    }
                }
                return result;
            }
        }
    }
}
