﻿using System.Collections.Generic;
using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ValueStruct_CT")]
    public class ValueStruct_CT
    {
        public ValueStruct_CT()
        {
            
        }
        public ValueStruct_CT(fi.tut.mei.resdescapi.rdapi.model.rd.ValueStructCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ValueStructCT InternalObj { get; }
 
        [DisplayName(@"Member")]
        public List<ValueStruct_CTMember> Member
        {
            get
            {
                List<ValueStruct_CTMember> result = null;
                if (InternalObj?.getMember()?.toArray() != null)
                {
                    result = new List<ValueStruct_CTMember>();

                    foreach (var obj in InternalObj.getMember().toArray())
                    {
                        var objType = obj;
                        if (objType is fi.tut.mei.resdescapi.rdapi.model.rd.ValueStructCT.Member)
                        {
                            result.Add(
                                new ValueStruct_CTMember((fi.tut.mei.resdescapi.rdapi.model.rd.ValueStructCT.Member)objType));
                        }
                    }
                }
                return result;
            }
        }
    }
}