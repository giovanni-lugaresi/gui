using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"XHTML_Doc_CT")]

    public class XHTML_Doc_CT
    {
        public XHTML_Doc_CT()
        {
            
        }
        public XHTML_Doc_CT(fi.tut.mei.resdescapi.rdapi.model.rd.XHTMLDocCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.XHTMLDocCT InternalObj { get; }

        public string Content => InternalObj?.getContent() != null
            ? InternalObj.getContent().ToString()
            : null;

        [DisplayName(@"URL")]
        public string Url
        {
            get { return InternalObj?.getURL(); }
            set { InternalObj?.setURL(value); }
        }

        [DisplayName(@"Lang")]
        public string Lang
        {
            get { return InternalObj?.getLang(); }
            set { InternalObj?.setLang(value); }
        }

        public static List<XHTML_Doc_CT> Get(object[] array)
        {
            var result = new List<XHTML_Doc_CT>();
            if (array == null)
                return result;
            try
            {
                result = array.OfType<fi.tut.mei.resdescapi.rdapi.model.rd.XHTMLDocCT>()
                    .Select(objType => new XHTML_Doc_CT(objType))
                    .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}