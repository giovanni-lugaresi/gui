﻿using System.Collections.Generic;
using System.ComponentModel;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"InterfacesForRD_CTStdBodies")]

    public class InterfacesForRD_CTStdBodies
    {
        public InterfacesForRD_CTStdBodies()
        {
            
        }
        public InterfacesForRD_CTStdBodies(fi.tut.mei.resdescapi.rdapi.model.rd.InterfacesForRDCT.StdBodies obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.InterfacesForRDCT.StdBodies InternalObj { get; }

        [DisplayName(@"StdBodyLink")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<ExternalResourceLink_CT> StdBodyLink
            =>
                ExternalResourceLink_CT.Get(
                    InternalObj?.getStdBodyLink()?.toArray());

        [DisplayName(@"StdBody")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<StdBody> StdBodylst
            =>
                StdBody.Get(
                    InternalObj?.getStdBody()?.toArray());
    }
}