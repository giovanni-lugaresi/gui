using System.Collections.Generic;
using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"VariableBase_CT")]
    public class VariableTypeForEventPort_CT : VariableBoolean_CT
    {
        public VariableTypeForEventPort_CT()
        {
            
        }
        public VariableTypeForEventPort_CT(fi.tut.mei.resdescapi.rdapi.model.rd.VariableTypeForEventPortCT obj):base(obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.VariableTypeForEventPortCT InternalObj { get; }

        [DisplayName(@"NameLoc")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public LocalizedString_CT NameLoc
            =>
                InternalObj?.getNameLoc()?.toArray() != null
                    ? new LocalizedString_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.LocalizedStringCT) InternalObj.getNameLoc().toArray()[0])
                    : null;

        [DisplayName(@"Comment")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public LocalizedString_CT Comment
            =>
                InternalObj?.getComment()?.toArray() != null
                    ? new LocalizedString_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.LocalizedStringCT) InternalObj.getComment().toArray()[0])
                    : null;


        [DisplayName(@"LeadsToOutputEvent")]
        public LinkedParamPorts_CT LinkedParamPorts
            =>
                InternalObj?.getLinkedParamPorts() != null
                    ? new LinkedParamPorts_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.LinkedParamPortsCT)
                            InternalObj.getLinkedParamPorts())
                    : null;

        [DisplayName(@"LeadsToOutputEvent")]
        public List<VariableTypeForEventPort_CTLeadsToOutputEvent> LeadsToOutputEvent
        {
            get
            {
                List<VariableTypeForEventPort_CTLeadsToOutputEvent> result = null;
                if (InternalObj?.getLeadsToOutputEvent()?.toArray() != null)
                {
                    result = new List<VariableTypeForEventPort_CTLeadsToOutputEvent>();

                    foreach (var obj in InternalObj.getLeadsToOutputEvent().toArray())
                    {
                        var objType = obj;
                        if (objType is fi.tut.mei.resdescapi.rdapi.model.rd.LinkedParamPortsCT.ParamPort)
                        {
                            result.Add(
                                new VariableTypeForEventPort_CTLeadsToOutputEvent(
                                    (fi.tut.mei.resdescapi.rdapi.model.rd.VariableTypeForEventPortCT.LeadsToOutputEvent)
                                        objType));
                        }
                    }
                }
                return result;
            }
        }

        public string ConnectionMethod => InternalObj?.getConnectionMethod()?.value();
    }
}