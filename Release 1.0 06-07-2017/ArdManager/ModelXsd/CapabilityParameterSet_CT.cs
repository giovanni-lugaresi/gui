using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"CapabilityParameterSet_CT")]
    public class CapabilityParameterSet_CT
    {
        public CapabilityParameterSet_CT()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public CapabilityParameterSet_CT(fi.tut.mei.resdescapi.rdapi.model.rd.CapabilityParameterSetCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.CapabilityParameterSetCT InternalObj { get; }

        //    //todo: va in eccezione quando espando il nodo
        //[DisplayName(@"Property")]
        //[TypeConverter(typeof (ExpandableObjectConverter))]
        //[Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        //public List<Property_CT> Property => Property_CT.Get(InternalObj?.getProperty()?.toArray());

        [DisplayName(@"Variable")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<Variable_CT_wComplex> Variable => Variable_CT_wComplex.Get(InternalObj?.getVariable()?.toArray());

        [DisplayName(@"VariableRef")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<VariableRef_CT> VariableRef => VariableRef_CT.Get(InternalObj?.getVariableRef()?.toArray());

        public static List<CapabilityParameterSet_CT> Get(object[] array)
        {
            var result = new List<CapabilityParameterSet_CT>();
            if (array == null)
                return result;
            try
            {
                result =
                    array
                        .OfType
                        <
                            fi.tut.mei.resdescapi.rdapi.model.rd.CapabilityParameterSetCT>()
                        .Select(objType => new CapabilityParameterSet_CT(objType))
                        .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}