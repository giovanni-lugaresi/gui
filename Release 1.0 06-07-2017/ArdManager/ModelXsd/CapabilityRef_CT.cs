using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"CapabilityRef_CT")]
    public  class CapabilityRef_CT
    {
        public CapabilityRef_CT()
        {
            
        }
        public CapabilityRef_CT(fi.tut.mei.resdescapi.rdapi.model.rd.CapabilityRefCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.CapabilityRefCT InternalObj { get; }


        [DisplayName(@"IdRef")]
        public string IdRef
        {
            get { return InternalObj?.getIdRef(); }
            set { InternalObj?.setIdRef(value); }
        }

        [DisplayName(@"GidRef")]
        public string GidRef
        {
            get { return InternalObj?.getGidRef(); }
            set { InternalObj?.setGidRef(value); }
        }

        [DisplayName(@"ReqOpt")]
        public string ReqOpt => InternalObj?.getReqOpt()?.value();

    }
}