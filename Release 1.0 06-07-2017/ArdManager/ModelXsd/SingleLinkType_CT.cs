using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"SingleLinkType_CT")]
    public class SingleLinkType_CT
    {
        public SingleLinkType_CT()
        {
            
        }
        public override string ToString()
        {
            return Uri;
        }

        public SingleLinkType_CT(fi.tut.mei.resdescapi.rdapi.model.rd.SingleLinkTypeCT internalObj)
        {
            InternalObj = internalObj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.SingleLinkTypeCT InternalObj { get; }

        [DisplayName(@"Labels")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<LocalizedString_CT> Labels => LocalizedString_CT.Get(InternalObj?.getLabel()?.toArray());

        [DisplayName(@"URI")]
        public string Uri
        {
            get { return InternalObj?.getURI(); }
            set { InternalObj?.setURI(value); }
        }


        public static List<SingleLinkType_CT> Get(object[] array)
        {
            var result = new List<SingleLinkType_CT>();
            if (array == null)
                return result;
            try
            {
                result = array.OfType<fi.tut.mei.resdescapi.rdapi.model.rd.SingleLinkTypeCT>()
                    .Select(objType => new SingleLinkType_CT(objType))
                    .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}