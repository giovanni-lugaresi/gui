using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"SymbolRefTypePort_CT")]
    public class VariableRefTypeForEventPort_CT : SymbolRefTypePort_CT
    {
        public VariableRefTypeForEventPort_CT()
        {
            
        }
        public VariableRefTypeForEventPort_CT(fi.tut.mei.resdescapi.rdapi.model.rd.VariableRefTypeForEventPortCT obj):base(obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.VariableRefTypeForEventPortCT InternalObj { get; }

        [DisplayName(@"LinkedParamPorts")]
        public LinkedParamPorts_CT LinkedParamPorts
            =>
                InternalObj?.getLinkedParamPorts() != null
                    ? new LinkedParamPorts_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.LinkedParamPortsCT)
                            InternalObj.getLinkedParamPorts())
                    : null;
    }
}