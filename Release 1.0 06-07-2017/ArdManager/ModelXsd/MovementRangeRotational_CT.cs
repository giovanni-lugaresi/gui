using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"MovementRangeRotational_CT")]

    public class MovementRangeRotational_CT
    {
        public MovementRangeRotational_CT()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public MovementRangeRotational_CT(
            fi.tut.mei.resdescapi.rdapi.model.rd.MovementRangeRotationalCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.MovementRangeRotationalCT InternalObj { get; }

        [DisplayName(@"Rotation_x")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ValueSimple_CT RotationX
            =>
                InternalObj?.getRotationX() != null
                    ? new ValueSimple_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.ValueSimpleCT) InternalObj.getRotationX())
                    : null;

        [DisplayName(@"Rotation_y")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ValueSimple_CT RotationY
            =>
                InternalObj?.getRotationY() != null
                    ? new ValueSimple_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.ValueSimpleCT) InternalObj.getRotationY())
                    : null;

        [DisplayName(@"Rotation_z")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ValueSimple_CT RotationZ
            =>
                InternalObj?.getRotationZ() != null
                    ? new ValueSimple_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.ValueSimpleCT) InternalObj.getRotationZ())
                    : null;
    }
}