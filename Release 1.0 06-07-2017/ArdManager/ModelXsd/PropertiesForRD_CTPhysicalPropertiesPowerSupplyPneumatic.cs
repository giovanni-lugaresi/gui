using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"PropertiesForRD_CTPhysicalPropertiesPowerSupplyPneumatic")]

    public class PropertiesForRD_CTPhysicalPropertiesPowerSupplyPneumatic
    {
        public PropertiesForRD_CTPhysicalPropertiesPowerSupplyPneumatic()
        {
            
        }
        public PropertiesForRD_CTPhysicalPropertiesPowerSupplyPneumatic(
            fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.PhysicalProperties.PowerSupplyPneumatic obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.PhysicalProperties.PowerSupplyPneumatic
            InternalObj { get; }
 
        [DisplayName(@"pressureRangeMin")]
        public string PressureRangeMin => InternalObj?.getPressureRangeMin().ToString();

        [DisplayName(@"pressureNominal")]
        public string PressureNominal => InternalObj?.getPressureNominal().ToString();


        [DisplayName(@"pressureRangeMax")]
        public string PressureRangeMax => InternalObj?.getPressureRangeMax().ToString();


        [DisplayName(@"Unit")]
        public string Unit
        {
            get { return InternalObj?.getUnit(); }
            set { InternalObj?.setUnit(value); }
        }

        [DisplayName(@"decade")]
        public string Decade => InternalObj?.getDecade().ToString();


        public static List<PropertiesForRD_CTPhysicalPropertiesPowerSupplyPneumatic> Get(object[] array)
        {
            var result = new List<PropertiesForRD_CTPhysicalPropertiesPowerSupplyPneumatic>();
            if (array == null)
                return result;
            try
            {
                result =
                    array
                        .OfType
                        <
                            fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.PhysicalProperties.PowerSupplyPneumatic>()
                        .Select(objType => new PropertiesForRD_CTPhysicalPropertiesPowerSupplyPneumatic(objType))
                        .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}