﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"PropertiesForRD_CTPhysicalPropertiesPowerSupplyElectrical")]

    public class PropertiesForRD_CTPhysicalPropertiesPowerSupplyElectrical
    {
        public PropertiesForRD_CTPhysicalPropertiesPowerSupplyElectrical()
        {
            
        }
        public PropertiesForRD_CTPhysicalPropertiesPowerSupplyElectrical(
            fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.PhysicalProperties.PowerSupplyElectrical obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.PhysicalProperties.PowerSupplyElectrical InternalObj { get; }
 
        [DisplayName(@"voltageRangeMin")]
        public string VoltageRangeMin => InternalObj?.getVoltageRangeMin().ToString();

        [DisplayName(@"voltageNominal")]
        public string VoltageNominal => InternalObj?.getVoltageNominal().ToString();

        [DisplayName(@"voltageRangeMax")]
        public string VoltageRangeMax => InternalObj?.getVoltageRangeMax().ToString();

        [DisplayName(@"frequency")]
        public string Frequency => InternalObj?.getFrequency().ToString();

        [DisplayName(@"numberOfPhases")]
        public string NumberOfPhases => InternalObj?.getNumberOfPhases().ToString();

        [DisplayName(@"currentNominal")]
        public string CurrentNominal => InternalObj?.getCurrentNominal().ToString();

        [DisplayName(@"currentMax")]
        public string CurrentMax => InternalObj?.getCurrentMax().ToString();

        [DisplayName(@"AC_or_DC")]
        public string AcOrDc => InternalObj?.getACOrDC();

        [DisplayName(@"CurrentMaxPassThrough")]
        public string CurrentMaxPassThrough => InternalObj?.getCurrentMaxPassThrough().toString();

        public static List<PropertiesForRD_CTPhysicalPropertiesPowerSupplyElectrical> Get(object[] array)
        {
            var result = new List<PropertiesForRD_CTPhysicalPropertiesPowerSupplyElectrical>();
            if (array == null)
                return result;
            try
            {
                result =
                    array
                        .OfType
                        <
                            fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.PhysicalProperties.PowerSupplyElectrical>()
                        .Select(objType => new PropertiesForRD_CTPhysicalPropertiesPowerSupplyElectrical(objType))
                        .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}