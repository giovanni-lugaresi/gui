using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"InterfaceImplRD_CTInstanceAreaRectangle")]

    public class InterfaceImplRD_CTInstanceAreaRectangle
    {
        public InterfaceImplRD_CTInstanceAreaRectangle()
        {
            
        }
        public InterfaceImplRD_CTInstanceAreaRectangle(
            fi.tut.mei.resdescapi.rdapi.model.rd.InterfaceImplRDCT.Instances.Instance.Area.Rectangle obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.InterfaceImplRDCT.Instances.Instance.Area.Rectangle InternalObj {
            get; }

        [DisplayName(@"Point_2D")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<string> Point_2D => InternalObj?.getPoint2D()?.toArray()?.Cast<string>().ToList();
    }
}