using System.Collections.Generic;
using System.ComponentModel;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"Company_CT")]
    public class Company_CT
    {
        public Company_CT()
        {
            
        }
        public Company_CT(fi.tut.mei.resdescapi.rdapi.model.rd.CompanyCT resourceDescription)
        {
            InternalObj = resourceDescription;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.CompanyCT InternalObj { get; }

        [DisplayName(@"Links")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<SingleLinkType_CT> Links => SingleLinkType_CT.Get(InternalObj?.getLink()?.toArray());

        [DisplayName(@"NameLoc")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<LocalizedString_CT> NameLoc => LocalizedString_CT.Get(InternalObj?.getNameLoc()?.toArray());

        [DisplayName(@"Id")]
        public string Id
        {
            get { return InternalObj?.getId(); }
            set { InternalObj?.setId(value); }
        }
    }
}