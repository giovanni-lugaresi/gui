using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"WorkspaceCylinder_CT")]

    public class WorkspaceCylinder_CT
    {
        public WorkspaceCylinder_CT()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public WorkspaceCylinder_CT(
            fi.tut.mei.resdescapi.rdapi.model.rd.WorkspaceCylinderCT
                obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.WorkspaceCylinderCT
            InternalObj
        { get; }

       
        [DisplayName(@"distance_r")]
        public string DistanceR
        {
            get { return InternalObj?.getDistanceR(); }
            set { InternalObj?.setDistanceR(value); }
        }
        [DisplayName(@"dimension_z")]
        public string DimensionZ
        {
            get { return InternalObj?.getDimensionZ(); }
            set { InternalObj?.setDimensionZ(value); }
        }
    }
}