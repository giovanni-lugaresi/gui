using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"InterfaceImplRD_CTInstanceAreaPolygon")]

    public class InterfaceImplRD_CTInstanceAreaPolygon
    {
        public InterfaceImplRD_CTInstanceAreaPolygon()
        {
            
        }
        public InterfaceImplRD_CTInstanceAreaPolygon(
            fi.tut.mei.resdescapi.rdapi.model.rd.InterfaceImplRDCT.Instances.Instance.Area.Polygon obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.InterfaceImplRDCT.Instances.Instance.Area.Polygon InternalObj { get;
        }

        [DisplayName(@"Point_2D")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<string> Point_2D => InternalObj?.getPoint2D()?.toArray()?.Cast<string>().ToList();
    }
}