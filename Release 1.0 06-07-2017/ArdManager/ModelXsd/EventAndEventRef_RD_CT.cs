using System.Collections.Generic;
using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"EventAndEventRef_RD_CT")]
    public class EventAndEventRef_RD_CT
    {
        public EventAndEventRef_RD_CT()
        {
            
        }
        public EventAndEventRef_RD_CT(fi.tut.mei.resdescapi.rdapi.model.rd.EventAndEventRefRDCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.EventAndEventRefRDCT InternalObj { get; }

        [DisplayName(@"Event")]
        public List<SymbolForEventPort_CT> Event
        {
            get
            {
                List<SymbolForEventPort_CT> result = null;
                if (InternalObj?.getEvent()?.toArray() != null)
                {
                    result = new List<SymbolForEventPort_CT>();

                    foreach (var obj in InternalObj.getEvent().toArray())
                    {
                        var objType = obj;
                        if (objType is fi.tut.mei.resdescapi.rdapi.model.rd.SymbolForEventPortCT)
                        {
                            result.Add(
                                new SymbolForEventPort_CT(
                                    (fi.tut.mei.resdescapi.rdapi.model.rd.SymbolForEventPortCT) objType));
                        }
                    }
                }
                return result;
            }
        }

        [DisplayName(@"EventRef")]
        public List<SymbolRefTypeForEventPort_CT> EventRef
        {
            get
            {
                List<SymbolRefTypeForEventPort_CT> result = null;
                if (InternalObj?.getEventRef()?.toArray() != null)
                {
                    result = new List<SymbolRefTypeForEventPort_CT>();

                    foreach (var obj in InternalObj.getEventRef().toArray())
                    {
                        var objType = obj;
                        if (objType is fi.tut.mei.resdescapi.rdapi.model.rd.ExecCapaCT)
                        {
                            result.Add(
                                new SymbolRefTypeForEventPort_CT(
                                    (fi.tut.mei.resdescapi.rdapi.model.rd.SymbolRefTypeForEventPortCT) objType));
                        }
                    }
                }
                return result;
            }
        }
    }
}