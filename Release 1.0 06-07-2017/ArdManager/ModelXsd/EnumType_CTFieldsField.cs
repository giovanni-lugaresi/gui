﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"EnumType_CTFieldsField")]

    public   class EnumType_CTFieldsField
    {
        public EnumType_CTFieldsField()
        {
            
        }
        public override string ToString()
        {
            return Name;
        }

        public EnumType_CTFieldsField(fi.tut.mei.resdescapi.rdapi.model.rd.EnumTypeCT.Fields.Field obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.EnumTypeCT.Fields.Field InternalObj { get; }
 
        [DisplayName(@"Value")]
        public string Value => InternalObj?.getValue().toString();

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }


        public static List<EnumType_CTFieldsField> Get(object[] array)
        {
            var result = new List<EnumType_CTFieldsField>();
            if (array == null)
                return result;
            try
            {
                result =
                    array
                        .OfType
                        <
                            fi.tut.mei.resdescapi.rdapi.model.rd.EnumTypeCT.Fields.Field>()
                        .Select(objType => new EnumType_CTFieldsField(objType))
                        .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}