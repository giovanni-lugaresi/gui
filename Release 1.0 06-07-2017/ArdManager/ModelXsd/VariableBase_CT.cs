using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"VariableBase_CT")]
    public class VariableBase_CT
    {
        public VariableBase_CT()
        {
            
        }
        public VariableBase_CT(fi.tut.mei.resdescapi.rdapi.model.rd.VariableBaseCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.VariableBaseCT InternalObj { get; }

        [DisplayName(@"Description")]
        public string Description
        {
            get { return InternalObj?.getDescription(); }
            set { InternalObj?.setDescription(value); }
        }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }

        [DisplayName(@"Unit")]
        public string Unit
        {
            get { return InternalObj?.getUnit(); }
            set { InternalObj?.setUnit(value); }
        }

        [DisplayName(@"EClassIRDIRef")]
        public string EClassIrdiRef
        {
            get { return InternalObj?.getEClassIRDIRef(); }
            set { InternalObj?.setEClassIRDIRef(value); }
        }
    }
}