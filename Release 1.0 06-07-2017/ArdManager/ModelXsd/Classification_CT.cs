﻿using System.Collections.Generic;
using System.ComponentModel;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"Classification_CT")]

    public class Classification_CT
    {
        public Classification_CT()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public Classification_CT(fi.tut.mei.resdescapi.rdapi.model.rd.ClassificationCT resourceDescription)
        {
            InternalObj = resourceDescription;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ClassificationCT InternalObj { get; }

        [DisplayName(@"ResourceModelClassification")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<ResourceModelClassification_CT> ResourceModelClassification
            => ResourceModelClassification_CT.Get(InternalObj?.getResourceModelClassification()?.toArray());

        [DisplayName(@"EClassClassificationClass")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<EClassClassificationClass_CT> EClassClassificationClass
            => EClassClassificationClass_CT.Get(InternalObj?.getEClassClassificationClass()?.toArray());

    }
}