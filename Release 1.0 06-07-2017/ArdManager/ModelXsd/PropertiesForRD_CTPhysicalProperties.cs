﻿using System.Collections.Generic;
using System.ComponentModel;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"PropertiesForRD_CTPhysicalProperties")]

    public class PropertiesForRD_CTPhysicalProperties
    {
        public PropertiesForRD_CTPhysicalProperties()
        {
            
        }
        public PropertiesForRD_CTPhysicalProperties(
            fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.PhysicalProperties obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.PhysicalProperties InternalObj { get; }

        [DisplayName(@"Mass")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public PropertiesForRD_CTPhysicalPropertiesMass Mass =>
            InternalObj?.getMass() != null
                ? new PropertiesForRD_CTPhysicalPropertiesMass(
                    (fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.PhysicalProperties.Mass)
                        InternalObj.getMass())
                : null;

        [DisplayName(@"InstallationPositions")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public PropertiesForRD_CTPhysicalPropertiesInstallationPositions InstallationPositions
            =>
                InternalObj?.getInstallationPositions() != null
                    ? new PropertiesForRD_CTPhysicalPropertiesInstallationPositions(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.PhysicalProperties.InstallationPositions
                            )
                            InternalObj.getInstallationPositions())
                    : null;

        [DisplayName(@"LocalOrigin")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public PropertiesForRD_CTPhysicalPropertiesLocalOrigin LocalOrigin
            =>
                InternalObj?.getLocalOrigin() != null
                    ? new PropertiesForRD_CTPhysicalPropertiesLocalOrigin(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.PhysicalProperties.LocalOrigin)
                            InternalObj.getLocalOrigin())
                    : null;

        [DisplayName(@"BoundingBox")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public DOF3_dataLimits_CT BoundingBox
            =>
                InternalObj?.getBoundingBox() != null
                    ? new DOF3_dataLimits_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.DOF3DataLimitsCT)
                            InternalObj.getBoundingBox())
                    : null;

        [DisplayName(@"CentreOfGravity")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public Vector_CT CentreOfGravity
            =>
                InternalObj?.getCentreOfGravity() != null
                    ? new Vector_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.VectorCT)
                            InternalObj.getCentreOfGravity())
                    : null;

        //todo: va in eccezione
        //[DisplayName(@"PowerSupplyElectrical")]
        //[TypeConverter(typeof (ExpandableObjectConverter))]
        //[Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        //public List<PropertiesForRD_CTPhysicalPropertiesPowerSupplyElectrical> PowerSupplyElectrical
        //    =>
        //        PropertiesForRD_CTPhysicalPropertiesPowerSupplyElectrical.Get(
        //            InternalObj?.getPowerSupplyElectrical()?.toArray());


        [DisplayName(@"PowerSupplyPneumatic")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))] 
        public List<PropertiesForRD_CTPhysicalPropertiesPowerSupplyPneumatic> PowerSupplyPneumatic
            =>
                PropertiesForRD_CTPhysicalPropertiesPowerSupplyPneumatic.Get(
                    InternalObj?.getPowerSupplyPneumatic()?.toArray());
    }
}