﻿using System.Collections.Generic;
using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ValueEnum_CT")]
    public class ValueEnum_CT
    {
        public ValueEnum_CT()
        {
            
        }
        public ValueEnum_CT(fi.tut.mei.resdescapi.rdapi.model.rd.ValueEnumCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ValueEnumCT InternalObj { get; }

        [DisplayName(@"Name")]
        public List<string> Names
        {
            get
            {
                List<string> result = null;
                if (InternalObj?.getName()?.toArray() != null)
                {
                    result = new List<string>();

                    foreach (string obj in InternalObj.getName().toArray())
                    {
                        result.Add(obj);
                    }
                }
                return result;
            }
        }

        [DisplayName(@"Datatype")]
        public string Datatype => InternalObj?.getDatatype();
    }
}