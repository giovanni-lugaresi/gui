using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"InterfacesForRD_CTIFImplementations")]

    public   class InterfacesForRD_CTIFImplementations
    {
        public InterfacesForRD_CTIFImplementations()
        {
            
        }
        public InterfacesForRD_CTIFImplementations(fi.tut.mei.resdescapi.rdapi.model.rd.InterfacesForRDCT.IFImplementations obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.InterfacesForRDCT.IFImplementations InternalObj { get; }



        [DisplayName(@"Mechanic")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public MechanicalResourceDesc_CT Mechanic
            =>
                InternalObj?.getMechanic() != null
                    ? new MechanicalResourceDesc_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.MechanicalResourceDescCT)InternalObj.getMechanic())
                    : null;

        [DisplayName(@"Service")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ServiceResourceDesc_CT Service =>
            InternalObj?.getService() != null
                ? new ServiceResourceDesc_CT(
                    (fi.tut.mei.resdescapi.rdapi.model.rd.ServiceResourceDescCT)InternalObj.getService())
                : null;

        [DisplayName(@"Electric")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ElectricalResourceDesc_CT Electric =>
            InternalObj?.getElectric() != null
                ? new ElectricalResourceDesc_CT(
                    (fi.tut.mei.resdescapi.rdapi.model.rd.ElectricalResourceDescCT)InternalObj.getElectric())
                : null;

        [DisplayName(@"Communication")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ElectricalResourceDesc_CT Communication =>
            InternalObj?.getCommunication() != null
                ? new ElectricalResourceDesc_CT(
                    (fi.tut.mei.resdescapi.rdapi.model.rd.ElectricalResourceDescCT)InternalObj.getCommunication())
                : null;
    }
}