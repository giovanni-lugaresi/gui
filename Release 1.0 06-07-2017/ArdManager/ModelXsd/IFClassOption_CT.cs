﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"IFClassOption_CT")]
    public class IFClassOption_CT
    {
        public IFClassOption_CT()
        {
            
        }
        public IFClassOption_CT(fi.tut.mei.resdescapi.rdapi.model.rd.IFClassOptionCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.IFClassOptionCT InternalObj { get; }

        [DisplayName(@"Depends")]

        public List<Depends> Depends
        {
            get
            {
                List<Depends> result = null;
                if (InternalObj?.getValues()?.toArray() != null)
                {
                    result = new List<Depends>();

                    foreach (var obj in InternalObj.getValues().toArray())
                    {
                        var objType = obj;
                        if (objType is fi.tut.mei.resdescapi.rdapi.model.rd.Depends)
                        {
                            result.Add(
                                new Depends((fi.tut.mei.resdescapi.rdapi.model.rd.Depends)objType));
                        }
                    }
                }
                return result;
            }
        }

        [DisplayName(@"Values")]
        public List<string> Values
        {
            get
            {
                List<string> result = null;
                if (InternalObj?.getValues()?.toArray() != null)
                {
                    result = new List<string>();

                    foreach (var obj in InternalObj.getValues().toArray())
                    {
                        result.Add(obj.ToString());
                    }
                }
                return result;
            }
        }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }

        [DisplayName(@"Description")]
        public string Description
        {
            get { return InternalObj?.getDescription(); }
            set { InternalObj?.setDescription(value); }
        }

 
        [DisplayName(@"IfCategory")]
        public string IfCategory => InternalObj?.getIfCategory()?.value();



        public static List<IFClassOption_CT> Get(object[] array)
        {
            var result = new List<IFClassOption_CT>();
            if (array == null)
                return result;
            try
            {
                result = array.OfType<fi.tut.mei.resdescapi.rdapi.model.rd.IFClassOptionCT>()
                    .Select(objType => new IFClassOption_CT(objType))
                    .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}