using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"Capability")]
    public class Capability
    {
        public Capability()
        {
            
        }
        public override string ToString()
        {
            return Name;
        }

        public Capability(fi.tut.mei.resdescapi.rdapi.model.rd.Capability obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.Capability InternalObj { get; }


        [DisplayName(@"ParameterPorts")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public InputOutputParamSetCapability_CT ParameterPorts
            =>
                InternalObj?.getParameterPorts() != null
                    ? new InputOutputParamSetCapability_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.InputOutputParamSetCapabilityCT)
                            InternalObj.getParameterPorts())
                    : null;

        [DisplayName(@"Gid")]
        public string Gid
        {
            get { return InternalObj?.getGid(); }
            set { InternalObj?.setGid(value); }
        }

        [DisplayName(@"Description")]
        public string Description
        {
            get { return InternalObj?.getDescription(); }
            set { InternalObj?.setDescription(value); }
        }

        [DisplayName(@"Id")]
        public string Id
        {
            get { return InternalObj?.getId(); }
            set { InternalObj?.setId(value); }
        }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }

        [DisplayName(@"ReqOpt")]
        public string ReqOpt => InternalObj?.getReqOpt()?.value();

        public static List<Capability> Get(object[] array)
        {
            var result = new List<Capability>();
            if (array == null)
                return result;
            try
            {
                result =
                    array
                        .OfType
                        <
                            fi.tut.mei.resdescapi.rdapi.model.rd.Capability>()
                        .Select(objType => new Capability(objType))
                        .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}