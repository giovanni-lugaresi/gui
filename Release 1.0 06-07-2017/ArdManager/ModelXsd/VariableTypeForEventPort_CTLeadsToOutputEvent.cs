using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"Datatype")]

    public class VariableTypeForEventPort_CTLeadsToOutputEvent
    {
        public VariableTypeForEventPort_CTLeadsToOutputEvent()
        {
            
        }
        public VariableTypeForEventPort_CTLeadsToOutputEvent(
            fi.tut.mei.resdescapi.rdapi.model.rd.VariableTypeForEventPortCT.LeadsToOutputEvent obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.VariableTypeForEventPortCT.LeadsToOutputEvent InternalObj { get; }

        [DisplayName(@"IdRef")]
        public string IdRef
        {
            get { return InternalObj?.getIdRef(); }
            set { InternalObj?.setIdRef(value); }
        }
    }
}