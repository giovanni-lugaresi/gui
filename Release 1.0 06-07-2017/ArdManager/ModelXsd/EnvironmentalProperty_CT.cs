using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"BusinessProperty_CT")]

    public class EnvironmentalProperty_CT : Variable_CT
    {
        public EnvironmentalProperty_CT()
        {
            
        }
        public EnvironmentalProperty_CT(fi.tut.mei.resdescapi.rdapi.model.rd.EnvironmentalPropertyCT obj) : base(obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.EnvironmentalPropertyCT InternalObj { get; }

        [DisplayName(@"Min")]
        public string Min => InternalObj?.getMin().toString();

        [DisplayName(@"Max")]
        public string Max => InternalObj?.getMax().toString();

        [DisplayName(@"DefaultValue")]
        public string DefaultValue => InternalObj?.getDefaultValue().toString();

        [DisplayName(@"Value")]
        public string Value => InternalObj?.getValue();

        [DisplayName(@"ClassOfEnvProperty")]
        public fi.tut.mei.resdescapi.rdapi.model.rd.EnvironmentalConditionsST ClassOfEnvProperty
        {
            get { return InternalObj?.getClassOfEnvProperty(); }
            set { InternalObj?.setClassOfEnvProperty(value); }
        }


        public static List<EnvironmentalProperty_CT> Get(object[] array)
        {
            var result = new List<EnvironmentalProperty_CT>();
            if (array == null)
                return result;
            try
            {
                result =
                    array
                        .OfType
                        <
                            fi.tut.mei.resdescapi.rdapi.model.rd.EnvironmentalPropertyCT>()
                        .Select(objType => new EnvironmentalProperty_CT(objType))
                        .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}