﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"EnumType_CT")]

    public class EnumType_CT
    {
        public EnumType_CT()
        {
            
        }
        public override string ToString()
        {
            return Name;
        }

        public EnumType_CT(fi.tut.mei.resdescapi.rdapi.model.rd.EnumTypeCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.EnumTypeCT InternalObj { get; }

        [DisplayName(@"Id")]
        public string Id
        {
            get { return InternalObj?.getId(); }
            set { InternalObj?.setId(value); }
        }

        [DisplayName(@"BaseType")]
        public string BaseType
        {
            get { return InternalObj?.getBaseType(); }
            set { InternalObj?.setBaseType(value); }
        }

        [DisplayName(@"NameSpace")]
        public string NameSpace
        {
            get { return InternalObj?.getNameSpace(); }
            set { InternalObj?.setNameSpace(value); }
        }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }
 
        [DisplayName(@"Fields")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public EnumType_CTFields Fields
            =>
                InternalObj?.getFields() != null
                    ? new EnumType_CTFields(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.EnumTypeCT.Fields)
                            InternalObj.getFields())
                    : null;
        public static List<EnumType_CT> Get(object[] array)
        {
            var result = new List<EnumType_CT>();
            if (array == null)
                return result;
            try
            {
                result =
                    array
                        .OfType
                        <
                            fi.tut.mei.resdescapi.rdapi.model.rd.EnumTypeCT>()
                        .Select(objType => new EnumType_CT(objType))
                        .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
       
    }
}