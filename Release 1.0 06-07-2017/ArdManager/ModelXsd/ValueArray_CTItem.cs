﻿using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ValueArray_CTItem")]
    public class ValueArray_CTItem
    {
        public ValueArray_CTItem()
        {
            
        }
        public ValueArray_CTItem(fi.tut.mei.resdescapi.rdapi.model.rd.ValueArrayCT.Item obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ValueArrayCT.Item InternalObj { get; }

        [DisplayName(@"SimpleValue")]
        public ValueSimple_CT SimpleValue =>
            InternalObj?.getSimpleValue() != null
                ? new ValueSimple_CT(InternalObj?.getSimpleValue())
                : null;

        [DisplayName(@"StringValue")]
        public ValueString_CT StringValue =>
            InternalObj?.getStringValue() != null
                ? new ValueString_CT(InternalObj?.getStringValue())
                : null;

        [DisplayName(@"OrderNbr")]
        public string OrderNbr => InternalObj?.getOrderNbr().toString();
    }
}