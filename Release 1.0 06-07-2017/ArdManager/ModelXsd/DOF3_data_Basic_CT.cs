﻿using System.ComponentModel;
using ikvm.extensions;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"DOF3_data_Basic_CT")]

    public class DOF3_data_Basic_CT
    {
        public DOF3_data_Basic_CT()
        {
            
        }
        public DOF3_data_Basic_CT(fi.tut.mei.resdescapi.rdapi.model.rd.DOF3DataBasicCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.DOF3DataBasicCT InternalObj { get; }

        [DisplayName(@"X")]
        public string X => InternalObj?.getX().toString();

        [DisplayName(@"Y")]
        public string Y => InternalObj?.getY().toString();

        [DisplayName(@"Z")]
        public string Z => InternalObj?.getZ().toString();
    }
}