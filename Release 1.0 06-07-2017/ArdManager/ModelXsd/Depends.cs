﻿using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"Depends")]

    public class Depends
    {
        public Depends()
        {
            
        }
        public Depends(fi.tut.mei.resdescapi.rdapi.model.rd.Depends obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.Depends InternalObj { get; }

        [DisplayName(@"Value")]
        public string Value
        {
            get { return InternalObj?.getValue(); }
            set { InternalObj?.setValue(value); }
        }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }

        [DisplayName(@"IfIdRef")]
        public string IfIdRef => InternalObj?.getIfIdRef().ToString();

       
        [DisplayName(@"DependsType")]
        public string DependsType => InternalObj?.getType();


    }
}