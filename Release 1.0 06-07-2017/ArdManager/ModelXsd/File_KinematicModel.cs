using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"InterfaceType")]

    public class File_KinematicModel
    {
        public File_KinematicModel()
        {
            
        }
        public File_KinematicModel(fi.tut.mei.resdescapi.rdapi.model.rd.FileKinematicModel obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.FileKinematicModel InternalObj { get; }

        [DisplayName(@"XHTML")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public XHTML_Doc_CT Xhtml =>
            InternalObj?.getXHTML() != null
                ? new XHTML_Doc_CT(InternalObj?.getXHTML())
                : null;

        [DisplayName(@"URL")]
        public string Url
        {
            get { return InternalObj?.getURL(); }
            set { InternalObj?.setURL(value); }
        }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }

        [DisplayName(@"InterfaceType")]
        public string Format => InternalObj?.getFormat()?.value();
    }
}