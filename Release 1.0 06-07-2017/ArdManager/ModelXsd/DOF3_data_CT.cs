﻿using System.ComponentModel;
using ArdManager.ModelXsd;
using ikvm.extensions;

[DisplayName(@"DOF3_data_CT")]

public class DOF3_data_CT : DOF3_data_Basic_CT
{
    public DOF3_data_CT()
    {
        
    }
    public DOF3_data_CT(fi.tut.mei.resdescapi.rdapi.model.rd.DOF3DataCT obj):base(obj)
    {
        InternalObj = obj;
    }

    [Browsable(false)]
    public fi.tut.mei.resdescapi.rdapi.model.rd.DOF3DataCT InternalObj { get; }
 
    [DisplayName(@"Unit")]
    public string Unit
    {
        get { return InternalObj?.getUnit(); }
        set { InternalObj?.setUnit(value); }
    }

    [DisplayName(@"Decade")]
    public string Decade => InternalObj?.getDecade().toString();

    [DisplayName(@"Increment")]
    public string Increment => InternalObj?.getIncrement().toString();
}