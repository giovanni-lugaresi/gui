using System.Collections.Generic;
using System.ComponentModel;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"InputOutputParamSetCapability_CTCharacteristics")]
    public class InputOutputParamSetCapability_CTCharacteristics : CapabilityParameterSet_CT
    {
        public InputOutputParamSetCapability_CTCharacteristics()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public InputOutputParamSetCapability_CTCharacteristics(
            fi.tut.mei.resdescapi.rdapi.model.rd.InputOutputParamSetCapabilityCT.Characteristics obj) : base(obj)
        {
            InternalObj = obj;

        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.InputOutputParamSetCapabilityCT.Characteristics InternalObj { get; }

        //private ItemSize_CT[][] hasItemSize_minField;

        //private ItemSize_CT[][] hasItemSize_maxField;

        ///// <remarks/>
        //[System.Xml.Serialization.XmlArrayItemAttribute("ItemSize", typeof (ItemSize_CT), IsNullable = false)]
        //public ItemSize_CT[][] hasItemSize_min
        //{
        //    get { return this.hasItemSize_minField; }
        //    set { this.hasItemSize_minField = value; }
        //}

        ///// <remarks/>
        //[System.Xml.Serialization.XmlArrayItemAttribute("ItemSize", typeof (ItemSize_CT), IsNullable = false)]
        //public ItemSize_CT[][] hasItemSize_max
        //{
        //    get { return this.hasItemSize_maxField; }
        //    set { this.hasItemSize_maxField = value; }
        //}

        //    //todo: va in eccezione quando espando il nodo
        //[DisplayName(@"Property")]
        //[TypeConverter(typeof(ExpandableObjectConverter))]
        ////[Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        //public new List<Property_CT> Property => Property_CT.Get(InternalObj?.getProperty()?.toArray());

        [DisplayName(@"Variable")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public new List<Variable_CT_wComplex> Variable => Variable_CT_wComplex.Get(InternalObj?.getVariable()?.toArray());

        [DisplayName(@"VariableRef")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public new List<VariableRef_CT> VariableRef => VariableRef_CT.Get(InternalObj?.getVariableRef()?.toArray());


        [DisplayName(@"hasAllowedItemShapeAndSize_min")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<InputOutputParamSetCapability_CTCharacteristicsHasAllowedItemShapeAndSize_min> HasAllowedItemShapeAndSizeMin
            =>
                InputOutputParamSetCapability_CTCharacteristicsHasAllowedItemShapeAndSize_min.Get(
                    InternalObj?.getGHasShapeAndSizes()?.toArray());

        [DisplayName(@"hasAllowedItemShapeAndSize_max")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<InputOutputParamSetCapability_CTCharacteristicsHasAllowedItemShapeAndSize_max> HasAllowedItemShapeAndSizeMax
            =>
                InputOutputParamSetCapability_CTCharacteristicsHasAllowedItemShapeAndSize_max.Get(
                    InternalObj?.getGHasShapeAndSizes()?.toArray());

        [DisplayName(@"hasShapeAndSize")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<InputOutputParamSetCapability_CTCharacteristicsHasShapeAndSize> HasShapeAndSize
            =>
                InputOutputParamSetCapability_CTCharacteristicsHasShapeAndSize.Get(
                    InternalObj?.getGHasShapeAndSizes()?.toArray());

        [DisplayName(@"hasWorkspaceTypeAndDimensions")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public HasWorkspaceTypeAndDimensions HasWorkspaceTypeAndDimensions
            =>
                InternalObj?.getHasWorkspaceTypeAndDimensions() != null
                    ? new HasWorkspaceTypeAndDimensions(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.HasWorkspaceTypeAndDimensions)
                            InternalObj.getHasWorkspaceTypeAndDimensions())
                    : null;

        [DisplayName(@"hasLinearMovementRanges")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public InputOutputParamSetCapability_CTCharacteristicsHasLinearMovementRanges HasLinearMovementRanges
            =>
                InternalObj?.getHasRotationalMovementRanges() != null
                    ? new InputOutputParamSetCapability_CTCharacteristicsHasLinearMovementRanges(
                        (
                            fi.tut.mei.resdescapi.rdapi.model.rd.InputOutputParamSetCapabilityCT.Characteristics.
                                HasLinearMovementRanges) InternalObj.getHasLinearMovementRanges())
                    : null;

        [DisplayName(@"hasRotationalMovementRanges")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public InputOutputParamSetCapability_CTCharacteristicsHasRotationalMovementRanges HasRotationalMovementRanges
            =>
                InternalObj?.getHasRotationalMovementRanges() != null
                    ? new InputOutputParamSetCapability_CTCharacteristicsHasRotationalMovementRanges(
                        (
                            fi.tut.mei.resdescapi.rdapi.model.rd.InputOutputParamSetCapabilityCT.Characteristics.
                                HasRotationalMovementRanges) InternalObj.getHasRotationalMovementRanges())
                    : null;
    }
}