using System.Collections.Generic;
using System.ComponentModel;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"CapabilityMapping_CTParameterValueRestriction")]
    public class CapabilityMapping_CTParameterValueRestriction
    {
        public CapabilityMapping_CTParameterValueRestriction()
        {
            
        }
        public override string ToString()
        {
            return Name;
        }

        public CapabilityMapping_CTParameterValueRestriction(
            fi.tut.mei.resdescapi.rdapi.model.rd.CapabilityMappingCT.ParameterValueRestriction obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.CapabilityMappingCT.ParameterValueRestriction InternalObj { get; }

 //todo: va in eccezzione
        //[DisplayName(@"ParameterValueRestriction")]
        //[TypeConverter(typeof (ExpandableObjectConverter))]
        //[Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        //public List<string> ParameterValueRestriction
        //{
        //    get
        //    {
        //        List<string> result = null;
        //        if (InternalObj?.getParameterValues()?.toArray() != null)
        //        {
        //            result = new List<string>();

        //            foreach (
        //                fi.tut.mei.resdescapi.rdapi.model.rd.CapabilityMappingCT.ParameterValueRestriction.
        //                    ParameterValues obj in InternalObj.getParameterValues().toArray())
        //            {
        //                var objType =
        //                    (
        //                        fi.tut.mei.resdescapi.rdapi.model.rd.CapabilityMappingCT.ParameterValueRestriction.
        //                            ParameterValues) obj;
        //                if (objType != null)
        //                {
        //                    result.Add(objType.getValue());
        //                }
        //            }
        //        }
        //        return result;
        //    }
        //}

        [DisplayName(@"IdRef")]
        public string IdRef
        {
            get { return InternalObj?.getIdRef(); }
            set { InternalObj?.setIdRef(value); }
        }

        [DisplayName(@"GidRef")]
        public string GidRef
        {
            get { return InternalObj?.getGidRef(); }
            set { InternalObj?.setGidRef(value); }
        }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }
    }
}