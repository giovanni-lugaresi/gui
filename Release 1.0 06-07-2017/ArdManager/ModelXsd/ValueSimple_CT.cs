using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ValueSimple_CT")]
    public class ValueSimple_CT : VariableBase_CT
    {
        public ValueSimple_CT()
        {
            
        }
        public ValueSimple_CT(fi.tut.mei.resdescapi.rdapi.model.rd.ValueSimpleCT obj) : base(obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ValueSimpleCT InternalObj { get; }

        [DisplayName(@"Id")]
        public string Id
        {
            get { return InternalObj?.getId(); }
            set { InternalObj?.setId(value); }
        }

        [DisplayName(@"Datatype")]
        public string Datatype => InternalObj?.getDatatype()?.value();

        [DisplayName(@"ReqOpt")]
        public string ReqOpt => InternalObj?.getReqOpt()?.value();

        [DisplayName(@"Min")]
        public string Min
        {
            get { return InternalObj?.getMin(); }
            set { InternalObj?.setMin(value); }
        }

        [DisplayName(@"Max")]
        public string Max
        {
            get { return InternalObj?.getMax(); }
            set { InternalObj?.setMax(value); }
        }

        [DisplayName(@"DefaultValue")]
        public string DefaultValue
        {
            get { return InternalObj?.getDefaultValue(); }
            set { InternalObj?.setDefaultValue(value); }
        }

        [DisplayName(@"Value")]
        public string Value
        {
            get { return InternalObj?.getValue(); }
            set { InternalObj?.setValue(value); }
        }
    }
}