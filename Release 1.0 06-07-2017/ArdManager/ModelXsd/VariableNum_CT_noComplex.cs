﻿using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"VariableNum_CT_noComplex")]
    public class VariableNum_CT_noComplex : VariableNum_CT
    {
        public VariableNum_CT_noComplex()
        {
            
        }
        public VariableNum_CT_noComplex(fi.tut.mei.resdescapi.rdapi.model.rd.VariableNumCTNoComplex obj) : base(obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.VariableNumCTNoComplex InternalObj { get; }
    }
}