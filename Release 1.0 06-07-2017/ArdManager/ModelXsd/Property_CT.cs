using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"Property_CT")]

    public class Property_CT : Variable_CT
    {
        public Property_CT()
        {
            
        }

        public Property_CT(fi.tut.mei.resdescapi.rdapi.model.rd.PropertyCT obj) : base(obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.PropertyCT InternalObj { get; }

        //[DisplayName(@"Choices")]

        //public Property_CTChoices Choices
        //    => InternalObj?.getChoices() != null ? new Property_CTChoices(InternalObj.getChoices()) : null;

        [DisplayName(@"Max")]
        public string Max => InternalObj?.getMax().toString();

        [DisplayName(@"Min")]
        public string Min => InternalObj?.getMin().toString();

        [DisplayName(@"Value")]
        public string Value
        {
            get { return InternalObj?.getValue(); }
            set { InternalObj?.setValue(value); }
        }

        [DisplayName(@"DefaultValue")]
        public string DefaultValue
        {
            get { return InternalObj?.getDefaultValue(); }
            set { InternalObj?.setDefaultValue(value); }
        }

        [DisplayName(@"ClassOfProperty")]
        public string ClassOfProperty
        {
            get { return InternalObj?.getClassOfProperty(); }
            set { InternalObj?.setClassOfProperty(value); }
        }

        public string TypeOfProperty => InternalObj?.getTypeOfProperty();

        public static List<Property_CT> Get(object[] array)
        {
            var result = new List<Property_CT>();
            if (array == null)
                return result;
            try
            {
                result =
                    array
                        .OfType
                        <
                            fi.tut.mei.resdescapi.rdapi.model.rd.PropertyCT>()
                        .Select(objType => new Property_CT(objType))
                        .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}