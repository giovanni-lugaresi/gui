using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"Property_CTChoices")]
    public class Property_CTChoices
    {
        public Property_CTChoices()
        {
            
        }
        public Property_CTChoices(fi.tut.mei.resdescapi.rdapi.model.rd.PropertyCT.Choices obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.PropertyCT.Choices InternalObj { get; }


        [DisplayName(@"Choices")]
        public List<string> Choices
        {
            get
            {
                List<string> result = null;
                if (InternalObj?.getChoices()?.toArray() != null)
                {
                    result = InternalObj.getChoices().toArray().Cast<string>().ToList();
                }
                return result;
            }
        }

        [DisplayName(@"SelectedMax")]
        public int? SelectedMax => InternalObj?.getSelectedMax();

        [DisplayName(@"SelectedMin")]
        public int? SelectedMin => InternalObj?.getSelectedMin();
    }
}