﻿using System.Collections.Generic;
using System.ComponentModel;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"SharedResource_CTProperties")]

    public class SharedResource_CTProperties
    {
        public SharedResource_CTProperties()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public SharedResource_CTProperties(fi.tut.mei.resdescapi.rdapi.model.rd.SharedResourceCT.Properties obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.SharedResourceCT.Properties InternalObj { get; }

        [DisplayName(@"Variable")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<Variable_CT_wComplex> Variable => Variable_CT_wComplex.Get(InternalObj?.getVariable()?.toArray());

        [DisplayName(@"VariableRef")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<VariableRef_CT> VariableRef => VariableRef_CT.Get(InternalObj?.getVariableRef()?.toArray());
    }
}