using System.ComponentModel;
using ikvm.extensions;

[DisplayName(@"DOF6_data_CT")]

public class DOF6_data_CT : DOF3_data_CT
{
    public DOF6_data_CT()
    {
        
    }
    public DOF6_data_CT(fi.tut.mei.resdescapi.rdapi.model.rd.DOF6DataCT obj) : base(obj)
    {
        InternalObj = obj;
    }

    [Browsable(false)]
    public fi.tut.mei.resdescapi.rdapi.model.rd.DOF6DataCT InternalObj { get; }

    [DisplayName(@"RotUnit")]
    public string RotUnit
    {
        get { return InternalObj?.getRotUnit(); }
        set { InternalObj?.setRotUnit(value); }
    }

    [DisplayName(@"RotIncrement")]
    public string RotIncrement => InternalObj?.getRotIncrement().toString();

    [DisplayName(@"RotDecade")]
    public string RotDecade => InternalObj?.getRotDecade().toString();

    [DisplayName(@"RotX")]
    public string RotX => InternalObj?.getRotX().toString();

    [DisplayName(@"RotY")]
    public string RotY => InternalObj?.getRotY().toString();

    [DisplayName(@"RotZ")]
    public string RotZ => InternalObj?.getRotZ().toString();
}