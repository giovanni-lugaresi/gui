using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ConnectionMethodCtrlAndCommRef")]

    public class ConnectionMethodCtrlAndCommRef
    {
        public ConnectionMethodCtrlAndCommRef()
        {
            
        }
        public override string ToString()
        {
            return IdRef.ToString();
        }

        public ConnectionMethodCtrlAndCommRef(fi.tut.mei.resdescapi.rdapi.model.rd.ConnectionMethodCtrlAndCommRef obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ConnectionMethodCtrlAndCommRef InternalObj { get; }

        [DisplayName(@"MethodRelatedData")]

        public ExtensionDataCt MethodRelatedData =>
            InternalObj?.getMethodRelatedData() != null
                ? new ExtensionDataCt(
                    (fi.tut.mei.resdescapi.rdapi.model.rd.ExtensionDataCT)InternalObj.getMethodRelatedData())
                : null;

        [DisplayName(@"IdRef")]
        public object IdRef
        {
            get { return InternalObj?.getIdRef(); }
            set { InternalObj?.setIdRef(value); }
        }
    }
}