﻿using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ValueComplex_CT")]

    public class VariableString_CT : Variable_CT_wComplex
    {
        public VariableString_CT()
        {
            
        }
        public VariableString_CT(fi.tut.mei.resdescapi.rdapi.model.rd.VariableStringCT obj) : base(obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.VariableStringCT InternalObj { get; }

        [DisplayName(@"Min")]
        public string Min
        {
            get { return InternalObj?.getMin(); }
            set { InternalObj?.setMin(value); }
        }

        [DisplayName(@"Max")]
        public string Max
        {
            get { return InternalObj?.getMax(); }
            set { InternalObj?.setMax(value); }
        }

        [DisplayName(@"DefaultValue")]
        public string DefaultValue
        {
            get { return InternalObj?.getDefaultValue(); }
            set { InternalObj?.setDefaultValue(value); }
        }

        [DisplayName(@"Value")]
        public string Value
        {
            get { return InternalObj?.getValue(); }
            set { InternalObj?.setValue(value); }
        }
    }
}