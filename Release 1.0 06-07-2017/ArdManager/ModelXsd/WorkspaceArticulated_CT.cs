using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"WorkspaceArticulated_CT")]
    public class WorkspaceArticulated_CT
    {
        public WorkspaceArticulated_CT()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public WorkspaceArticulated_CT(
            fi.tut.mei.resdescapi.rdapi.model.rd.WorkspaceArticulatedCT
                obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.WorkspaceArticulatedCT
            InternalObj
        { get; }

      
        [DisplayName(@"AngleA")]
        public string AngleA
        {
            get { return InternalObj?.getAngleA(); }
            set { InternalObj?.setAngleA(value); }
        }
        [DisplayName(@"AngleB")]
        public string AngleB
        {
            get { return InternalObj?.getAngleB(); }
            set { InternalObj?.setAngleB(value); }
        }
        [DisplayName(@"AngleC")]
        public string AngleC
        {
            get { return InternalObj?.getAngleC(); }
            set { InternalObj?.setAngleC(value); }
        }
    }
}