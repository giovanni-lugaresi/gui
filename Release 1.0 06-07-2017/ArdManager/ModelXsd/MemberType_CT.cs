﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"MemberType_CT")]

    public class MemberType_CT
    {
        public MemberType_CT()
        {
            
        }
        public override string ToString()
        {
            return Name;
        }

        public MemberType_CT(fi.tut.mei.resdescapi.rdapi.model.rd.MemberTypeCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.MemberTypeCT InternalObj { get; }

        [DisplayName(@"Type")]
        public string Type
        {
            get { return InternalObj?.getType(); }
            set { InternalObj?.setType(value); }
        }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }


        [DisplayName(@"Comment")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public LocalizedString_CT Comment
            =>
                InternalObj?.getComment() != null
                    ? new LocalizedString_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.LocalizedStringCT) InternalObj.getComment())
                    : null;


        public static List<MemberType_CT> Get(object[] array)
        {
            var result = new List<MemberType_CT>();
            if (array == null)
                return result;
            try
            {
                result =
                    array
                        .OfType
                        <
                            fi.tut.mei.resdescapi.rdapi.model.rd.MemberTypeCT>()
                        .Select(objType => new MemberType_CT(objType))
                        .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}