﻿using System.ComponentModel;
using ikvm.extensions;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"PropertiesForRD_CTPhysicalPropertiesMass")]
    public   class PropertiesForRD_CTPhysicalPropertiesMass
    {
        public PropertiesForRD_CTPhysicalPropertiesMass()
        {
            
        }
        public PropertiesForRD_CTPhysicalPropertiesMass(
            fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.PhysicalProperties.Mass resourceDescription)
        {
            InternalObj = resourceDescription;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.PhysicalProperties.Mass InternalObj { get; }


        //[DisplayName(@"Unit")]
        //public string Unit
        //{
          //  get { return InternalObj?.getUnit(); }
            //set { InternalObj?.setUnit(value); }
        //}

        //[DisplayName(@"Value")]
        //public string Value => InternalObj?.getValue().toString();


        [DisplayName(@"Unit")]
        public string Unit => InternalObj?.getUnit().toString();
        

        [DisplayName(@"Value")]
        public string Value
        {
             get { return InternalObj?.getUnit(); }
            set { InternalObj?.setUnit(value); }
            }
        }
    }