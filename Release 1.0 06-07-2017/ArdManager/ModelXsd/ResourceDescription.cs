﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using ArdManager.Model;
using fi.tut.mei.resdescapi.rdapi.model.rd;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"Resource Description")]
    public class ResourceDescription
    {
        public ResourceDescription()
        {
            
        }
        public string Label { get; internal set; }

        public ResourceDescription(fi.tut.mei.resdescapi.rdapi.model.rd.ResourceDescription resourceDescription,
            string label)
        {
            InternalObj = resourceDescription;
            Label = label;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ResourceDescription InternalObj { get; }

        [DisplayName(@"SharedResources")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public SharedResource_CT SharedResources
            =>
                InternalObj?.getSharedResources() != null
                    ? new SharedResource_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.SharedResourceCT) InternalObj.getSharedResources())
                    : null;

        [DisplayName(@"DescriptionVersion")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public Version_CT DescriptionVersion
            =>
                InternalObj?.getDescriptionVersion() != null
                    ? new Version_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.VersionCT) InternalObj.getDescriptionVersion())
                    : null;

        [DisplayName(@"Extension")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<ExtensionDataCt> Extension
            => ExtensionDataCt.Get(InternalObj?.getExtension()?.toArray());

        [DisplayName(@"Resource")]
        [Category("Resource")]
        public Resource_CT Resource
            =>
                InternalObj?.getResource()?.toArray()[0] != null
                    ? new Resource_CT((ResourceCT) InternalObj.getResource().toArray()[0])
                    : null;

        [DisplayName(@"General Information")]
        [Category("GeneralInformation")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public GeneralInformation GeneralInformation
            =>
                InternalObj != null
                    ? new GeneralInformation(InternalObj)
                    : null;
    }
}