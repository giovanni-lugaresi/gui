﻿using System.ComponentModel;
using ikvm.extensions;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"PropertiesForRD_CTPhysicalPropertiesLocalOrigin")]

    public class PropertiesForRD_CTPhysicalPropertiesLocalOrigin
    {
        public PropertiesForRD_CTPhysicalPropertiesLocalOrigin()
        {
            
        }
        public PropertiesForRD_CTPhysicalPropertiesLocalOrigin(
            fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.PhysicalProperties.LocalOrigin obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.PhysicalProperties.LocalOrigin InternalObj { get; }
 

        [DisplayName(@"XHTML")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public XHTML_Doc_CT Xhtml =>
            InternalObj?.getXHTML() != null
                ? new XHTML_Doc_CT(InternalObj?.getXHTML())
                : null;

        [DisplayName(@"IFInstanceRef")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public string  IfInstanceRef => InternalObj?.getIFInstanceRef().toString();
    }
}