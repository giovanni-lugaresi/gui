﻿using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ValueComplex_CT")]
    public class ValueComplex_CT
    {
        public ValueComplex_CT()
        {
            
        }
        public ValueComplex_CT(fi.tut.mei.resdescapi.rdapi.model.rd.ValueComplexCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ValueComplexCT InternalObj { get; }


        [DisplayName(@"ValueArrayCt")]
        public ValueArray_CT ValueArrayCt
            => new ValueArray_CT(InternalObj?.getArrayValue());

        [DisplayName(@"ValueArrayCt")]
        public ValueEnum_CT ValueEnumCt
            => new ValueEnum_CT(InternalObj?.getEnumValue());

        [DisplayName(@"ValueArrayCt")]
        public ValueStruct_CT ValueStructCt
            => new ValueStruct_CT(InternalObj?.getStructValue());

        [DisplayName(@"ValueArrayCt")]
        public ValueSubRange_CT ValueSubRangeCt
            => new ValueSubRange_CT(InternalObj?.getSubRangeValue());


        [DisplayName(@"IdRefComplexType")]
        public string IdRefComplexType => InternalObj?.getIdRefComplexType().ToString();


        [DisplayName(@"Type")]
        public string Type => InternalObj?.getType();
    }
}