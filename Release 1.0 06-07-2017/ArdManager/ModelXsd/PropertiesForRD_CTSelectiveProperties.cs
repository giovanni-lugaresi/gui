using System.Collections.Generic;
using System.ComponentModel;
using ArdManager.ModelXsd;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

[DisplayName(@"PropertiesForRD_CTSelectiveProperties")]

public class PropertiesForRD_CTSelectiveProperties
{
    public PropertiesForRD_CTSelectiveProperties()
    {
        
    }
    public PropertiesForRD_CTSelectiveProperties(
        fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.SelectiveProperties resourceDescription)
    {
        InternalObj = resourceDescription;
    }

    [Browsable(false)]
    public fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.SelectiveProperties InternalObj { get; }

    [DisplayName(@"Variable")]
    [TypeConverter(typeof (ExpandableObjectConverter))]
    [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
    public List<Variable_CT_wComplex> Variable => Variable_CT_wComplex.Get(InternalObj?.getVariable()?.toArray());

    [DisplayName(@"VariableRef")]
    [TypeConverter(typeof (ExpandableObjectConverter))]
    [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
    public List<VariableRef_CT> VariableRef => VariableRef_CT.Get(InternalObj?.getVariableRef()?.toArray());
}