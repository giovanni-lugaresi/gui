﻿using System.Collections.Generic;
using System.ComponentModel;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;
using System;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"PropertiesForRD_CTBusinessAndLifeCycleProperties")]

    public class PropertiesForRD_CTBusinessAndLifeCycleProperties
    {
        public PropertiesForRD_CTBusinessAndLifeCycleProperties()
        {
            
        }
        public PropertiesForRD_CTBusinessAndLifeCycleProperties(
            fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.BusinessAndLifeCycleProperties obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.BusinessAndLifeCycleProperties InternalObj { get;
        }

        //todo: va in eccezione
        //[DisplayName(@"BusinessProperty")]
        //[TypeConverter(typeof(ExpandableObjectConverter))]
        //[Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        //public List<BusinessProperty_CT> BusinessProperty
        //   => BusinessProperty_CT.Get(InternalObj?.getBusinessProperty()?.toArray());

        [DisplayName(@"VariableRef")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<VariableRef_CT> VariableRef => VariableRef_CT.Get(InternalObj?.getVariableRef()?.toArray());
    }
}