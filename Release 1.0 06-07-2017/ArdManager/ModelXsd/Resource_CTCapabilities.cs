using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
        [DisplayName(@"Resource_CTCapabilities")]
    public class Resource_CTCapabilities
    {
            public Resource_CTCapabilities()
            {
                
            }
        public override string ToString()
        {
            return base.ToString();
        }

        public Resource_CTCapabilities(fi.tut.mei.resdescapi.rdapi.model.rd.ResourceCT.Capabilities obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ResourceCT.Capabilities InternalObj { get; }
 

        [DisplayName(@"PlaceHolder_CapabilityModelAsOWL")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Resource_CTCapabilitiesPlaceHolder_CapabilityModelAsOWL PlaceHolderCapabilityModelAsOwl =>
                InternalObj?.getPlaceHolderCapabilityModelAsOWL() != null
                    ? new Resource_CTCapabilitiesPlaceHolder_CapabilityModelAsOWL(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.ResourceCT.Capabilities.PlaceHolderCapabilityModelAsOWL)
                            InternalObj.getPlaceHolderCapabilityModelAsOWL())
                    : null;

        //todo: non esiste
        //[DisplayName(@"PlaceHolder_Temp")]
        //[TypeConverter(typeof(ExpandableObjectConverter))]
        //public Resource_CTCapabilitiesPlaceHolder_Temp PlaceHolder_Temp
        // =>
        //        InternalObj?.te() != null
        //            ? new InputOutputParamSetCapability_CT(
        //                (fi.tut.mei.resdescapi.rdapi.model.rd.InputOutputParamSetCapabilityCT)
        //                    InternalObj.getParameterPorts())
        //            : null;



        [DisplayName(@"Capability")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<Capability> Capabilitylst => Capability.Get(InternalObj?.getCapability()?.toArray());



        public static List<Resource_CTCapabilities> Get(object[] array)
        {
            var result = new List<Resource_CTCapabilities>();
            if (array == null)
                return result;
            try
            {
                result = array.OfType<fi.tut.mei.resdescapi.rdapi.model.rd.ResourceCT.Capabilities>()
                    .Select(objType => new Resource_CTCapabilities(objType))
                    .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}