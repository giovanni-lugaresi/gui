using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"InterfaceImplRD_CTInstanceAreaCircle")]

    public class InterfaceImplRD_CTInstanceAreaCircle
    {
        public InterfaceImplRD_CTInstanceAreaCircle()
        {
            
        }
        public InterfaceImplRD_CTInstanceAreaCircle(fi.tut.mei.resdescapi.rdapi.model.rd.InterfaceImplRDCT.Instances.Instance.Area.Circle obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.InterfaceImplRDCT.Instances.Instance.Area.Circle InternalObj { get; }

        [DisplayName(@"R")]
        public string R => InternalObj?.getR().ToString();
    }
}