using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ExecCapas")]
    public class ExecCapabilities_CTExecCapas
    {
        public ExecCapabilities_CTExecCapas()
        {
            
        }
        public ExecCapabilities_CTExecCapas(fi.tut.mei.resdescapi.rdapi.model.rd.ExecCapabilitiesCT.ExecCapas obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ExecCapabilitiesCT.ExecCapas InternalObj { get; }

        [DisplayName(@"Exec Capabilities")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<ExecCapa_CT> ExecCapabilities
        {
            get
            {
                List<ExecCapa_CT> result = null;
                if (InternalObj?.getExecCapa()?.toArray() != null)
                {
                    result = new List<ExecCapa_CT>();

                    foreach (var obj in InternalObj.getExecCapa().toArray())
                    {
                        var objType = obj;
                        if (objType is fi.tut.mei.resdescapi.rdapi.model.rd.ExecCapaCT)
                        {
                            result.Add(new ExecCapa_CT((fi.tut.mei.resdescapi.rdapi.model.rd.ExecCapaCT) objType));
                        }
                    }
                }
                return result;
            }
        }

        [DisplayName(@"Exec Capa Ref")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<string> ExecCapaRef
        {
            get
            {
                List<string> result = null;
                if (InternalObj?.getExecCapaRef()?.toArray() != null)
                {
                    result = InternalObj.getExecCapaRef().toArray().Select(obj => obj.ToString()).ToList();
                }
                return result;
            }
        }
    }
}