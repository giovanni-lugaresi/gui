﻿using System.Collections.Generic;
using System.ComponentModel;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ModuleInfo_CT")]
    public class ModuleInfo_CT
    {
        public ModuleInfo_CT()
        {
            
        }
        public ModuleInfo_CT(fi.tut.mei.resdescapi.rdapi.model.rd.ModuleInfoCT resourceDescription)
        {
            InternalObj = resourceDescription;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ModuleInfoCT InternalObj { get; }

        [DisplayName(@"Labels")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<LocalizedString_CT> Labels => LocalizedString_CT.Get(InternalObj?.getLabel()?.toArray());

        [DisplayName(@"Comments")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<LocalizedString_CT> Comments => LocalizedString_CT.Get(InternalObj?.getComment()?.toArray());

        [DisplayName(@"Documentations")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<Documentation_CT> Documentations => Documentation_CT.Get(InternalObj?.getDocumentation()?.toArray())
            ;

        [DisplayName(@"Vendor")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public Company_CT Vendor =>
            InternalObj?.getVendor() != null
                ? new Company_CT(InternalObj?.getVendor())
                : null;

        [DisplayName(@"Classification")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public Classification_CT Classification =>
            InternalObj?.getClassification() != null
                ? new Classification_CT(InternalObj?.getClassification())
                : null;


        [DisplayName(@"AbstractModelOfEquipment")]
        public XHTML_Doc_CT AbstractModelOfEquipment =>
            InternalObj?.getAbstractModelOfEquipment() != null
                ? new XHTML_Doc_CT(InternalObj?.getAbstractModelOfEquipment())
                : null;


        [DisplayName(@"FunctionalDescription")]
        public XHTML_Doc_CT XhtFunctionalDescriptionml =>
            InternalObj?.getFunctionalDescription() != null
                ? new XHTML_Doc_CT(InternalObj?.getFunctionalDescription())
                : null;


        [DisplayName(@"File_Image")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<File_Image> FileImagelst => File_Image.Get(InternalObj?.getFileImage()?.toArray());

        [DisplayName(@"File_CAD")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<File_CAD> FileCaDlst => File_CAD.Get(InternalObj?.getFileCAD()?.toArray());
    }
}