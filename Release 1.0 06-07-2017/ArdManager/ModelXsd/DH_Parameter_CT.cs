using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using ikvm.extensions;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"DH_Parameter_CT")]

    public class DH_Parameter_CT
    {
        public DH_Parameter_CT()
        {
            
        }
        public DH_Parameter_CT(fi.tut.mei.resdescapi.rdapi.model.rd.DHParameterCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.DHParameterCT InternalObj { get; }

        [DisplayName(@"Parameter")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<DH_Parameter_CTParameter> Parameter
            =>
                DH_Parameter_CTParameter.Get(
                    InternalObj?.getParameter()?.toArray());

        [DisplayName(@"rowNumber")]
        public string RowNumber => InternalObj?.getRowNumber().toString();

        [DisplayName(@"linkLength")]
        public string linkLength => InternalObj?.getLinkLength().toString();

        [DisplayName(@"linkTwist")]
        public string LinkTwist => InternalObj?.getLinkTwist().toString();

        [DisplayName(@"linkOffset")]
        public string LinkOffset => InternalObj?.getLinkOffset().toString();

        [DisplayName(@"jointAngle")]
        public string JointAngle => InternalObj?.getJointAngle().toString();

        public static List<DH_Parameter_CT> Get(object[] array)
        {
            var result = new List<DH_Parameter_CT>();
            if (array == null)
                return result;
            try
            {
                result =
                    array
                        .OfType
                        <
                            fi.tut.mei.resdescapi.rdapi.model.rd.DHParameterCT>()
                        .Select(objType => new DH_Parameter_CT(objType))
                        .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }

    }
}