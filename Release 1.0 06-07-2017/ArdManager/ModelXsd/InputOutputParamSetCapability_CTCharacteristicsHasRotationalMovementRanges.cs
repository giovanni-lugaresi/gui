using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"InputOutputParamSetCapability_CTCharacteristicsHasRotationalMovementRanges")]
    public class InputOutputParamSetCapability_CTCharacteristicsHasRotationalMovementRanges
    {
        public InputOutputParamSetCapability_CTCharacteristicsHasRotationalMovementRanges()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public InputOutputParamSetCapability_CTCharacteristicsHasRotationalMovementRanges(
            fi.tut.mei.resdescapi.rdapi.model.rd.InputOutputParamSetCapabilityCT.Characteristics.HasRotationalMovementRanges obj) 
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.InputOutputParamSetCapabilityCT.Characteristics.HasRotationalMovementRanges InternalObj { get; }

        [DisplayName(@"RotationalMovementRanges")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public MovementRangeRotational_CT RotationalMovementRanges
            =>
                InternalObj?.getRotationalMovementRanges() != null
                    ? new MovementRangeRotational_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.MovementRangeRotationalCT)InternalObj.getRotationalMovementRanges())
                    : null;
    }
}