using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"InterfacesForRD_CT")]

    public class InterfacesForRD_CT
    {
        public InterfacesForRD_CT()
        {
            
        }
        public InterfacesForRD_CT(fi.tut.mei.resdescapi.rdapi.model.rd.InterfacesForRDCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.InterfacesForRDCT InternalObj { get; }

        [DisplayName(@"AbstractInterfacePorts")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<AbstractInterfacePort> AbstractInterfacePorts
            =>
                AbstractInterfacePort.Get(
                    InternalObj?.getAbstractInterfacePorts()?.getAbstractInterfacePort()?.toArray());

        [DisplayName(@"StdBodies")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public InterfacesForRD_CTStdBodies StdBodies
            =>
                InternalObj?.getStdBodies() != null
                    ? new InterfacesForRD_CTStdBodies(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.InterfacesForRDCT.StdBodies) InternalObj.getStdBodies())
                    : null;

        //todo: va in eccezione
        //[DisplayName(@"InterfaceStds")]
        //[TypeConverter(typeof (ExpandableObjectConverter))]
        //public InterfacesForRD_CTInterfaceStds InterfaceStds
        //    =>
        //        InternalObj?.getInterfaceStds() != null
        //            ? new InterfacesForRD_CTInterfaceStds(
        //                (fi.tut.mei.resdescapi.rdapi.model.rd.InterfacesForRDCT.InterfaceStds)InternalObj.getInterfaceStds())
        //            : null;

        [DisplayName(@"IFImplementations")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public InterfacesForRD_CTIFImplementations IFImplementations
              =>
                InternalObj?.getIFImplementations() != null
                    ? new InterfacesForRD_CTIFImplementations(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.InterfacesForRDCT.IFImplementations)InternalObj.getIFImplementations())
                    : null;
    }

    [DisplayName(@"InterfaceImplRD_CT")]

    public class InterfaceImplRD_CT
    {
        public InterfaceImplRD_CT(fi.tut.mei.resdescapi.rdapi.model.rd.InterfaceImplRDCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.InterfaceImplRDCT InternalObj { get; }

        [DisplayName(@"InterfaceRef")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public InterfaceRef_CT InterfaceRef
            =>
                InternalObj?.getInterfaceRef() != null
                    ? new InterfaceRef_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.InterfaceRefCT) InternalObj.getInterfaceRef())
                    : null;

        [DisplayName(@"ForceAndTorqueLimits")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public DOF6_dataLimits_CT ForceAndTorqueLimits
            =>
                InternalObj?.getForceAndTorqueLimits() != null
                    ? new DOF6_dataLimits_CT(InternalObj?.getForceAndTorqueLimits())
                    : null;

        [DisplayName(@"Instances")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<InterfaceImplRD_CTInstance> Instances
            =>
                InterfaceImplRD_CTInstance.Get(
                    InternalObj?.getInstances()?.getInstance()?.toArray());

        [DisplayName(@"Id")]
        public string Id
        {
            get { return InternalObj?.getId(); }
            set { InternalObj?.setId(value); }
        }
    }

    [DisplayName(@"InterfaceImplRD_CTInstance")]

    public class InterfaceImplRD_CTInstance
    {
        public InterfaceImplRD_CTInstance(fi.tut.mei.resdescapi.rdapi.model.rd.InterfaceImplRDCT.Instances.Instance obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.InterfaceImplRDCT.Instances.Instance InternalObj { get; }
 
        [DisplayName(@"Location")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Vector_CT Location
           =>
               InternalObj?.getLocation() != null
                   ? new Vector_CT(
                       (fi.tut.mei.resdescapi.rdapi.model.rd.VectorCT)
                           InternalObj.getLocation())
                   : null;


        [DisplayName(@"Orientation")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public RotationVector_CT Orientation
          =>
                InternalObj?.getOrientation() != null
                    ? new RotationVector_CT(InternalObj?.getOrientation())
                    : null;


        [DisplayName(@"MatrixLocations")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public MatrixLocations_CT MatrixLocations
            =>
                InternalObj?.getMatrixLocations() != null
                    ? new MatrixLocations_CT(InternalObj?.getMatrixLocations())
                    : null;

        [DisplayName(@"Area")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public InterfaceImplRD_CTInstanceArea Area
         =>
                InternalObj?.getArea() != null
                    ? new InterfaceImplRD_CTInstanceArea(InternalObj?.getArea())
                    : null;
 
        [DisplayName(@"ForceAndTorqueLimits")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public DOF6_dataLimits_CT ForceAndTorqueLimits
           =>
                InternalObj?.getForceAndTorqueLimits() != null
                    ? new DOF6_dataLimits_CT(InternalObj?.getForceAndTorqueLimits())
                    : null;

        [DisplayName(@"DH_set")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<DH_Parameter_CT> DhSet
          =>
              DH_Parameter_CT.Get(
                  InternalObj?.getParameters()?.getParameter().toArray());


        [DisplayName(@"Parameters")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<VariableNum_CT> Parameters
           =>
               VariableNum_CT.Get(
                   InternalObj?.getParameters()?.getParameter()?.toArray());


        [DisplayName(@"Id")]
        public string Id
        {
            get { return InternalObj?.getId(); }
            set { InternalObj?.setId(value); }
        }

        [DisplayName(@"positiveInteger")]
        public string InterfacePortRefQualifier => InternalObj?.getInterfacePortRefQualifier().toString();

        [DisplayName(@"ReqOpt")]
        public string ReqOpt => InternalObj?.getReqOpt()?.value();
 
        public static List<InterfaceImplRD_CTInstance> Get(object[] array)
        {
            var result = new List<InterfaceImplRD_CTInstance>();
            if (array == null)
                return result;
            try
            {
                result =
                    array
                        .OfType
                        <
                            fi.tut.mei.resdescapi.rdapi.model.rd.InterfaceImplRDCT.Instances.Instance>()
                        .Select(objType => new InterfaceImplRD_CTInstance(objType))
                        .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }


    [DisplayName(@"RotationVector_CT")]

    public class RotationVector_CT
    {
        public RotationVector_CT(fi.tut.mei.resdescapi.rdapi.model.rd.RotationVectorCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.RotationVectorCT InternalObj { get; }

        [DisplayName(@"x")]
        public string X => InternalObj?.getX().ToString();

        [DisplayName(@"y")]
        public string Y => InternalObj?.getY().ToString();

        [DisplayName(@"z")]
        public string Z => InternalObj?.getZ().ToString();

        [DisplayName(@"angle")]
        public string Angle => InternalObj?.getAngle().ToString();
    }


    [DisplayName(@"MatrixLocations_CT")]

    public class MatrixLocations_CT
    {

        public MatrixLocations_CT(fi.tut.mei.resdescapi.rdapi.model.rd.MatrixLocationsCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.MatrixLocationsCT InternalObj { get; }


        [DisplayName(@"X")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public PitchAndQuantity_CT X
           =>
                InternalObj?.getX() != null
                    ? new PitchAndQuantity_CT(InternalObj?.getX())
                    : null;

        [DisplayName(@"Y")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public PitchAndQuantity_CT Y
          =>
               InternalObj?.getY() != null
                   ? new PitchAndQuantity_CT(InternalObj?.getY())
                   : null;

        [DisplayName(@"Z")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public PitchAndQuantity_CT Z
          =>
               InternalObj?.getZ() != null
                   ? new PitchAndQuantity_CT(InternalObj?.getZ())
                   : null;
    }

    [DisplayName(@"PitchAndQuantity_CT")]
    public class PitchAndQuantity_CT
    { 
        public PitchAndQuantity_CT(fi.tut.mei.resdescapi.rdapi.model.rd.PitchAndQuantityCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.PitchAndQuantityCT InternalObj { get; }
 
        [DisplayName(@"pitch")]
        public string Pitch => InternalObj?.getPitch().ToString();

        [DisplayName(@"quantity")]
        public string Quantity => InternalObj?.getQuantity().toString();
    }
}