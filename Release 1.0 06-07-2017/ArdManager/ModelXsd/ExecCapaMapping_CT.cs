﻿using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ExecCapaMapping_CT")]
    public class ExecCapaMapping_CT : ExecCapaRef_CT
    {
        public ExecCapaMapping_CT()
        {
            
        }
        public ExecCapaMapping_CT(fi.tut.mei.resdescapi.rdapi.model.rd.ExecCapaMappingCT obj) : base(obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ExecCapaMappingCT InternalObj { get; }

        [DisplayName(@"MappingType")]
        public string MappingType => InternalObj?.getMappingType();
    }
}