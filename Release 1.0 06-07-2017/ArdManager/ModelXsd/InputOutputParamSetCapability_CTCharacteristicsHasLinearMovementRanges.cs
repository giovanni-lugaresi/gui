using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"InputOutputParamSetCapability_CTCharacteristicsHasRotationalMovementRanges")]
    public class InputOutputParamSetCapability_CTCharacteristicsHasLinearMovementRanges
    {
        public InputOutputParamSetCapability_CTCharacteristicsHasLinearMovementRanges()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public InputOutputParamSetCapability_CTCharacteristicsHasLinearMovementRanges(
            fi.tut.mei.resdescapi.rdapi.model.rd.InputOutputParamSetCapabilityCT.Characteristics.HasLinearMovementRanges
                obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public
            fi.tut.mei.resdescapi.rdapi.model.rd.InputOutputParamSetCapabilityCT.Characteristics.HasLinearMovementRanges
            InternalObj { get; }

        [DisplayName(@"LinearMovementRanges")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public MovementRangeLinear_CT LinearMovementRanges
            =>
                InternalObj?.getLinearMovementRanges() != null
                    ? new MovementRangeLinear_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.MovementRangeLinearCT)
                            InternalObj.getLinearMovementRanges())
                    : null;
    }
}