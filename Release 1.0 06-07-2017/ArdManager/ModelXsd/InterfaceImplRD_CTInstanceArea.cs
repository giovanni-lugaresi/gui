using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"InterfaceImplRD_CTInstanceArea")]

    public class InterfaceImplRD_CTInstanceArea
    {
        public InterfaceImplRD_CTInstanceArea()
        {
            
        }

        public InterfaceImplRD_CTInstanceArea(
            fi.tut.mei.resdescapi.rdapi.model.rd.InterfaceImplRDCT.Instances.Instance.Area obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.InterfaceImplRDCT.Instances.Instance.Area InternalObj { get; }

        [DisplayName(@"Circle")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public InterfaceImplRD_CTInstanceAreaCircle Circle
            =>
                InternalObj?.getCircle() != null
                    ? new InterfaceImplRD_CTInstanceAreaCircle(InternalObj?.getCircle())
                    : null;

        [DisplayName(@"Rectangle")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public InterfaceImplRD_CTInstanceAreaRectangle Rectangle
            =>
                InternalObj?.getRectangle() != null
                    ? new InterfaceImplRD_CTInstanceAreaRectangle(InternalObj?.getRectangle())
                    : null;

        [DisplayName(@"Polygon")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public InterfaceImplRD_CTInstanceAreaPolygon Polygon
            =>
                InternalObj?.getPolygon() != null
                    ? new InterfaceImplRD_CTInstanceAreaPolygon(InternalObj?.getPolygon())
                    : null;
    }
}