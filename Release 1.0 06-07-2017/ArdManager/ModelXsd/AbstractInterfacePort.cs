using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"AbstractInterfacePort")]

    public class AbstractInterfacePort
    {
        public AbstractInterfacePort()
        {
            
        }
        public override string ToString()
        {
            return Name;
        }

        public AbstractInterfacePort(fi.tut.mei.resdescapi.rdapi.model.rd.AbstractInterfacePort obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.AbstractInterfacePort InternalObj { get; }


        [DisplayName(@"Id")]
        public string Id
        {
            get { return InternalObj?.getId(); }
            set { InternalObj?.setId(value); }
        }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }

        [DisplayName(@"Description")]
        public string Description
        {
            get { return InternalObj?.getDescription(); }
            set { InternalObj?.setDescription(value); }
        }

        [DisplayName(@"functionClass")]
        public string FunctionClass => InternalObj?.getFunctionClass()?.ToString();

        [DisplayName(@"Purpose")]
        public string Purpose
        {
            get { return InternalObj?.getPurpose(); }
            set { InternalObj?.setPurpose(value); }
        }
 
        public static List<AbstractInterfacePort> Get(object[] array)
        {
            var result = new List<AbstractInterfacePort>();
            if (array == null)
                return result;
            try
            {
                result =
                    array
                        .OfType
                        <
                            fi.tut.mei.resdescapi.rdapi.model.rd.AbstractInterfacePort>()
                        .Select(objType => new AbstractInterfacePort(objType))
                        .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}