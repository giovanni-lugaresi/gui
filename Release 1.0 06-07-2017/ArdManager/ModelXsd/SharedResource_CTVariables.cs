﻿using System.Collections.Generic;
using System.ComponentModel;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"SharedResource_CTVariables")]
    public class SharedResource_CTVariables
    {
        public SharedResource_CTVariables()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public SharedResource_CTVariables(fi.tut.mei.resdescapi.rdapi.model.rd.SharedResourceCT.Variables obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.SharedResourceCT.Variables InternalObj { get; }

        [DisplayName(@"Variable")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<Variable_CT_wComplex> Variable => Variable_CT_wComplex.Get(InternalObj?.getVariable()?.toArray());

        [DisplayName(@"VariableRef")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<VariableRef_CT> VariableRef => VariableRef_CT.Get(InternalObj?.getVariableRef()?.toArray());
    }
}