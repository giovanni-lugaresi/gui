using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"InputOutputParamSetCapability_CTCharacteristicsHasAllowedItemShapeAndSize_max")]

    public class InputOutputParamSetCapability_CTCharacteristicsHasAllowedItemShapeAndSize_max
    {
        public InputOutputParamSetCapability_CTCharacteristicsHasAllowedItemShapeAndSize_max()
        {
            
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public InputOutputParamSetCapability_CTCharacteristicsHasAllowedItemShapeAndSize_max(
            fi.tut.mei.resdescapi.rdapi.model.rd.InputOutputParamSetCapabilityCT.Characteristics.HasAllowedItemShapeAndSizeMax obj)
        {
            InternalObj = obj;

        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.InputOutputParamSetCapabilityCT.Characteristics.HasAllowedItemShapeAndSizeMax InternalObj { get; }

        ////todo: non esiste la get
        // [DisplayName(@"BoxShape")]
        // [TypeConverter(typeof (ExpandableObjectConverter))]
        // [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        // public List<ShapeBox_CT> BoxShape => ShapeBox_CT.Get(InternalObj?.box()?.toArray());

        ////todo: non esiste la get
        //[DisplayName(@"CylinderShape")]
        //[TypeConverter(typeof (ExpandableObjectConverter))]
        //[Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        //public List<ShapeCylinder_CT> CylinderShape => ShapeCylinder_CT.Get(InternalObj?.get()?.toArray());


        ////todo: non esiste la get
        //[DisplayName(@"ConeShape")]
        //[TypeConverter(typeof (ExpandableObjectConverter))]
        //[Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        //public List<ShapeCone_CT> ConeShape => ShapeCone_CT.Get(InternalObj?.ge()?.toArray());


        ////todo: non esiste la get
        //[DisplayName(@"PyramidShape")]
        //[TypeConverter(typeof(ExpandableObjectConverter))]
        //[Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        //public List<ShapePyramid_CT> PyramidShape => ShapePyramid_CT.Get(InternalObj?.get()?.toArray());

        [DisplayName(@"SphereShape")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<ShapeBox_CT> SphereShape => ShapeBox_CT.Get(InternalObj?.getGShapeDefinition()?.toArray());

        public static List<InputOutputParamSetCapability_CTCharacteristicsHasAllowedItemShapeAndSize_max> Get(object[] array)
        {
            var result = new List<InputOutputParamSetCapability_CTCharacteristicsHasAllowedItemShapeAndSize_max>();
            if (array == null)
                return result;
            try
            {
                result =
                    array
                        .OfType
                        <
                            fi.tut.mei.resdescapi.rdapi.model.rd.InputOutputParamSetCapabilityCT.Characteristics.HasAllowedItemShapeAndSizeMax>()
                        .Select(objType => new InputOutputParamSetCapability_CTCharacteristicsHasAllowedItemShapeAndSize_max(objType))
                        .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}