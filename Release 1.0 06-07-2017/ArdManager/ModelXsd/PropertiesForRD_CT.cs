using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"PropertiesForRD_CT")]

    public class PropertiesForRD_CT
    {

        public PropertiesForRD_CT()
        {
            
        }
        public PropertiesForRD_CT(fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT InternalObj { get; }


        [DisplayName(@"SelectiveProperties")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public PropertiesForRD_CTSelectiveProperties SelectiveProperties =>
            InternalObj?.getSelectiveProperties() != null
                ? new PropertiesForRD_CTSelectiveProperties(
                    (fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.SelectiveProperties)
                        InternalObj.getSelectiveProperties())
                : null;

        [DisplayName(@"PhysicalProperties")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public PropertiesForRD_CTPhysicalProperties PhysicalProperties
            => InternalObj?.getPhysicalProperties() != null
                ? new PropertiesForRD_CTPhysicalProperties(
                    (fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.PhysicalProperties)
                        InternalObj.getPhysicalProperties())
                : null;

        [DisplayName(@"TechnicalProperties")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public PropertiesForRD_CTTechnicalProperties TechnicalProperties
            => InternalObj?.getTechnicalProperties() != null
                ? new PropertiesForRD_CTTechnicalProperties(
                    (fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.TechnicalProperties)
                        InternalObj.getTechnicalProperties())
                : null;

        [DisplayName(@"BusinessAndLifeCycleProperties")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public PropertiesForRD_CTBusinessAndLifeCycleProperties BusinessAndLifeCycleProperties
            => InternalObj?.getBusinessAndLifeCycleProperties() != null
                ? new PropertiesForRD_CTBusinessAndLifeCycleProperties(
                    (fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.BusinessAndLifeCycleProperties)
                        InternalObj.getBusinessAndLifeCycleProperties())
                : null;


        [DisplayName(@"EnvironmentalProperties")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public PropertiesForRD_CTEnvironmentalProperties EnvironmentalProperties
            => InternalObj?.getEnvironmentalProperties() != null
                ? new PropertiesForRD_CTEnvironmentalProperties(
                    (fi.tut.mei.resdescapi.rdapi.model.rd.PropertiesForRDCT.EnvironmentalProperties)
                        InternalObj.getEnvironmentalProperties())
                : null;
    }
}