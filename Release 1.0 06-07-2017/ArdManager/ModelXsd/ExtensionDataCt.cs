using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ExtensionData_CT")]
    public class ExtensionDataCt
    {
        public ExtensionDataCt()
        {
            
        }
        public override string ToString()
        {
            return Id;
        }

        public ExtensionDataCt(fi.tut.mei.resdescapi.rdapi.model.rd.ExtensionDataCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ExtensionDataCT InternalObj { get; }

        [DisplayName(@"Any")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<string> Any
        {
            get
            {
                List<string> result = null;
                if (InternalObj?.getAny()?.toArray() != null)
                {
                    result = InternalObj.getAny().toArray().Cast<string>().ToList();
                }
                return result;
            }
        }

        [DisplayName(@"Id")]
        public string Id
        {
            get { return InternalObj?.getId(); }
            set { InternalObj?.setId(value); }
        }

        public static List<ExtensionDataCt> Get(object[] array)
        {
            var result = new List<ExtensionDataCt>();
            if (array == null)
                return result;
            try
            {
                result = array.OfType<fi.tut.mei.resdescapi.rdapi.model.rd.ExtensionDataCT>()
                    .Select(objType => new ExtensionDataCt(objType))
                    .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}