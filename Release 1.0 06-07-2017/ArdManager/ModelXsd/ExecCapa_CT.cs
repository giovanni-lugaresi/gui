using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ExecCapa_CT")]
    public class ExecCapa_CT
    {
        public ExecCapa_CT()
        {
            
        }
        public override string ToString()
        {
            return Name;
        }

        public ExecCapa_CT(fi.tut.mei.resdescapi.rdapi.model.rd.ExecCapaCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ExecCapaCT InternalObj { get; }

        [DisplayName(@"Labels")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<LocalizedString_CT> Labels => LocalizedString_CT.Get(InternalObj?.getLabel()?.toArray());

        [DisplayName(@"Comments")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<LocalizedString_CT> Comments => LocalizedString_CT.Get(InternalObj?.getComment()?.toArray());

        [DisplayName(@"Documentations")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<Documentation_CT> Documentations => Documentation_CT.Get(InternalObj?.getDocumentation()?.toArray());

        [DisplayName(@"Parent Exec Capas")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        [Editor(typeof (CollectionEditor), typeof (CollectionEditor))]
        public List<ExecCapaRef_CT> ParentExecCapas
        {
            get
            {
                List<ExecCapaRef_CT> result = null;
                if (InternalObj?.getParentExecCapas()?.getExecCapaRef() != null)
                {
                    result = new List<ExecCapaRef_CT>();

                    foreach (var obj in InternalObj.getParentExecCapas().getExecCapaRef().toArray())
                    {
                        var objType = obj;
                        if (objType is fi.tut.mei.resdescapi.rdapi.model.rd.ExecCapaRefCT)
                        {
                            result.Add(new ExecCapaRef_CT((fi.tut.mei.resdescapi.rdapi.model.rd.ExecCapaRefCT) objType));
                        }
                    }
                }
                return result;
            }
        }

        [DisplayName(@"Mappings")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public ExecCapa_CTMappings Mappings =>
            InternalObj?.getMappings() != null
                ? new ExecCapa_CTMappings(
                    (fi.tut.mei.resdescapi.rdapi.model.rd.ExecCapaCT.Mappings) InternalObj.getMappings())
                : null;

        [DisplayName(@"Id")]
        public string Id
        {
            get { return InternalObj?.getId(); }
            set { InternalObj?.setId(value); }
        }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }

        [DisplayName(@"Description")]
        public string Description
        {
            get { return InternalObj?.getDescription(); }
            set { InternalObj?.setDescription(value); }
        }

        [DisplayName(@"Gid")]
        public string Gid
        {
            get { return InternalObj?.getGid(); }
            set { InternalObj?.setGid(value); }
        }

        [DisplayName(@"Category")]
        public string Category => InternalObj?.getCategory().value();

        [DisplayName(@"ReqOpt")]
        public string ReqOpt => InternalObj?.getReqOpt()?.value();

        [DisplayName(@"ConnectionMethod")]
        public string ConnectionMethod => InternalObj?.getConnectionMethod()?.value();

        [DisplayName(@"EventPorts")]
        public InputOutputParamSetForEventPort_CT EventPorts =>
            InternalObj?.getEventPorts() != null
                ? new InputOutputParamSetForEventPort_CT(
                    (fi.tut.mei.resdescapi.rdapi.model.rd.InputOutputParamSetForEventPortCT) InternalObj.getEventPorts())
                : null;

        [DisplayName(@"ParameterPorts")]

        public InputOutputParamSet_CT ParameterPorts =>
            InternalObj?.getParameterPorts() != null
                ? new InputOutputParamSet_CT(
                    (fi.tut.mei.resdescapi.rdapi.model.rd.InputOutputParamSetCT) InternalObj.getParameterPorts())
                : null;
 

        [DisplayName(@"ConnectionMethods")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<ConnectionMethodCtrlAndCommRef> ConnectionMethods
        {
            get
            {
                List<ConnectionMethodCtrlAndCommRef> result = null;
                if (InternalObj?.getExtension() != null)
                {
                    result = new List<ConnectionMethodCtrlAndCommRef>();

                    foreach (var obj in InternalObj.getExtension().toArray())
                    {
                        var objType = obj;
                        if (objType is fi.tut.mei.resdescapi.rdapi.model.rd.ConnectionMethodCtrlAndCommRef)
                        {
                            result.Add(new ConnectionMethodCtrlAndCommRef((fi.tut.mei.resdescapi.rdapi.model.rd.ConnectionMethodCtrlAndCommRef)objType));
                        }
                    }
                }
                return result;
            }
        }

        [DisplayName(@"Implementation")]

        public Implementation_CT Implementation =>
            InternalObj?.getImplementation() != null
                ? new Implementation_CT(
                    (fi.tut.mei.resdescapi.rdapi.model.rd.ImplementationCT)InternalObj.getImplementation())
                : null;

        [DisplayName(@"Extension")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Editor(typeof(CollectionEditor), typeof(CollectionEditor))]
        public List<ExtensionDataCt> Extension
        {
            get
            {
                List<ExtensionDataCt> result = null;
                if (InternalObj?.getExtension() != null)
                {
                    result = new List<ExtensionDataCt>();

                    foreach (var obj in InternalObj.getExtension().toArray())
                    {
                        var objType = obj;
                        if (objType is fi.tut.mei.resdescapi.rdapi.model.rd.ExtensionDataCT)
                        {
                            result.Add(new ExtensionDataCt((fi.tut.mei.resdescapi.rdapi.model.rd.ExtensionDataCT)objType));
                        }
                    }
                }
                return result;
            }
        }
    }
}