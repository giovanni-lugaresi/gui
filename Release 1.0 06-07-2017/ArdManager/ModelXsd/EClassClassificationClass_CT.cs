using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"EClassClassificationClass_CT")]

    public class EClassClassificationClass_CT
    {
        public EClassClassificationClass_CT()
        {
            
        }
        public override string ToString()
        {
            return Name;
        }

        public EClassClassificationClass_CT(
            fi.tut.mei.resdescapi.rdapi.model.rd.EClassClassificationClassCT resourceDescription)
        {
            InternalObj = resourceDescription;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.EClassClassificationClassCT InternalObj { get; }

        [DisplayName(@"CodedName")]
        public string CodedName
        {
            get { return InternalObj?.getCodedName(); }
            set { InternalObj?.setCodedName(value); }
        }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }


        [DisplayName(@"eClassIRDI")]
        public string EClassIrdi
        {
            get { return InternalObj?.getEClassIRDI(); }
            set { InternalObj?.setEClassIRDI(value); }
        }

        [DisplayName(@"Definition")]
        public string Definition
        {
            get { return InternalObj?.getDefinition(); }
            set { InternalObj?.setDefinition(value); }
        }

        public static List<EClassClassificationClass_CT> Get(object[] array)
        {
            var result = new List<EClassClassificationClass_CT>();
            if (array == null)
                return result;
            try
            {
                result = array.OfType<fi.tut.mei.resdescapi.rdapi.model.rd.EClassClassificationClassCT>()
                    .Select(objType => new EClassClassificationClass_CT(objType))
                    .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}