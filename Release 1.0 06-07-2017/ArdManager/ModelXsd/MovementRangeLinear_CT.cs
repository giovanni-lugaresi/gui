using System.ComponentModel;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"MovementRangeLinear_CT")]

    public class MovementRangeLinear_CT
    {
        public MovementRangeLinear_CT()
        {
            
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public MovementRangeLinear_CT(
            fi.tut.mei.resdescapi.rdapi.model.rd.MovementRangeLinearCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.MovementRangeLinearCT InternalObj { get; }


        [DisplayName(@"Translation_x")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ValueSimple_CT TranslationX
            =>
                InternalObj?.getTranslationX() != null
                    ? new ValueSimple_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.ValueSimpleCT)InternalObj.getTranslationX())
                    : null;

        [DisplayName(@"Translation_y")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ValueSimple_CT TranslationY
            =>
                InternalObj?.getTranslationY() != null
                    ? new ValueSimple_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.ValueSimpleCT)InternalObj.getTranslationY())
                    : null;

        [DisplayName(@"Translation_z")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ValueSimple_CT TranslationZ
            =>
                InternalObj?.getTranslationZ() != null
                    ? new ValueSimple_CT(
                        (fi.tut.mei.resdescapi.rdapi.model.rd.ValueSimpleCT)InternalObj.getTranslationZ())
                    : null;
    }
}