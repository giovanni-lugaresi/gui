using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ArdManager.ModelXsd
{
    [DisplayName(@"ArrayType_CT")]
    public class ArrayType_CT
    {
        public ArrayType_CT()
        {
            
        }
        public override string ToString()
        {
            return Name;
        }

        public ArrayType_CT(fi.tut.mei.resdescapi.rdapi.model.rd.ArrayTypeCT obj)
        {
            InternalObj = obj;
        }

        [Browsable(false)]
        public fi.tut.mei.resdescapi.rdapi.model.rd.ArrayTypeCT InternalObj { get; }

        [DisplayName(@"Id")]
        public string Id
        {
            get { return InternalObj?.getId(); }
            set { InternalObj?.setId(value); }
        }

        [DisplayName(@"BaseType")]
        public string BaseType
        {
            get { return InternalObj?.getBaseType(); }
            set { InternalObj?.setBaseType(value); }
        }

        [DisplayName(@"NameSpace")]
        public string NameSpace
        {
            get { return InternalObj?.getNameSpace(); }
            set { InternalObj?.setNameSpace(value); }
        }

        [DisplayName(@"Name")]
        public string Name
        {
            get { return InternalObj?.getName(); }
            set { InternalObj?.setName(value); }
        }

        [DisplayName(@"LBound")]
        public string LBound => InternalObj?.getLBound().ToString();

        [DisplayName(@"Size")]
        public string Size => InternalObj?.getSize().ToString();
        public static List<ArrayType_CT> Get(object[] array)
        {
            var result = new List<ArrayType_CT>();
            if (array == null)
                return result;
            try
            {
                result =
                    array
                        .OfType
                        <
                            fi.tut.mei.resdescapi.rdapi.model.rd.ArrayTypeCT>()
                        .Select(objType => new ArrayType_CT(objType))
                        .ToList();
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}