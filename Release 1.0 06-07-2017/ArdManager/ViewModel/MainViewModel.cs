using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using ArdManager.Model;
using System.Windows.Media;
using ArdManager.ModelXsd;
using ArdManager.Properties;
using GalaSoft.MvvmLight.Messaging;

namespace ArdManager.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            ////if (IsInDesignMode)
            ////{
            ////    // Code runs in Blend --> create design time data.
            ////}
            ////else
            ////{
            ////    // Code runs "for real"
            ////}

            SearchAdvancedCommand = new RelayCommand(OnSearchAdvancedCommand, () => true);

            SearchRdIdCommand = new RelayCommand(OnSearchRdIdCommand, () => true);
            LoadEmptyCommand = new RelayCommand(OnLoadEmptyCommand, OnCanExecuteLoadEmptyCommand);

            GetCapabilitiesCommand = new RelayCommand(OnGetCapabilitiesCommand, () => true);
            SaveCommand = new RelayCommand(OnSaveCommand, OnCanExecuteSaveCommand);
            ClearCommand = new RelayCommand(OnClearCommand, OnCanExecuteClearCommand);
            InitCommand = new RelayCommand(OnInitCommand, () => true);

            NewArdCommand = new RelayCommand<WpfProfile>(OnNewArdCommand, p => true);
            NewRdCommand = new RelayCommand<string>(OnNewRdCommand, p => true);

            GetSkeletonCommand = new RelayCommand(OnGetSkeletonCommand, () => true);

            ClearCapabilityFilterCommnad = new RelayCommand(OnClearCapabilityFilterCommnad, () => true);
            ClearCategoryFilterCommnad = new RelayCommand(OnClearCategoryFilterCommnad, () => true);
            ClearDescriptionFilterCommnad = new RelayCommand(OnClearDescriptionFilterCommnad, () => true);
            ClearInterfaceFilterCommnad = new RelayCommand(OnClearInterfaceFilterCommnad, () => true);
            ClearAdvancedFilterCommnad = new RelayCommand(OnClearAdvancedFilterCommnad, () => true);
            LoadConfigFileCommand = new RelayCommand(OnLoadConfigFileCommand, () => true);
            SearchParameterCommand = new RelayCommand<WpfCapability>(OnSearchParameterCommand,
                OnCanExecuteSearchParameterCommand);
            ClearParameterCommand = new RelayCommand<WpfCapability>(OnClearParameterCommand,
                OnCanExecuteClearParameterCommand);



            ArdWrapper = new ArdWrapper();
            ArdWrapper.MessageGenerated += ArdWrapper_MessageGenerated;

            //if (string.IsNullOrEmpty(Settings.Default.ConfigFileName))
            //{
            //    Settings.Default.ConfigFileName = @"D:\Projects\Politecnico\resources\resourceDescription.properties";
            //    Settings.Default.Save();
            //}

            FileSystems =
                new ObservableCollection<string>(ArdWrapper.GetLocalFileSystems(Settings.Default.ConfigFileName));

            Capabilities = new ObservableCollection<WpfCapability>();
            UpdateText();

            LoadTestRd = new RelayCommand(OnLoadTestRd, () => true);
            SearchOrCapability = true;
            SearchOrAdvanced = true;

            Messenger.Default.Register<ParamWindowCloseMessage>(this, ParamWindowCloseMessageReceived);
        }

        private void ParamWindowCloseMessageReceived(ParamWindowCloseMessage msg)
        {
            msg?.Capability?.RaisePropertyChanged("ValuedParameter");

            OnSearchArdParameters(msg?.Capability);
            OnSearchRdParameters(msg?.Capability);
        }

        private void ArdWrapper_MessageGenerated(object sender, ArdWrapper.MessageEventArgs e)
        {
            if (e.Ex != null)
            {
                ShowMessage = $"Exception({e.Source}): {e.Ex.Message}";
                ShowMessageColor = Brushes.Red;
            }
            else
            {
                ShowMessage = $"({e.Source}): {e.Message}";
                ShowMessageColor = Brushes.Black;
                BusyContent = string.IsNullOrEmpty(ShowMessage)
                    ? "Please Wait..."
                    : $"Please Wait... [{ShowMessage}]";
            }
        }

        #region Properties

        #region WindowProperties

        public string Text => $"Resource Description Editor ({Assembly.GetExecutingAssembly().GetName().Version})";
        public string TextTab0 => $"Add new Resource Description";
        public string TextTab1 => $"View existing Resource Description";

        private bool _isBusy;

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                if (value != _isBusy)
                {
                    _isBusy = value;
                    RaisePropertyChanged();
                }
            }
        }


        private bool _isDirty;

        public bool IsDirty
        {
            get { return _isDirty; }
            set
            {
                if (value != _isDirty)
                {
                    _isDirty = value;
                    RaisePropertyChanged();
                    RaisePropertyChanged("PropertyGridColor");
                }
            }
        }


        private bool _allCapability;

        public bool AllCapability
        {
            get { return _allCapability; }
            set
            {
                if (value != _allCapability)
                {
                    _allCapability = value;
                    foreach (var capability in Capabilities)
                    {
                        capability.IsChecked = value;
                    }
                    RaisePropertyChanged();
                    RaisePropertyChanged("SelectedCapability");

                }
            }
        }

        private bool _hideEmpty;

        public bool HideEmpty
        {
            get { return _hideEmpty; }
            set
            {
                if (value != _hideEmpty)
                {
                    _hideEmpty = value;
                    OnSearchAdvancedCommand();
                    RaisePropertyChanged();
                    RaisePropertyChanged("SelectedCapability");
                }
            }
        }

        public SolidColorBrush PropertyGridColor => IsDirty ? Brushes.Red : Brushes.Black;

        private string _busyContent;

        public string BusyContent
        {
            get { return _busyContent; }
            set
            {
                if (value != _busyContent)
                {
                    _busyContent = value;
                    RaisePropertyChanged();
                }
            }
        }


        private bool _isDropDownOpenCapability;

        public bool IsDropDownOpenCapability
        {
            get { return _isDropDownOpenCapability; }
            set
            {
                if (value != _isDropDownOpenCapability)
                {
                    _isDropDownOpenCapability = value;
                    RaisePropertyChanged();
                    UpdateText();
                }
            }
        }

        private string _showMessage;

        public string ShowMessage
        {
            get { return _showMessage; }
            set
            {
                if (value != _showMessage)
                {
                    _showMessage = value;
                    RaisePropertyChanged();
                }
            }
        }

        private SolidColorBrush _showMessageColor = Brushes.Black;

        public SolidColorBrush ShowMessageColor
        {
            get { return _showMessageColor; }
            set
            {
                if (value != _showMessageColor)
                {
                    _showMessageColor = value;
                    RaisePropertyChanged();
                }
            }
        }

        public Visibility VisSearchCapability
            => SelectedCapability == null || !SelectedCapability.Any()
                ? Visibility.Collapsed
                : Visibility.Visible;

        public Visibility VisSearchDescription
            => string.IsNullOrEmpty(SelectedDescription)
                ? Visibility.Collapsed
                : Visibility.Visible;

        public Visibility VisSearchCategory
            => string.IsNullOrEmpty(SelectedCategory)
                ? Visibility.Collapsed
                : Visibility.Visible;

        public Visibility VisSearchInterface
            => string.IsNullOrEmpty(SelectedInterface)
                ? Visibility.Collapsed
                : Visibility.Visible;

        public Visibility VisSearchAdvanced
            =>
                string.IsNullOrEmpty(SelectedInterface) &&
                string.IsNullOrEmpty(SelectedCategory) &&
                string.IsNullOrEmpty(SelectedDescription)
                    ? Visibility.Collapsed
                    : Visibility.Visible;

        public Visibility ObjVisibility
        {
            get
            {
                if (TabSelectedIndex == 0)
                    return _propertyGridObjectTab0 == null ? Visibility.Collapsed : Visibility.Visible;
                if (TabSelectedIndex == 1)
                    return _propertyGridObjectTab1 == null ? Visibility.Collapsed : Visibility.Visible;
                return Visibility.Collapsed;
            }
        }



        public MainWindowMessage MainWindowMessage => new MainWindowMessage
        {
            Message = "All filters will be cleared",
            Caption = "Confirm Message",
            MessageIcon = (int) MessageBoxImage.Asterisk
        };


        private int _tabSelectedIndex;

        public int TabSelectedIndex
        {
            get { return _tabSelectedIndex; }
            set
            {
                if (value != _tabSelectedIndex)
                {
                    _tabSelectedIndex = value;
                    RaisePropertyChanged();


                    if (value == 0)
                    {
                        if (_propertyGridObjectTab1 != null || SelectedCapability.Any())
                            MessengerInstance.Send(MainWindowMessage);

                        OnClearAdvancedFilterCommnad();
                        OnClearCommand();
                        OnSearchAdvancedCommand();
                    }
                    if (value == 1)
                    {
                        if (_propertyGridObjectTab0 != null || SelectedCapability.Any())
                            MessengerInstance.Send(MainWindowMessage);

                        OnClearAdvancedFilterCommnad();
                        OnClearCommand();
                        OnSearchAdvancedCommand();
                    }
                }
                RaisePropertyChanged("ObjVisibility");
            }
        }

        private string _captionButtonInit = "Load Repository";

        public string CaptionButtonInit
        {
            get { return _captionButtonInit; }
            set
            {
                if (value != _captionButtonInit)
                {
                    _captionButtonInit = value;
                    RaisePropertyChanged();
                }
            }
        }


        private bool _isInit;

        public bool IsInit
        {
            get { return _isInit; }
            set
            {
                if (value != _isInit)
                {
                    _isInit = value;
                    RaisePropertyChanged();
                    CaptionButtonInit = _isInit ? "Refresh Repository" : "Load Repository";
                }
            }
        }

        #endregion

        #region Search

        private bool _searchOrCapability;

        public bool SearchOrCapability
        {
            get { return _searchOrCapability; }
            set
            {
                if (value != _searchOrCapability)
                {
                    _searchOrCapability = value;
                    RaisePropertyChanged();
                    RaisePropertyChanged("SelectedCapability");
                }
            }
        }

        private bool _searchAndCapability;

        public bool SearchAndCapability
        {
            get { return _searchAndCapability; }
            set
            {
                if (value != _searchAndCapability)
                {
                    _searchAndCapability = value;
                    RaisePropertyChanged();
                    RaisePropertyChanged("SelectedCapability");
                }
            }
        }


        private bool _searchOrAdvanced;

        public bool SearchOrAdvanced
        {
            get { return _searchOrAdvanced; }
            set
            {
                if (value != _searchOrAdvanced)
                {
                    _searchOrAdvanced = value;
                    RaisePropertyChanged();
                    OnSearchAdvancedCommand();
                }
            }
        }

        private bool _searchAndAdvanced;

        public bool SearchAndAdvanced
        {
            get { return _searchAndAdvanced; }
            set
            {
                if (value != _searchAndAdvanced)
                {
                    _searchAndAdvanced = value;
                    RaisePropertyChanged();
                    OnSearchAdvancedCommand();
                }
            }
        }

        #endregion

        #region Wrapper

        private ArdWrapper ArdWrapper { get; set; }
        public Array SearchType => ArdWrapper.SearchType;

        public string RepositoryCount => "Under Costruction";

        public string ConfigFileName
        {
            get { return Settings.Default.ConfigFileName; }
            set
            {
                if (value != Settings.Default.ConfigFileName)
                {
                    Settings.Default.ConfigFileName = value;
                    Settings.Default.Save();
                    RaisePropertyChanged();
                }
                FileSystems =
                    new ObservableCollection<string>(ArdWrapper.GetLocalFileSystems(Settings.Default.ConfigFileName));
            }
        }

        public string LocalFileSystem
        {
            get { return Settings.Default.LocalFileSystem; }
            set
            {
                if (value != Settings.Default.LocalFileSystem)
                {
                    Settings.Default.LocalFileSystem = value;
                    Settings.Default.Save();
                    RaisePropertyChanged();
                }
            }
        }

        private string _ardFolder;

        public string ArdFolder
        {
            get { return _ardFolder; }
            set
            {
                if (value != _ardFolder)
                {
                    _ardFolder = value;
                    RaisePropertyChanged();
                }
            }
        }

        private string _rdFolder;

        public string RdFolder
        {
            get { return _rdFolder; }
            set
            {
                if (value != _rdFolder)
                {
                    _rdFolder = value;
                    RaisePropertyChanged();
                }
            }
        }

        private string _fileName;

        public string FileName
        {
            get { return _fileName; }
            set
            {
                if (value != _fileName)
                {
                    _fileName = value;
                    RaisePropertyChanged();
                }
            }
        }

        #endregion

        #region ObservableCollection

        private ObservableCollection<string> _fileSystems;

        public ObservableCollection<string> FileSystems
        {
            get { return _fileSystems; }
            set
            {
                if (value != _fileSystems)
                {
                    _fileSystems = value;
                    RaisePropertyChanged();
                }
            }
        }

        //private ObservableCollection<string> _profiles;

        //public ObservableCollection<string> Profiles
        //{
        //    get { return _profiles; }
        //    set
        //    {
        //        if (value != _profiles)
        //        {
        //            _profiles = value;
        //            RaisePropertyChanged();
        //        }
        //    }
        //}

        //private ObservableCollection<string> _ardIds;

        //public ObservableCollection<string> ArdIds
        //{
        //    get { return _ardIds; }
        //    set
        //    {
        //        if (value != _ardIds)
        //        {
        //            _ardIds = value;
        //            RaisePropertyChanged();
        //        }
        //    }
        //}


        //private ObservableCollection<WpfSearchKey> _wpfSearchKeys;

        //public ObservableCollection<WpfSearchKey> WpfSearchKeys
        //{
        //    get { return _wpfSearchKeys; }
        //    set
        //    {
        //        if (value != _wpfSearchKeys)
        //        {
        //            _wpfSearchKeys = value;
        //            RaisePropertyChanged();
        //        }
        //    }
        //}

        private ObservableCollection<WpfCapability> _capabilites;

        public ObservableCollection<WpfCapability> Capabilities
        {
            get { return _capabilites; }
            set
            {
                if (value != _capabilites)
                {
                    _capabilites = value;
                    RaisePropertyChanged();
                    RaisePropertyChanged("SelectedCapability");
                }
            }
        }

        private Object lockThis = new System.Object();

        public ObservableCollection<WpfCapability> SelectedCapability
        {
            get
            {
                var result = new ObservableCollection<WpfCapability>();
                if (Capabilities != null)
                {
                    var firstFullCapArd = Capabilities.Where(x => x.IsChecked)
                        .FirstOrDefault(x => x.SearchKeysArd.Any());
                    var firstFullCapRd = Capabilities.Where(x => x.IsChecked).FirstOrDefault(x => x.SearchKeysRd.Any());

                    var intersectResultArd = firstFullCapArd?.SearchKeysArd.Select(x => x.ArdId).ToList() ??
                                             new List<string>();
                    var intersectResultRd = firstFullCapRd?.SearchKeysRd.Select(x => x.RdId).ToList() ??
                                            new List<string>();


                    foreach (var cap in Capabilities)
                    {
                        cap.SearchKeysArdFromAndSearch = null;
                        cap.SearchKeysRdFromAndSearch = null;
                        if (cap.IsChecked)
                        {
                            intersectResultArd =
                                intersectResultArd.Intersect(cap.SearchKeysArd.Select(x => x.ArdId)).ToList();
                            intersectResultRd =
                                intersectResultRd.Intersect(cap.SearchKeysRd.Select(x => x.RdId)).ToList();
                        }
                    }

                    if (SearchAndCapability)
                    {
                        foreach (var cap in Capabilities.Where(x => x.IsChecked))
                        {
                            cap.SearchKeysArdFromAndSearch = intersectResultArd;
                            cap.SearchKeysRdFromAndSearch = intersectResultRd;
                        }
                    }
                    if (HideEmpty)
                    {
                        if (TabSelectedIndex == 0)
                        {
                            result =
                                new ObservableCollection<WpfCapability>(
                                    Capabilities.Where(x => x.IsChecked && x.SearchKeysArd.Any()));
                        }
                        if (TabSelectedIndex == 1)
                        {
                            result =
                                new ObservableCollection<WpfCapability>(
                                    Capabilities.Where(x => x.IsChecked && x.SearchKeysRd.Any()));
                        }
                    }
                    else
                        result = new ObservableCollection<WpfCapability>(Capabilities.Where(x => x.IsChecked));
                }

                return result;
            }
        }

        private ObservableCollection<WpfSearchKeyArd> _selectedSearchKeys;

        public ObservableCollection<WpfSearchKeyArd> SelectedSearchKeys
        {
            get { return _selectedSearchKeys; }
            set
            {
                if (value != _selectedSearchKeys)
                {
                    _selectedSearchKeys = value;
                    RaisePropertyChanged();
                }
                RaisePropertyChanged("VisSearchCapability");
            }
        }

        #endregion

        #region Selected

        private string _selectedRdId;

        public string SelectedRdId
        {
            get { return _selectedRdId; }
            set
            {
                if (value != _selectedRdId)
                {
                    _selectedRdId = value;
                    RaisePropertyChanged();
                }
            }
        }

        private string _textSelectedCapability;

        public string TextSelectedCapability
        {
            get { return _textSelectedCapability; }
            set
            {
                if (value != _textSelectedCapability)
                {
                    _textSelectedCapability = value;
                    RaisePropertyChanged();
                }
            }
        }

        private string _tooltipSelectedCapability;

        public string TooltipSelectedCapability
        {
            get { return _tooltipSelectedCapability; }
            set
            {
                if (value != _tooltipSelectedCapability)
                {
                    _tooltipSelectedCapability = value;
                    RaisePropertyChanged();
                }
            }
        }

        private string _selectedCategory;

        public string SelectedCategory
        {
            get { return _selectedCategory; }
            set
            {
                if (value != _selectedCategory)
                {
                    _selectedCategory = value;
                    RaisePropertyChanged();
                    RaisePropertyChanged("VisSearchCategory");
                    RaisePropertyChanged("VisSearchAdvanced");
                }
            }
        }

        private string _selectedDescription;

        public string SelectedDescription
        {
            get { return _selectedDescription; }
            set
            {
                if (value != _selectedDescription)
                {
                    _selectedDescription = value;
                    RaisePropertyChanged();
                    RaisePropertyChanged("VisSearchDescription");
                    RaisePropertyChanged("VisSearchAdvanced");
                }
            }
        }

        private string _selectedInterface;

        public string SelectedInterface
        {
            get { return _selectedInterface; }
            set
            {
                if (value != _selectedInterface)
                {
                    _selectedInterface = value;
                    RaisePropertyChanged();
                    RaisePropertyChanged("VisSearchInterface");
                    RaisePropertyChanged("VisSearchAdvanced");
                }
            }
        }

        private ResourceDescription _propertyGridObject;

        public ResourceDescription PropertyGridObject
        {
            get { return _propertyGridObject; }
            set
            {
                if (value != _propertyGridObject)
                {
                    _propertyGridObject = value;
                    RaisePropertyChanged();
                    IsDirty = false;
                }
            }
        }

        private ResourceDescription _propertyGridObjectTab0;

        private ResourceDescription _propertyGridObjectTab1;


        //private WpfResourceDescription _propertyGridObjectOld;

        //public WpfResourceDescription PropertyGridObjectOld
        //{
        //    get { return _propertyGridObjectOld; }
        //    set
        //    {
        //        if (value != _propertyGridObjectOld)
        //        {
        //            _propertyGridObjectOld = value;
        //            RaisePropertyChanged();
        //        }
        //    }
        //}

        //private WpfResourceDescription _propertyGridObjectTab0Old;

        //private WpfResourceDescription _propertyGridObjectTab1Old;

        #endregion

        #region SearchType

        private ArdWrapper.EnumWpfSearchType _wpfSearchTypeCategory;

        public ArdWrapper.EnumWpfSearchType WpfSearchTypeCategory
        {
            get { return _wpfSearchTypeCategory; }
            set
            {
                if (value != _wpfSearchTypeCategory)
                {
                    _wpfSearchTypeCategory = value;
                    RaisePropertyChanged();
                }
            }
        }

        private ArdWrapper.EnumWpfSearchType _wpfSearchTypeDescription;

        public ArdWrapper.EnumWpfSearchType WpfSearchTypeDescription
        {
            get { return _wpfSearchTypeDescription; }
            set
            {
                if (value != _wpfSearchTypeDescription)
                {
                    _wpfSearchTypeDescription = value;
                    RaisePropertyChanged();
                }
            }
        }

        private ArdWrapper.EnumWpfSearchType _wpfSearchTypeInterface;

        public ArdWrapper.EnumWpfSearchType WpfSearchTypeInterface
        {
            get { return _wpfSearchTypeInterface; }
            set
            {
                if (value != _wpfSearchTypeInterface)
                {
                    _wpfSearchTypeInterface = value;
                    RaisePropertyChanged();
                }
            }
        }

        #endregion

        #region Expanders

        private bool _isExpandedCapability;

        public bool IsExpandedCapability
        {
            get { return _isExpandedCapability; }
            set
            {
                if (value != _isExpandedCapability)
                {
                    _isExpandedCapability = value;
                    RaisePropertyChanged();

                    if (value)
                    {
                        if (Capabilities == null || Capabilities.Count == 0)
                        {
                            OnGetCapabilitiesCommand();
                        }
                    }
                }
            }
        }

        private bool _isExpandedAdvanced;

        public bool IsExpandedAdvanced
        {
            get { return _isExpandedAdvanced; }
            set
            {
                if (value != _isExpandedAdvanced)
                {
                    _isExpandedAdvanced = value;
                    RaisePropertyChanged();
                }
            }
        }

        private bool _isExpandedCategory;

        public bool IsExpandedCategory
        {
            get { return _isExpandedCategory; }
            set
            {
                if (value != _isExpandedCategory)
                {
                    _isExpandedCategory = value;
                    RaisePropertyChanged();
                }
            }
        }

        private bool _isExpandedDescription;

        public bool IsExpandedDescription
        {
            get { return _isExpandedDescription; }
            set
            {
                if (value != _isExpandedDescription)
                {
                    _isExpandedDescription = value;
                    RaisePropertyChanged();
                }
            }
        }

        private bool _isExpandedInterface;

        public bool IsExpandedInterface
        {
            get { return _isExpandedInterface; }
            set
            {
                if (value != _isExpandedInterface)
                {
                    _isExpandedInterface = value;
                    RaisePropertyChanged();
                }
            }
        }


        #endregion

        #endregion

        #region Commands

        public RelayCommand SearchAdvancedCommand { get; set; }
        public RelayCommand SearchRdIdCommand { get; set; }
        public RelayCommand SetLocationCommand { get; set; }
        public RelayCommand GetCapabilitiesCommand { get; set; }
        public RelayCommand SaveCommand { get; set; }
        public RelayCommand ClearCommand { get; set; }
        public RelayCommand InitCommand { get; set; }
        public RelayCommand<WpfProfile> NewArdCommand { get; set; }
        public RelayCommand<string> NewRdCommand { get; set; }
        public RelayCommand GetSkeletonCommand { get; set; }
        public RelayCommand ClearCapabilityFilterCommnad { get; set; }
        public RelayCommand ClearCategoryFilterCommnad { get; set; }
        public RelayCommand ClearDescriptionFilterCommnad { get; set; }
        public RelayCommand ClearInterfaceFilterCommnad { get; set; }
        public RelayCommand ClearAdvancedFilterCommnad { get; set; }
        public RelayCommand LoadConfigFileCommand { get; set; }
        public RelayCommand LoadEmptyCommand { get; set; }
        public RelayCommand<WpfCapability> SearchParameterCommand { get; set; }
        public RelayCommand<WpfCapability> ClearParameterCommand { get; set; }

        private void OnSearchParameterCommand(WpfCapability cap)
        {
            var mess = new ParamWindowMessage {Capability = cap};
            MessengerInstance.Send(mess);
        }

        private void OnClearParameterCommand(WpfCapability cap)
        {
            cap?.ClearParameter();
            OnSearchAdvancedCommand();
        }

        private bool OnCanExecuteSearchParameterCommand(WpfCapability cap)
        {
            return cap.Parameters != null && cap.Parameters.Any();
        }

        private bool OnCanExecuteClearParameterCommand(WpfCapability cap)
        {
            return cap.Parameters != null && cap.Parameters.Any();
        }

        private void OnLoadConfigFileCommand()
        {
            var dlg = new Microsoft.Win32.OpenFileDialog
            {
                DefaultExt = ".properties",
                Filter = "Property Files|*.properties|All Files|*.*"
            };

            if (dlg.ShowDialog() == true)
            {
                ConfigFileName = dlg.FileName;
            }
        }

        private void OnSearchAdvancedCommand()
        {
            if (!_isInit) return;
            var worker = new BackgroundWorker();
            worker.DoWork += (o, ea) =>
            {
                if (TabSelectedIndex == 0)
                {
                    foreach (var wpfCapability in Capabilities)
                    {
                        ArdWrapper.GetSearchKeysArd(wpfCapability);
                    }
                }
                if (TabSelectedIndex == 1)
                {
                    foreach (var wpfCapability in Capabilities)
                    {
                        ArdWrapper.GetSearchKeysRd(wpfCapability);
                    }
                }

                ArdWrapper.EnumSearchType search = ArdWrapper.EnumSearchType.And;
                //if (SearchAndAdvanced) search = ArdWrapper.EnumSearchType.And;
                if (SearchOrAdvanced) search = ArdWrapper.EnumSearchType.Or;

                if (TabSelectedIndex == 0)
                {
                    var resultSearch = ArdWrapper.SearchArdByAdvanced(SelectedDescription,
                        SelectedCategory,
                        SelectedInterface, search);

                    foreach (var wpfCapability in Capabilities)
                    {
                        wpfCapability.SearchResultArd = resultSearch == null
                            ? null
                            : new ObservableCollection<string>(resultSearch);
                    }
                }
                if (TabSelectedIndex == 1)
                {
                    var resultSearch = ArdWrapper.SearchRdByAdvanced(SelectedDescription,
                        SelectedCategory,
                        SelectedInterface, search);

                    foreach (var wpfCapability in Capabilities)
                    {
                        wpfCapability.SearchResultRd = resultSearch == null
                            ? null
                            : new ObservableCollection<string>(resultSearch);
                    }
                }
            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                IsBusy = false;
                RaisePropertyChanged("SelectedCapability");
                RaisePropertyChanged("VisSearchCapability");
            };

            IsBusy = true;
            worker.RunWorkerAsync();
        }

        private void OnSearchRdIdCommand()
        {
            var worker = new BackgroundWorker();
            worker.DoWork += (o, ea) =>
            {
                _propertyGridObjectTab1 = string.IsNullOrEmpty(SelectedRdId)
                    ? null
                    : ArdWrapper.GetRdStructure(SelectedRdId);
            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                IsBusy = false;
                if (TabSelectedIndex == 0)
                {
                    PropertyGridObject = _propertyGridObjectTab0;
                }
                if (TabSelectedIndex == 1)
                {
                    PropertyGridObject = _propertyGridObjectTab1;
                }
                RaisePropertyChanged("ObjVisibility");
            };

            IsBusy = true;
            worker.RunWorkerAsync();
        }

        private void OnLoadEmptyCommand()
        {
            var worker = new BackgroundWorker();
            worker.DoWork += (o, ea) =>
            {
                var empty = ArdWrapper.GetEmpty;
                _propertyGridObjectTab0 = empty;
                _propertyGridObjectTab1 = empty;
            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                IsBusy = false;
                if (TabSelectedIndex == 0)
                {
                    PropertyGridObject = _propertyGridObjectTab0;
                }
                if (TabSelectedIndex == 1)
                {
                    PropertyGridObject = _propertyGridObjectTab1;
                }
                RaisePropertyChanged("ObjVisibility");
            };

            IsBusy = true;
            worker.RunWorkerAsync();
        }

        private bool OnCanExecuteLoadEmptyCommand()
        {
            return IsInit;
        }

        private bool OnCanExecuteSaveCommand()
        {
            return IsInit;
        }

        private bool OnCanExecuteClearCommand()
        {
            return IsInit;
        }

        private void OnGetCapabilitiesCommand()
        {
            var worker = new BackgroundWorker();
            worker.DoWork += (o, ea) =>
            {
                _capabilites = new ObservableCollection<WpfCapability>(ArdWrapper.GetCapabilities());
            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                IsBusy = false;
                RaisePropertyChanged("Capabilities");

                foreach (var wpfCapability in _capabilites)
                {
                    wpfCapability.PropertyChanged += WpfCapability_PropertyChanged;
                }
                TextSelectedCapability = string.Empty;
            };
            IsBusy = true;
            worker.RunWorkerAsync();
        }

        private void OnSaveCommand()
        {
            if (!IsInit) return;

            var worker = new BackgroundWorker();
            worker.DoWork += (o, ea) =>
            {
                ArdWrapper.Save(ArdWrapper.EnumWpfSaveMode.RdContent, PropertyGridObject);
                _isDirty = false;
            };
            worker.RunWorkerCompleted += (o, ea) =>
            {

                IsBusy = false;
                RaisePropertyChanged("IsDirty");
                RaisePropertyChanged("PropertyGridColor");
            };
            IsBusy = true;
            worker.RunWorkerAsync();
        }

        private void OnClearCommand()
        {
            ShowMessage = string.Empty;

            PropertyGridObject = null;
            _propertyGridObjectTab0 = null;
            _propertyGridObjectTab1 = null;

            SelectedInterface = null;
            SelectedCategory = null;
            SelectedDescription = null;
            //SelectedProfile = null;

            //WpfSearchKeys = new ObservableCollection<WpfSearchKey>();
            Capabilities = new ObservableCollection<WpfCapability>();
            AllCapability = false;
            HideEmpty = false;

            IsExpandedCapability = false;
            IsExpandedAdvanced = false;
            IsExpandedCategory = false;
            IsExpandedDescription = false;
            IsExpandedInterface = false;

            UpdateText();

            RaisePropertyChanged("SelectedCapability");
            RaisePropertyChanged("VisSearchCapability");
            RaisePropertyChanged("ObjVisibility");
        }

        private void OnInitCommand()
        {
            OnClearCommand();

            var worker = new BackgroundWorker();
            worker.DoWork += (o, ea) =>
            {
                ArdWrapper.Init(ConfigFileName, LocalFileSystem);
                _isInit = true;
            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                IsBusy = false;
                RaisePropertyChanged("IsInit");
                SaveCommand.RaiseCanExecuteChanged();
                ClearCommand.RaiseCanExecuteChanged();
                LoadEmptyCommand.RaiseCanExecuteChanged();
            };
            IsBusy = true;
            worker.RunWorkerAsync();
        }

        private void OnNewArdCommand(WpfProfile selection)
        {
            var worker = new BackgroundWorker();
            worker.DoWork += (o, ea) =>
            {
                var obj = ArdWrapper.GetSkeleton(selection);

                if (TabSelectedIndex == 0)
                {
                    _propertyGridObjectTab0 = obj;
                }
                if (TabSelectedIndex == 1)
                {
                    _propertyGridObjectTab1 = obj;
                }

            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                IsBusy = false;
                if (TabSelectedIndex == 0)
                {
                    PropertyGridObject = _propertyGridObjectTab0;
                }
                if (TabSelectedIndex == 1)
                {
                    PropertyGridObject = _propertyGridObjectTab1;
                }

                RaisePropertyChanged("ObjVisibility");
            };
            IsBusy = true;
            worker.RunWorkerAsync();
        }

        private void OnNewRdCommand(string selection)
        {
            var worker = new BackgroundWorker();
            worker.DoWork += (o, ea) =>
            {
                var obj = ArdWrapper.GetRdStructure(selection);

                if (TabSelectedIndex == 0)
                {
                    _propertyGridObjectTab0 = obj;
                }
                if (TabSelectedIndex == 1)
                {
                    _propertyGridObjectTab1 = obj;
                }

            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                IsBusy = false;
                if (TabSelectedIndex == 0)
                {
                    PropertyGridObject = _propertyGridObjectTab0;
                }
                if (TabSelectedIndex == 1)
                {
                    PropertyGridObject = _propertyGridObjectTab1;
                }

                RaisePropertyChanged("ObjVisibility");
            };
            IsBusy = true;
            worker.RunWorkerAsync();
        }

        private void OnGetSkeletonCommand()
        {
            var worker = new BackgroundWorker();
            worker.DoWork += (o, ea) =>
            {
                //var obj = ArdWrapper.GetSkeleton(SelectedSearchKey.ArdId, selection.ProfileId);
                //if (TabSelectedIndex == 0)
                //{
                //    _propertyGridObjectTab0 = obj;
                //}
                //if (TabSelectedIndex == 1)
                //{
                //    _propertyGridObjectTab1 = obj;
                //}
            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                IsBusy = false;
                if (TabSelectedIndex == 0)
                {
                    PropertyGridObject = _propertyGridObjectTab0;
                }
                if (TabSelectedIndex == 1)
                {
                    PropertyGridObject = _propertyGridObjectTab1;
                }

                RaisePropertyChanged("ObjVisibility");
            };
            IsBusy = true;
            worker.RunWorkerAsync();
        }

        private void OnClearCapabilityFilterCommnad()
        {
            AllCapability = false;

            OnSearchAdvancedCommand();
            RaisePropertyChanged("SelectedCapability");

            foreach (var capability in Capabilities)
            {
                capability.IsChecked = false;
            }
        }

        private void OnClearCategoryFilterCommnad()
        {
            SelectedCategory = null;
            OnSearchAdvancedCommand();
        }

        private void OnClearDescriptionFilterCommnad()
        {
            SelectedDescription = null;
            OnSearchAdvancedCommand();
        }

        private void OnClearInterfaceFilterCommnad()
        {
            SelectedInterface = null;
            OnSearchAdvancedCommand();
        }

        private void OnClearAdvancedFilterCommnad()
        {
            SelectedCategory = null;
            SelectedDescription = null;
            SelectedInterface = null;
            IsExpandedAdvanced = false;
            OnSearchAdvancedCommand();
        }

        private void OnSearchArdParameters(WpfCapability cap)
        {
            if (cap != null)
            {
                var worker = new BackgroundWorker();
                worker.DoWork += (o, ea) =>
                {
                    var temp = ArdWrapper.SearchArdByParameters(cap.Key, cap.JavaParameters, cap.JavaParameterValues);
                };
                worker.RunWorkerCompleted += (o, ea) =>
                {
                    IsBusy = false;
                };
                IsBusy = true;
                worker.RunWorkerAsync();
            }
        }

        private void OnSearchRdParameters(WpfCapability cap)
        {
            if (cap != null)
            {
                var worker = new BackgroundWorker();
                worker.DoWork += (o, ea) =>
                {
                    var temp = ArdWrapper.SearchRdByParameters(cap.Key, cap.JavaParameters, cap.JavaParameterValues);
                };
                worker.RunWorkerCompleted += (o, ea) =>
                {
                    IsBusy = false;
                };
                IsBusy = true;
                worker.RunWorkerAsync();
            }
        }

        #endregion

        #region Methods

        private void WpfCapability_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsChecked")
            {
                var cap = sender as WpfCapability;
                if (cap != null && cap.IsChecked)
                {
                    cap.ClearParameter();
                    var worker = new BackgroundWorker();

                    worker.DoWork += (o, ea) =>
                    {
                        if (TabSelectedIndex == 0)
                        {
                            ArdWrapper.GetSearchKeysArd(cap);
                        }
                        if (TabSelectedIndex == 1)
                        {
                            ArdWrapper.GetSearchKeysRd(cap);
                        }
                    };
                    worker.RunWorkerCompleted += (o, ea) =>
                    {
                        IsBusy = false;
                        UpdateText();
                        SearchTypeManager();
                    };

                    IsBusy = true;
                    worker.RunWorkerAsync();
                }
                RaisePropertyChanged("SelectedCapability");
                RaisePropertyChanged("VisSearchCapability");
            }
        }

        private void UpdateText()
        {
            switch (SelectedCapability.Count)
            {
                case 0:
                    TextSelectedCapability = "<No Selection>";
                    break;
                case 1:
                    TextSelectedCapability = SelectedCapability.First().Key;
                    break;
                default:
                    TextSelectedCapability = "<Multiple>";
                    break;
            }
            var tooltip = SelectedCapability.Aggregate("",
                (current, wpfCapability) => current + $"{wpfCapability.Key}, ");
            TooltipSelectedCapability = string.IsNullOrEmpty(tooltip)
                ? "<No Selection>"
                : tooltip.Substring(0, tooltip.Length - 2);

        }

        private void SearchTypeManager()
        {
            foreach (var wpfCapability in SelectedCapability)
            {

            }
        }

        #endregion

        #region Test

        private string _rdIdTest = "rd.Bosch-ReCaM-Gripper-Disc1";

        public string RdIdTest
        {
            get { return _rdIdTest; }
            set
            {
                if (value != _rdIdTest)
                {
                    _rdIdTest = value;
                    RaisePropertyChanged();
                }
            }
        }

        public RelayCommand LoadTestRd { get; set; }

        private void OnLoadTestRd()
        {
            OnClearCommand();
            OnSearchAdvancedCommand();

            var worker = new BackgroundWorker();
            worker.DoWork += (o, ea) =>
            {
                if (TabSelectedIndex == 1)
                    TabSelectedIndex = 0;
                else
                    TabSelectedIndex = 1;
                ArdWrapper.TestFinlands();

                //ArdWrapper.Init(ConfigFileName, LocalFileSystem);
                //_propertyGridObjectTab0 = ArdWrapper.TestFinlands();
                //_propertyGridObjectTab1 = ArdWrapper.TestFinlands();
                //_propertyGridObjectTab0 = ArdWrapper.GetRdStructure(RdIdTest);
                //_propertyGridObjectTab1 = ArdWrapper.GetRdStructure(RdIdTest);
            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                IsBusy = false;
                if (TabSelectedIndex == 0)
                {
                    PropertyGridObject = _propertyGridObjectTab0;
                }
                if (TabSelectedIndex == 1)
                {
                    PropertyGridObject = _propertyGridObjectTab1;
                }
                RaisePropertyChanged("ObjVisibility");
            };
            IsBusy = true;
            worker.RunWorkerAsync();
        }

        #endregion
    }

    public class MainWindowMessage
    {
        public string Message { get; set; }
        public string Caption { get; set; }
        public int MessageIcon { get; set; }
    }

    public class ParamWindowMessage
    {
        public WpfCapability Capability { get; set; }
    }

    public class ParamWindowCloseMessage
    {
        public WpfCapability Capability { get; set; }
    }
}