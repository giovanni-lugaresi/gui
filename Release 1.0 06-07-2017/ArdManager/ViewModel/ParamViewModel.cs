﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArdManager.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace ArdManager.ViewModel
{
    public class ParamViewModel : ViewModelBase
    {
        public ParamViewModel()
        {
            ////if (IsInDesignMode)
            ////{
            ////    // Code runs in Blend --> create design time data.
            ////}
            ////else
            ////{
            ////    // Code runs "for real"
            ////}

            SetParamCommand = new RelayCommand(OnSetParamCommand, () => true);
            ClearCommand = new RelayCommand(OnClearCommand, () => true);
        }

        public string Text => $"Parameter Search";
        public string ButtonSearch => $"Set Values";
        public string ButtonClear => $"Clear";
        
        private WpfCapability _capability;

        public WpfCapability Capability
        {
            get { return _capability; }
            set
            {
                if (value != _capability)
                {
                    _capability = value;
                    RaisePropertyChanged();
                    RaisePropertyChanged("SearchParameterCount");
                }
            }
        }

        public string SearchParameterCount => Capability?.Parameters != null
            ? $"Par. Count: {Capability.Parameters.Count}"
            : $"No Par.";


        #region Commands

        public RelayCommand SetParamCommand { get; set; }
        public RelayCommand ClearCommand { get; set; }

        private void OnSetParamCommand()
        {
            Messenger.Default.Send(new NotificationMessage("Close"));
        }

        private void OnClearCommand()
        {
            if (Capability?.Parameters?.Any()==true)
            {
                foreach (var obj in Capability.Parameters)
                {
                    obj.Value = null;
                }
            }
            Messenger.Default.Send(new NotificationMessage("Close"));
        }
        #endregion
    }

}
